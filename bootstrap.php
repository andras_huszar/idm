<?php
error_reporting(E_ALL);
chdir(__DIR__);

/**
 * Define environment
 */
define('APP_HOST', preg_replace('/[:\d]$/', '', @$_SERVER['HTTP_HOST']) ?: @$_SERVER['argv'][1]);

$local = '/^localhost|\.local|\.dmz|^127\.|^192\.168\.|^10\.|^172\.1[6-9]\.|^172\.2[0-9]\.|^172\.3[0-1]\.|^orm:/';

if (!APP_HOST || preg_match($local, APP_HOST)) {
	$env = 'development';
} elseif (preg_match('/preview\./', APP_HOST)) {
    $env = 'staging';
} else {
	$env = getenv('APPLICATION_ENV') ?: 'production';
}

define('APP_ENV', 'development');

/**
 * Composer init
 */
$loader = require_once __DIR__ . '/vendor/autoload.php';
$loader->add('', realpath('module'));

/**
 * MVC application boot
 */
$app = \Zend\Mvc\Application::init(include __DIR__ . '/config/application.config.php');