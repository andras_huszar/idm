﻿# SQL Manager 2010 for MySQL 4.5.0.9
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : idm_project


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `idm_project`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_unicode_ci';

USE `idm_project`;

#
# Structure for the `file` table : 
#

CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `size` int(11) NOT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Structure for the `groups` table : 
#

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `leader_id` int(11) DEFAULT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F06D3970727ACA70` (`parent_id`),
  KEY `IDX_F06D397073154ED4` (`leader_id`),
  CONSTRAINT `FK_F06D3970727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `groups` (`id`),
  CONSTRAINT `FK_F06D397073154ED4` FOREIGN KEY (`leader_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Structure for the `user` table : 
#

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture_id` int(11) DEFAULT NULL,
  `uploads_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_senior` tinyint(1) NOT NULL,
  `is_lead` tinyint(1) NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:guid)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `admited_at` date DEFAULT NULL,
  `fired_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  UNIQUE KEY `UNIQ_8D93D649EE45BDBF` (`picture_id`),
  UNIQUE KEY `UNIQ_8D93D649B66372A5` (`uploads_id`),
  KEY `IDX_8D93D649FE54D947` (`group_id`),
  CONSTRAINT `FK_8D93D649B66372A5` FOREIGN KEY (`uploads_id`) REFERENCES `file` (`id`),
  CONSTRAINT `FK_8D93D649EE45BDBF` FOREIGN KEY (`picture_id`) REFERENCES `file` (`id`),
  CONSTRAINT `FK_8D93D649FE54D947` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

#
# Structure for the `report` table : 
#

CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requester_id` int(11) DEFAULT NULL,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C42F7784ED442CF4` (`requester_id`),
  CONSTRAINT `FK_C42F7784ED442CF4` FOREIGN KEY (`requester_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Structure for the `rights` table : 
#

CREATE TABLE `rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `resource` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `privilege` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqid` (`resource`,`privilege`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Structure for the `role` table : 
#

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_57698A6A5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Structure for the `role__right` table : 
#

CREATE TABLE `role__right` (
  `role_id` int(11) NOT NULL,
  `right_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`right_id`),
  KEY `IDX_95D47839D60322AC` (`role_id`),
  KEY `IDX_95D4783954976835` (`right_id`),
  CONSTRAINT `FK_95D4783954976835` FOREIGN KEY (`right_id`) REFERENCES `rights` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_95D47839D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Structure for the `user__role` table : 
#

CREATE TABLE `user__role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `IDX_E88E0129A76ED395` (`user_id`),
  KEY `IDX_E88E0129D60322AC` (`role_id`),
  CONSTRAINT `FK_E88E0129A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_E88E0129D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for the `user` table  (LIMIT 0,500)
#

INSERT INTO `user` (`id`, `picture_id`, `uploads_id`, `group_id`, `email`, `password`, `first_name`, `last_name`, `gender`, `city`, `address`, `phone`, `is_active`, `is_senior`, `is_lead`, `uuid`, `created_at`, `updated_at`, `admited_at`, `fired_at`) VALUES 
  (1,NULL,NULL,3,'hawer183@gmail.com','$2a$10$N5VCS6sYAvfeUYVzUstBYud5tWGOl0HQaMUVsnEfBnbO5gS.zImRu','Zoltan','Lawrence','m','Nova Straz','Tas Vezér Utca','0611234567',1,1,1,'','2014-08-25 21:51:07','2014-09-14 14:08:44','2014-09-14',NULL),
  (2,NULL,NULL,2,'alma.bela@ezaz.com','$2a$10$oTG6O2nzUr2OMk5fD57QLOWHNePZMzYVls8ymEYIwtmntpRGPncaS','Alma','Béla','m','Göd','Rendes utca 9','06303569558',1,1,1,'','2014-08-27 16:46:11','2014-09-21 12:26:55','2014-09-21',NULL),
  (3,NULL,NULL,3,'verebelyi.gabor@humanoit.hu','$2a$10$j74C4T55nOM.EKDfNYvBN.s3.7JsR/a0gJMykzd1gpUHB1kqoip4C','Verebélyi','Gábor','m','Budapest','Ó utca. 3','06305698988',1,0,1,'','2014-08-27 16:47:17','2014-09-21 12:28:15','2013-01-01','2014-01-01'),
  (4,NULL,NULL,3,'hollosi.gabor@marelmentacegtol.com','$2a$10$a2ZzxqtUtB4KEGy6KuLM/ONTw2RfB0AkP4MAjMI26du0L23SAfoei','Hollósi','Gábor','m','Budapest','Egér út. 4','06704568585',1,0,1,'5e7554075d161fe9c7cb4fe9800148bd14ef2d4a','2014-08-28 21:52:56','2014-09-21 12:28:41','2006-01-01','2014-06-13'),
  (5,NULL,NULL,2,'kiss@pal.com','$2a$10$oTB0wOY1IIyeRW2tdtsr.uaqDlleSoaBMCXPDKKpaS/f6iGt29PC6','Kiss','Pál','m','Budapest','Acél út','06303565656',1,0,1,'4e414a4440e980774a940d6f944de02fdcb1e360','2014-09-06 22:56:00','2014-09-21 12:29:00','2014-09-05','2014-01-01'),
  (6,NULL,NULL,3,'szevasz@csa.com','$2a$10$iGyvptIxdBukJiZ9rExCxOp.72Saxn/tzTvrI6l4G2HFYs2Nh.2fW','Nagy','Imre','m','Debrecen','Tas utca','06203568574',1,0,1,'39d7b58058d56e6768de5ac4f02521116e665cbc','2014-09-06 22:56:53','2014-09-07 12:55:08','2014-09-05','2014-01-01'),
  (7,NULL,NULL,4,'ferency@aniko.hu','$2a$10$/93pJv1vUmIKxeF7GroNkOQEkV9B2xSt5TltwVf4lowgoXa2P/8fa','Ferency','Aniko','f','Szeged','Váci út 123','06704587878',1,0,1,'60669ce90a03b81b812ab9eee7824368bb698056','2014-09-06 22:58:13','2014-09-21 12:30:14','2014-08-31','2014-01-01'),
  (8,NULL,NULL,5,'hullman@gmail.com','$2a$10$WjfVah.RlgD/R1Mx6LNLMeA1peYMpvtJEGR.I76k3eDEPO/tU6n7C','Hullman','Katalin','f','Budapest','Móricz utca 89/43b','06303213256',1,0,1,'5ba90532a26c8820114bfedc769d4921328c984c','2014-09-06 22:59:12','2014-09-07 12:56:37','2014-08-04','2014-01-01'),
  (9,NULL,NULL,7,'szabo@gmail.com','$2a$10$LqL94Lqaomd/dTt0cOApMe03Ig9TUd5ZhQ30wTF3vi.0B23eGM8eC','Szabó','Attila','m','Budapest','Gizella utca 23','06303569898',1,0,1,'181deeddea94248472c23a5db1900b5468b56274','2014-09-06 23:01:31','2014-09-21 12:30:27','2014-02-01','2014-01-01'),
  (10,NULL,NULL,9,'hernam@yahoo.com','$2a$10$gRVkOnfqN2CLTn5ZdcrzqeAhiR0PCFCMwtwQpr8V1DTZ4oqZd07ji','Hermann','Attila','m','Budapest','Csepp utca 34','06305654896',1,0,1,'83d3b602232d4f371e4ffbc916e53ef0102fe77f','2014-09-06 23:16:11','2014-09-07 12:57:03','2014-11-01','2014-01-01'),
  (11,NULL,NULL,10,'fencyke@gmail.com','$2a$10$BGtl0XUPzTL7KyOoewGao.DkfVYwlphmH8lJ8NNadHx0rHf3Ym1e2','Ferency','Gábor','m','Pécs','Fő utca 3','06303236569',1,0,1,'0fe715eb6632862ffe5cc59a0e3ce84da45d688b','2014-09-06 23:17:07','2014-09-21 12:30:58','2014-03-01','2014-01-01'),
  (12,NULL,NULL,8,'banhalmi.csaba@humanoit.hu','$2a$10$JZmnNEOEkea02GxGNITPC.oitEuaGMgvEo.8qCFA27jx9.fMUoy6e','Bánhalmi','Csaba','m','Budapest','Király utca 3','0630569858',1,0,0,'2c44b53743c93413386d3186ba3ea4ffaa92f6c8','2014-09-06 23:25:00','2014-09-21 12:31:16','2014-01-01','2014-01-01'),
  (13,NULL,NULL,7,'daught.tamas@mszp.hu','$2a$10$q8DQUlkB3tkiCt.5tCafo.YzDms.82MDnaCX1pQivjVxxW5NfAV32','Daught','Tamás','m','Pécs','','',1,0,1,'71c661c14900bf5ef8b841707a2f32a53f6c487e','2014-09-06 23:25:44','2014-09-21 12:32:16','2014-01-01','2014-01-01'),
  (14,NULL,NULL,8,'kovacs98@gmail.com','$2a$10$q9xU/FU.lUQN0Zxyrul4OeZWMW4hkqPKeBdv5FFyjE4FYucg.7Dqi','Kovács','Géza',NULL,'Budapest','Virág utca 112c','0630/457-7878',0,0,0,'8390c204686279b925d684425a5ea1c12a2af670','2014-09-07 11:59:16','2014-09-21 12:32:01','2014-04-01','2014-08-07'),
  (15,NULL,NULL,1,'boldog.arpad@humanoit.hu','$2a$10$y4ryTXl4DbN1bTyDLwBCaeeTRq7Pe.QJPp4RZJz/vTwjKNQGj.CZu','Boldog','Árpád','m','Budapest','Király utca 76','06303696565',1,1,1,'bae6f36659e0538a506c3a5f02bdac0412b411b4','2014-09-07 15:35:51','2014-09-21 12:32:34','2004-04-01','2014-01-01'),
  (16,NULL,NULL,5,'kovacs.mihaly@humanoit.hu','$2a$10$Abzbq3lXZ0e3e4qIdJ0wK.QpKUr70RCspogmZClgvsWSp3oaPur3S','Kovács','Mihály','m','Budapest','Bubi utca 32','06305456958',1,0,1,'f766620ae085f4247a658df0a24f1171de585010','2014-09-07 15:37:32','2014-09-21 12:32:51','2009-01-01','2014-01-01'),
  (17,NULL,NULL,1,'berenyi.attila@humanoit.hu','$2a$10$17OUoiHneQCBVuMd.c6Bv.ej6j3o996FDdwgb6fdeuere6OtTFKU2','Berényi','Attila','m','Vác','Fá utca 34','06303696565',1,0,1,'735b098431ba96ddd4cea4bc95195b5bfbe10720','2014-09-07 15:39:11','2014-09-21 12:33:15','2005-01-01','2014-01-01'),
  (18,NULL,NULL,5,'javor.zsofia@humanoit.hu','$2a$10$xtC.NU1/XzRQgvYvvYWGuOGseNH5De60SD0OsWg4OwBfAuGJMvOE6','Jávor','Zsófia','f','Budapest','Kanári út 34','06704568787',1,0,0,'6b2fc08db84c93dfd844976c5828e8018c9852f3','2014-09-07 15:40:05','2014-09-21 12:34:00','2014-01-01','2014-01-01'),
  (19,NULL,NULL,8,'varga.gabor@humanoit.hu','$2a$10$oMZIMtqTWeW5no.uSJBVYuqVPaa9ZqjmaVzubM0Pnw7WRJJNEH1Su','Varga','Gábor','m','Budapest','Bajcsy Zsilinszky utca 234','06305698585',1,0,0,'21fe1431a93ebe9fcf14732e8499471412d1996f','2014-09-07 15:40:59','2014-09-21 12:34:26','2011-01-01','2014-01-01'),
  (20,NULL,NULL,12,'gubai.gyorgy@humanoit.hu','$2a$10$D02tI2ZhKMRsNim4IWmJGuKgWB.KUofqKOHFLPPbs0iXALz3OEO32','Gubai','György','m','Veresegyháza','Pitypang tér 2.','06303569658',1,1,1,'d9cad8dd2bd2f755ad6306fa5970de12c03dc824','2014-09-07 15:41:59','2014-09-21 12:34:46','2007-01-01','2014-01-01'),
  (21,NULL,NULL,11,'hollosi.izabella@yahoo.com','$2a$10$2M2WQVR68JHccsuso9nN1uCo4SGpuTN97XIE0krpAH4Xay21DDOTK','Hollósi','Izabella','f','Érd','Nefelejcs utca 3','06305698585',1,0,0,'0f79b638f142faf04f791c9c223b2d3527f8a591','2014-09-07 15:50:05','2014-09-21 12:35:09','2010-01-01','2014-01-01'),
  (22,NULL,NULL,9,'szecsi.balazs@moresimp.hu','$2a$10$Tbn2urEmHSCvYpkNZLmzi.BbXV79lhJpuqJkZuC2l5554pnnbgXt2','Szőcsi','Balázs','m','Budapest','Oktogon 4','06303698585',1,0,0,'6786625dd42927f31c91bec4dbb9250c81b2c607','2014-09-07 15:51:04','2014-09-21 12:35:40','2014-04-01','2014-01-01'),
  (23,NULL,NULL,7,'gersi.karoly@moresimp.hu','$2a$10$33PdVSu9Dzek.vcq.KvmduAf9O.7xEtpVI4AdBledJgqT3.gA7VNG','Gersi','Károly','m','Budapest','Etele tér 423b.','06705698585',1,0,1,'00f51bccc161a77e7897a62f991be93bffd3359b','2014-09-07 15:51:55','2014-09-21 12:35:57','2014-01-01','2014-01-01'),
  (24,NULL,NULL,11,'erdei.sandor@humanoit.hu','$2a$10$FreRJ6y4dLWefsiRcYfEXu6RR0fWLgNMMGMionDKY05/4sTzGb1vC','Erdei','Sándor','m','Budapest','Kiss utca 32.','06705698585',1,0,0,'049cde78d6235adcc3be7b8251f4d89bf57060c2','2014-09-07 15:52:53','2014-09-21 12:36:12','2013-09-01','2014-01-01'),
  (25,NULL,NULL,NULL,'dobos.andrea@moresimp.com','$2a$10$pzL.peakPtOExEh4taMsrelBqHLK5U6Rg1YznLRqMjKp.j3eGYCZq','Dobos','Andrea','f','Esztergom','Fa utca 5.','06305654745',1,0,1,'51b6d49125bda0598c8422c74792672a5e0b2dae','2014-09-07 15:54:15',NULL,'2011-01-01','2014-01-01'),
  (26,NULL,NULL,4,'csonka.attila@humanoit.hu','$2a$10$yy2DGIzWYQrJewuBlKhY0O2JG.dpu0JxwtxrL3PQm6ppDYOuZfqCi','Csonka','Attila','m','Esztergom','Gömb utca 3.','06305698585',1,0,1,'a84dcdd3fac53a4316ef4b2499e0f23cc69d3174','2014-09-07 15:55:29','2014-09-21 12:40:05','2011-01-01','2014-01-01'),
  (27,NULL,NULL,NULL,'pozsgai.peter@humanoit.hu','$2a$10$b.T6GOYcgslAYYMZvbhDX.bXEIaZ3xCoP2dAiJ6bfI1e.N4KWnTGe','Pozsgai','Péter','m','Budapest','Csorba utca 3.','06302145454',1,0,0,'7b652e46e1bac56de124b714bed3308d8a647eb8','2014-09-07 15:56:23',NULL,'2012-04-28','2014-01-01'),
  (28,NULL,NULL,NULL,'nagy.viktor@humanoit.hu','$2a$10$Prxnhd.sG6gjmt8S1/FrhuBsRuZ534/SQhuG0TbseiyM9zheuY8YK','Nagy','Viktor','m','Budapest','Ária utca 345.','0630/548-7878',1,0,0,'33c50f44602b13143acff09941d2e4b140f5edc8','2014-09-07 15:57:24',NULL,'2011-04-01','2014-01-01'),
  (29,NULL,NULL,NULL,'somogyvari.kata@humanoit.hu','$2a$10$nPr9BmTvbipmqhjNokEtgOB/iMuUrW9Yw3SOkKGEuZskMeSdtBixm','Somogyvári','Katalin','f','Budapest','Vessző utca 32.','0630789-4521',1,0,0,'0547f1fe546b850208c82f5420eead354fc1e2ad','2014-09-07 15:58:31',NULL,'2012-01-01','2009-01-01'),
  (30,NULL,NULL,NULL,'biro.mark@moresimp.com','$2a$10$SX2IklQ/ww0Dpw4SvmTo9e9LP9.N1d6A0yCY4zY2wuuesvSJQ5h96','Báró','Márk','m','Budapest ','Budaörsi Ãºt 23','06306879654',1,0,0,'c93d5022fcb9179ff3935822c8b8dbd2ba168df4','2014-09-07 16:00:33',NULL,'2014-07-01','2014-01-01'),
  (31,NULL,NULL,NULL,'felletar.fanni@moresimp.com','$2a$10$md0iG6PAAf.abN7IwoQyuup8dsbgA/987CX/ft3doOVEkobvy3jba','Felletér','Fanni','f','Budapest','Orczy utca 23.','06307898569',1,0,0,'fb96f5f5e4d9f7185f28ac22d444aea822ed9298','2014-09-07 16:02:35',NULL,'2014-08-01','2014-01-01'),
  (32,NULL,NULL,NULL,'szanto.andras@moresimp.com','$2a$10$E5.iEgra9qtOA9ijkNLRy.QOIPvaZwNCTyOdzN5QTMpLSeynxyA/.','Szántó','András','m','Budapest','Margit sziget 43.','06305896564',0,0,1,'758fa6aec2909ca3ebdc1097f6be482fe1ba2ee7','2014-09-07 16:03:17',NULL,'2014-08-01','2014-01-01'),
  (33,NULL,NULL,NULL,'kapcsos.zoltan@moresimp.com','$2a$10$XI/q6NGHZ44p9C3/.g1vbO.NAspjE6goa0v0Jeo289e85Ade9jYUq','Kapcsos','Zoltán','m','Budapest','Uszoda utca 3.','06305478787',1,0,0,'e0bc007c275966365e07f7b2ff8d6b7113b05f19','2014-09-07 16:03:59',NULL,'2014-08-01','2014-01-01'),
  (34,NULL,NULL,24,'katalin.cseh@moresimp.com','$2a$10$fTxgbu9SostjFXaNJ9EiP.Tl3i7twDWwyHrNDPgzZfXSn7s8Jpkbe','Bacsa-Cseh','Katalin','f','Budapest','','',1,0,0,'70c020cd526e3c07e0a4bb543e86b745f89f038d','2014-09-07 20:08:37',NULL,'2014-01-01','2014-01-01'),
  (35,NULL,NULL,25,'andras.gyorffy@moresimp.com','$2a$10$wtRg3CPc3SEKhw86Mry5J.aKE2MCH9ZXlAXUbjPTkbUg4dl24RRgy','Győrffy','András','m','','','',1,0,0,'cbe39a333c2e9ceb7965a8823aef4f481615a784','2014-09-07 20:12:13','2014-09-07 20:12:44','2014-01-01','2014-01-01'),
  (36,NULL,NULL,29,'dobos.eva@humaniot.hu','$2a$10$gK05oDo5nJ.K/0F1YoYl4.D5tPrLGKTkqs3UaSffIm5uYDUQb0qbW','Dobos','Éva','f','Budapest','','',1,0,1,'2ceb2a79028a9245b3bbb4b040ed8659759061f5','2014-09-07 20:18:35',NULL,'2014-01-01','2014-01-01'),
  (37,NULL,NULL,29,'kazamer.adel@humanoit.hu','$2a$10$/9K79NuRcK1FmE0zMnBLLe7k3Oy6ug4nZtByv75cVhinOq4qk1ub6','Kazamér','Adél','f','Budapest','','',1,0,0,'a1ab4e843126781ea59c2745f792b3d1ff511080','2014-09-07 20:21:17',NULL,'2014-01-01','2014-01-01'),
  (38,NULL,NULL,18,'laszlo.kiss@moresimp.com','$2a$10$zdxJ8XdIhx8C93wO2F5iAeTF1.wLZYJuQ8fLZbLzkuMPPKh9zu5ke','Kiss','László','m','Budapest','','',1,0,0,'960edf7053824ca5f7f62c9f370ad06d0ba27edf','2014-09-07 20:22:03',NULL,'2014-01-01','2014-01-01'),
  (39,NULL,NULL,25,'kremzel.jozsef@moresimp.com','$2a$10$xhtM3ZnMYwaVdujjYSKlRe6R.hzKiVskKieM9pTDSfrI96GN.agvm','Kremzel','József','m','Budapest','','',1,0,0,'15115c8d04211cac824184d0159de06de3685fad','2014-09-07 20:22:52',NULL,'2014-01-01','2014-01-01'),
  (40,NULL,NULL,18,'miklos.papp@humanoit.hu','$2a$10$XZjjWb.apJ633e9PDan5oOaGUsTudFPXTL2NKLI76o25tKJ7v9Gze','Papp','Miklós','m','Budapest','','',1,0,0,'0296d526c9b3147daf5e128e1e437aae507fbc4c','2014-09-07 20:23:39',NULL,'2014-01-01','2014-01-01'),
  (41,NULL,NULL,7,'schumayer.marta@humanoit.hu','$2a$10$J4a.SZa7jUL2kanwUh83cevIycv2tD.UrnI3.ojCbUFWbrnPABJc2','Schumayer','Márta','f','Budapest','','',1,0,0,'a0dda22eb5a4051b263fc4e07415e96779ed1ab1','2014-09-07 20:24:59',NULL,'2014-01-01','2014-01-01'),
  (42,NULL,NULL,11,'gabor.varga@humanoit.hu','$2a$10$mFosQkRahA9DRc3pimU75OG2LoZpw.hbthlew38ovgXA.x35t6l6K','Gábor','Varga','m','Budapest','','',1,0,0,'fe567adf9690af25651d93b9bdb345693076ecac','2014-09-07 20:35:29',NULL,'2014-01-01','2014-01-01'),
  (43,NULL,NULL,8,'varhegyi.viktor@humanoit.hu','$2a$10$MUOsfdqsxXFQsW46ng1q2.wIl021pz9vsS1TgIpMfXREroIEqAR0e','Várhegyi','Viktor','m','Budapest','','',1,0,0,'4c194136ea6e4a03db6bf4edc96aca4a259c553c','2014-09-07 20:39:13',NULL,'2014-01-01','2014-01-01');
COMMIT;

#
# Data for the `groups` table  (LIMIT 0,500)
#

INSERT INTO `groups` (`id`, `parent_id`, `leader_id`, `name`) VALUES 
  (1,NULL,15,'CEO'),
  (2,1,5,'HR'),
  (3,1,6,'Development'),
  (4,1,7,'Business'),
  (5,4,10,'Marketing'),
  (6,4,11,'Consulting'),
  (7,4,13,'Finance'),
  (8,3,8,'Support'),
  (9,3,9,'Testers'),
  (10,3,10,'Coders'),
  (11,4,16,'Sales'),
  (12,3,20,'System Integration'),
  (13,1,17,'Controlling'),
  (14,1,25,'Secretary'),
  (15,6,26,'Quality Management department');
COMMIT;

#
# Data for the `role` table  (LIMIT 0,500)
#

INSERT INTO `role` (`id`, `name`) VALUES 
  (1,'Admin'),
  (2,'User');
COMMIT;

#
# Data for the `user__role` table  (LIMIT 0,500)
#

INSERT INTO `user__role` (`user_id`, `role_id`) VALUES 
  (1,1);
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;