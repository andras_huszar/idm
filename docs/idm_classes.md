
 User
  UserForm 
   UserContact
   UserBio
   UserSettings
  ChangePasswordForm
  GroupForm
  RequestAccess
  
 File
  UploadForm
  FileForm
  ShareFileForm

 Tasks
  RequestIssueForm
  AssignIssueForm
  ResolveIssueForm

 Office
  DeviceForm
  RequestDeviceForm
  ResolveDeviceRequestForm
  RecoverDeviceForm
  RoomForm
  RequestRoomForm
  DismissRoomReservationForm
  RequestOfficeIssueForm

 Job
  RequestResourceForm
  ResolveResourceRequestForm
  
  RequestHolidayForm
  ResolveHolidayRequestForm
  
 Messages
  Write message

 News
  PostForm

 Options
  UserOptionsForm
  UploadOptionsForm
  BackupOptionsForm
  ReportBugForm