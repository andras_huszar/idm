
function openUploadPopup(url, name) {
	var newwindow = window.open(url, 'name', 'width=350,height=200');
	if (window.focus) {
		newwindow.focus();
	}
	
	var processForm = function () {
		alert('submit');
		return false;
	};
	
	var form = document.getElementByTagName('form');
	if (form.attachEvent) {
	    form.attachEvent("submit", processForm);
	} else {
	    form.addEventListener("submit", processForm);
	}
	return false;
}
$(function() {
	$('#user-form').each(function() {
		var $isActive = $(this).find('.is_active');
		var $firedAt = $(this).find(':input[name^="fired_at"]');
		var cb = function() {
			if ($isActive.filter(':checked').attr('value') == '1') {
				$firedAt.prop('disabled', 'disabled');
			} else {
				$firedAt.prop('disabled', false);
			}
		};
		$isActive.on('change', cb);
		cb();
	});
	
    $('.tree li').hide();
    $('.tree li:first').show();
    $('.tree li').on('click', function (e) {
    	e.preventDefault();
        var children = $(this).find('> ul > li');
        if (children.is(":visible")) children.hide('fast');
        else children.show('fast');
        e.stopPropagation();
        return false;
    });
    
    // Picture upload
    $('[data-picture-upload]').pictureUpload({
		successCallback: function(result) {
			var $el = result[0],
				data = result[1],
				img = new Image(),
				thumb = $el.data('thumbnail'),
				path = 'file/picture/';

			if (thumb) {
				path += thumb + '/';
			}
			img.src = path + data.path;
			
			$el.find('.target-area img').remove();
			$el.find('[type="hidden"]').val(data.picture);
			$el.append(img);
		},
		errorCallback: function() {
		}
	});
    
});