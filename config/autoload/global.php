<?php
return array(
    'doctrine' => array(
        'configuration' => array(
            'orm_default' => array(
                'proxy_dir' => __DIR__ . '/../../data/cache/doctrine/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
            ),
        ),
        'connection' => array(
            'orm_default' => array(
                'params' => array(
                    'charset' => 'utf8'
                ),
            ),
        ),
    ),
    'gk_smarty' => array(
        'suffix' => 'tpl',
        'compile_dir' => __DIR__ . '/../../data/cache/smarty/templates_c',
        'cache_dir' => __DIR__ . '/../../data/cache/smarty/cache/templates',
    ),
    'session' => array(
        'config' => array(
            'class' => 'Zend\Session\Config\SessionConfig',
            'options' => array(
                'name' => 'idm_application',
                'savePath' => __DIR__ . '/../../data/session',
            ),
        ),
        'storage' => 'Zend\Session\Storage\SessionArrayStorage',
        'validators' => array(
            array(
                'Zend\Session\Validator\RemoteAddr',
                'Zend\Session\Validator\HttpUserAgent',
            ),
        ),
    ),
);