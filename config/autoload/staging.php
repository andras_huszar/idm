<?php
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'configuration' => 'orm_default',
                'eventmanager'  => 'orm_default',
                'params' => array(
                    'driver'   => 'pdo_mysql',
                    'host'     => 'localhost',
                    'port'     => '3306',
                    'user'     => '',
                    'password' => '',
                    'dbname'   => '',
                ),
            ),
        ),
        'configuration' => array(
            'orm_default' => array(
                'metadata_cache'    => 'xcache',
                'query_cache'       => 'xcache',
                'result_cache'      => 'xcache',
                'generate_proxies'  => false
            ),
        ),
    ),
);