<?php
$viewDir = __DIR__ . '/../view/idm-dashboard';
return array(
    'view_manager' => array(
        'template_map' => array(
            'layout/layout' => $viewDir . '/layout.tpl',
            'error/403'     => $viewDir . '/error/403.tpl',
            'error/404'     => $viewDir . '/error/404.tpl',
            'error/index'   => $viewDir . '/error/index.tpl',
            'layout/blank'  => $viewDir . '/blank.tpl',
            'paginator' => $viewDir . '/paginator.tpl',
            'idm-application/form'      => $viewDir . '/form.tpl',

            'idm-application/index/index'      => $viewDir . '/index/index.tpl',
            'idm-user/user/index' => $viewDir . '/user/index.tpl',
            'idm-user/user/hierarchy' => $viewDir . '/user/hierarchy.tpl',
            'idm-user/user/login' => $viewDir . '/user/login.tpl',
            'idm-user/role/index' => $viewDir . '/role/index.tpl',
            'idm-user/right/index' => $viewDir . '/right/index.tpl',
            'idm-user/group/index' => $viewDir . '/group/index.tpl',
            'idm-user/group/tree' => $viewDir . '/group/tree.tpl',
            'idm-user/profile/view' => $viewDir . '/profile/view.tpl',
            'idm-file/file/index' => $viewDir . '/file/index.tpl',
            'idm-file/file/view' => $viewDir . '/file/view.tpl',
            'idm-holiday/holiday/index' => $viewDir . '/holiday/index.tpl',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    )
);