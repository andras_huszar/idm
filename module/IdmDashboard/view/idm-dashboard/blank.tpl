<!DOCTYPE html>
<html lang="en"><head>
    <base href="{$this->basePath()}/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {$this->headTitle()}
    <link rel="shortcut icon" href="images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/icons/favicon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/icons/favicon-114x114.png">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="dashboard/css/jquery-ui-1.10.4.custom.min.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/animate.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/all.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/main.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/style-responsive.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/zabuto_calendar.min.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/pace.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/jquery.news-ticker.css">
    <link type="text/css" rel="stylesheet" href="dashboard/css/custom.css">
  </head>

  <body>

    <div class="container">
      {$content}
    </div> <!-- /container -->

    {$this->headScript()}
  </body>
</html>