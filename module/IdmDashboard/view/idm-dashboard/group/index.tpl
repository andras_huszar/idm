
<div class="panel panel-green">
  <div class="panel-heading">{$pageLabel}</div>
    <div class="panel-body">
        {if !empty($data)}
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <th width="1%">#</th>
              <th>{order action="index" order="g.name" label="Name"}</th>
              <th>{order action="index" order="p.name" label="Parent"}</th>
              <th>{order action="index" order="l.last_name" label="Group leader"}</th>
              <th width="120"></th>
            </tr>
          </thead>
          <tbody>
          {foreach from=$data item="item"}
            <tr>
              <td>{$item.id|default}</td>
              <td>{$item.name|default}</td>
              <td>{$item.parent.name|default}</td>
              <td>
  			{if $item.leader}{$item.leader.first_name} {$item.leader.last_name}{/if}
              </td>
              <td>
                <a class="btn btn-warning btn-xs" href="/group/edit/id/{$item.id|default}">Edit</a>
                <a class="btn btn-danger btn-xs" href="/group/remove/id/{$item.id|default}">Remove</a>
              </td> 
            </tr>
          {/foreach}
          </tbody>
        </table>
        {$this->paginationControl($data)}
        {else}
        <p>No data found</p>
        {/if}
    </div>
  </div>
</div>