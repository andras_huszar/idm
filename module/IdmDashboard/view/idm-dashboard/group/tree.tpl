        
<div class="panel panel-green">
  <div class="panel-heading">{$pageLabel}</div>
    <div class="panel-body" style="min-height: 500px">
        {$this->headLink()->appendStylesheet('dashboard/css/tree.css')}
		<div class="tree">
		{include file="treenode.tpl" data=$result}
		</div>
    </div>
  </div>
</div>