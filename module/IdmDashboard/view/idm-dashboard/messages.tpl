
          {if !empty($msg.error)}
            {foreach from=$msg.error item="message"}
            <div class="alert alert-warning">
              <p>{$message}</p>
            </div>
            {/foreach}
          {/if}

          {if !empty($msg.info)}
            {foreach from=$msg.error item="message"}
            <div class="alert alert-info">
              <p>{$message}</p>
            </div>
            {/foreach}
          {/if}

          {if !empty($msg.success)}
            {foreach from=$msg.success item="message"}
            <div class="alert alert-success">
              <p>{$message}</p>
            </div>
            {/foreach}
          {/if}