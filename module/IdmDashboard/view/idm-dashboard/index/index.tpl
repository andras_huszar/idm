            <div class="row mbl">
                <div class="col-lg-8">
                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="caption">
                                My issues</div>
                        </div>
                        <div style="overflow: hidden;" class="portlet-body">
                            <ul class="todo-list sortable">
                                <li class="clearfix"><span class="drag-drop"><i></i></span>
                                    <div class="todo-check pull-left">
                                        <input type="checkbox" value="" /></div>
                                    <div class="todo-title">
                                        Sed ut perspiciatis unde omnis iste</div>
                                    <div class="todo-actions pull-right clearfix">
                                        <a href="#" class="todo-complete"><i class="fa fa-check"></i></a><a href="#" class="todo-edit">
                                            <i class="fa fa-edit"></i></a><a href="#" class="todo-remove"><i class="fa fa-trash-o">
                                            </i></a>
                                    </div>
                                </li>
                                <li class="clearfix"><span class="drag-drop"><i></i></span>
                                    <div class="todo-check pull-left">
                                        <input type="checkbox" value="" /></div>
                                    <div class="todo-title">
                                        At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</div>
                                    <div class="todo-actions pull-right clearfix">
                                        <a href="#" class="todo-complete"><i class="fa fa-check"></i></a><a href="#" class="todo-edit">
                                            <i class="fa fa-edit"></i></a><a href="#" class="todo-remove"><i class="fa fa-trash-o">
                                            </i></a>
                                    </div>
                                </li>
                                <li class="clearfix"><span class="drag-drop"><i></i></span>
                                    <div class="todo-check pull-left">
                                        <input type="checkbox" value="" /></div>
                                    <div class="todo-title">
                                        Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo
                                        minus id</div>
                                    <div class="todo-actions pull-right clearfix">
                                        <a href="#" class="todo-complete"><i class="fa fa-check"></i></a><a href="#" class="todo-edit">
                                            <i class="fa fa-edit"></i></a><a href="#" class="todo-remove"><i class="fa fa-trash-o">
                                            </i></a>
                                    </div>
                                </li>
                                <li class="clearfix"><span class="drag-drop"><i></i></span>
                                    <div class="todo-check pull-left">
                                        <input type="checkbox" value="" /></div>
                                    <div class="todo-title">
                                        Et harum quidem rerum facilis est</div>
                                    <div class="todo-actions pull-right clearfix">
                                        <a href="#" class="todo-complete"><i class="fa fa-check"></i></a><a href="#" class="todo-edit">
                                            <i class="fa fa-edit"></i></a><a href="#" class="todo-remove"><i class="fa fa-trash-o">
                                            </i></a>
                                    </div>
                                </li>
                                <li class="clearfix"><span class="drag-drop"><i></i></span>
                                    <div class="todo-check pull-left">
                                        <input type="checkbox" value="" /></div>
                                    <div class="todo-title">
                                        Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet</div>
                                    <div class="todo-actions pull-right clearfix">
                                        <a href="#" class="todo-complete"><i class="fa fa-check"></i></a><a href="#" class="todo-edit">
                                            <i class="fa fa-edit"></i></a><a href="#" class="todo-remove"><i class="fa fa-trash-o">
                                            </i></a>
                                    </div>
                                </li>
                                <li class="clearfix"><span class="drag-drop"><i></i></span>
                                    <div class="todo-check pull-left">
                                        <input type="checkbox" value="" /></div>
                                    <div class="todo-title">
                                        Excepteur sint occaecat cupidatat non proident</div>
                                    <div class="todo-actions pull-right clearfix">
                                        <a href="#" class="todo-complete"><i class="fa fa-check"></i></a><a href="#" class="todo-edit">
                                            <i class="fa fa-edit"></i></a><a href="#" class="todo-remove"><i class="fa fa-trash-o">
                                            </i></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>



                <div class="col-lg-4">

                    <div class="portlet box">
                        <div class="portlet-header">
                            <div class="caption">
                                Messages</div>
                        </div>
                        <div class="portlet-body">
                            <div class="chat-scroller">
                                <ul class="chats">
                                    <li class="in">
                                        <img src="images/avatar/48.jpg" class="avatar img-responsive" />
                                        <div class="message">
                                            <span class="chat-arrow"></span><a href="#" class="chat-name">Admin</a>&nbsp;<span
                                                class="chat-datetime">at July 06, 2014 17:06</span><span class="chat-body">Lorem ipsum
                                                    dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                                                    ut laoreet dolore magna aliquam erat volutpat.</span></div>
                                    </li>
                                    <li class="out">
                                        <img src="images/avatar/48.jpg" class="avatar img-responsive" />
                                        <div class="message">
                                            <span class="chat-arrow"></span><a href="#" class="chat-name">Admin</a>&nbsp;<span
                                                class="chat-datetime">at July 06, 2014 18:06</span><span class="chat-body">Lorem ipsum
                                                    dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                                                    ut laoreet dolore magna aliquam erat volutpat.</span></div>
                                    </li>
                                    <li class="in">
                                        <img src="images/avatar/48.jpg" class="avatar img-responsive" />
                                        <div class="message">
                                            <span class="chat-arrow"></span><a href="#" class="chat-name">Admin</a>&nbsp;<span
                                                class="chat-datetime">at July 06, 2014 17:06</span><span class="chat-body">Lorem ipsum
                                                    dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                                                    ut laoreet dolore magna aliquam erat volutpat.</span></div>
                                    </li>
                                    <li class="out">
                                        <img src="images/avatar/48.jpg" class="avatar img-responsive" />
                                        <div class="message">
                                            <span class="chat-arrow"></span><a href="#" class="chat-name">Admin</a>&nbsp;<span
                                                class="chat-datetime">at July 06, 2014 18:06</span><span class="chat-body">Lorem ipsum
                                                    dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                                                    ut laoreet dolore magna aliquam erat volutpat.</span></div>
                                    </li>
                                    <li class="in">
                                        <img src="images/avatar/48.jpg" class="avatar img-responsive" />
                                        <div class="message">
                                            <span class="chat-arrow"></span><a href="#" class="chat-name">Admin</a>&nbsp;<span
                                                class="chat-datetime">at July 06, 2014 17:06</span><span class="chat-body">Lorem ipsum
                                                    dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                                                    ut laoreet dolore magna aliquam erat volutpat.</span></div>
                                    </li>
                                </ul>
                            </div>
                            <div class="chat-form">
                                <div class="input-group">
                                    <input id="input-chat" type="text" placeholder="Type a message here..." class="form-control" /><span
                                        id="btn-chat" class="input-group-btn">
                                        <button type="button" class="btn btn-green">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>