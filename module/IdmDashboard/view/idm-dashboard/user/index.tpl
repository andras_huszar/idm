
<div class="panel panel-green">
  <div class="panel-heading">{$pageLabel}</div>
    <div class="panel-body">
          {$this->form($form, 'inline')}
          {if !empty($data)}
          <table class="table table-hover table-striped">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>{order action="index" order="fullname" label="Full name"}</th>
                  <th>{order action="index" order="email" label="Email"}</th>
                  <th>{order action="index" order="group" label="Group"}</th>
                  <th>{order action="index" order="group_leader" label="Pincipal"}</th>
                  <th>{order action="index" order="is_active" label="Status"}</th>
                  <th width="120"></th>
                </tr>
              </thead>
              <tbody>
              {foreach from=$data item="item"}
                <tr>
                  <td>{$item.id|default}</td>
                  <td>{$item|fullname}</td>
                  <td>{$item.email|default}</td>
                  <td>{$item.group.name|default:"Uncategorized"}</td>
                  <td>{$item.principal|fullname}</td>
                  {*<td>{foreach from=$item.roles|default:[] item="role"}
                    {$role.name}
                  {/foreach}</td>*}
                  <td><span class="badge badge-{if $item.is_active}green{else}red{/if}">{if $item.is_active}Active{else}Inactive{/if}</span></td>
                  <td>
                    <a class="btn btn-warning btn-xs" href="/user/edit/id/{$item.id|default}">Edit</a>
                    <a class="btn btn-danger btn-xs" href="/user/remove/id/{$item.id|default}">Remove</a>
                  </td> 
                </tr>
              {/foreach}
              </tbody>
            </table>
          	{$this->paginationControl($data)}
          {else}
          	<p>No data found</p>
          {/if}
		</div>
	</div>
</div>