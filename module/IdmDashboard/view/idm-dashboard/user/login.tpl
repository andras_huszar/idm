<div class="center-wrapper">
    <div class="panel panel-yellow">
        <div class="panel-heading">
            Login
        </div>
        <div class="panel-body pan">
            {$this->form()->openTag($form, 'form-horizontal')}
            <div class="form-body pal">
                <div class="form-group">
                    <label for="inputName" class="col-md-3 control-label">
                        E-mail
                    </label>
                    <div class="col-md-9">
                        <div class="input-icon right">
                        	<i class="fa fa-user"></i>
                        	{$this->formElement($form->get('email'))}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-md-3 control-label">
                        Password
                    </label>
                    <div class="col-md-9">
                        <div class="input-icon right">
                            <i class="fa fa-lock"></i>
                        	{$this->formElement($form->get('password'))}
                        </div>
                        <span class="help-block mbn"><a href="#"><small>Forgot password?</small> </a></span>
                    </div>
                </div>
                <div class="form-group mbn">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="checkbox">
                            <label><input tabindex="5" type="checkbox" />&nbsp; Keep me logged in</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions pal">
                <div class="form-group mbn">
                    <div class="col-md-offset-3 col-md-6">
                        <a href="{url route='application/register'}" class="btn btn-primary">Register</a>&nbsp;&nbsp;
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </div>
            </div>
            {$this->form()->closeTag()}
        </div>
    </div>
</div>