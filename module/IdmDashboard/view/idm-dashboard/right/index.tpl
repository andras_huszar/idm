
<div class="panel panel-green">
  <div class="panel-heading">{$pageLabel}</div>
    <div class="panel-body">
          {if !empty($data)}
          <table class="table table-hover table-striped">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>Name</th>
                  <th>Resource</th>
                  <th>Privilege</th>
                  <th width="120"></th>
                </tr>
              </thead>
              <tbody>
              {foreach from=$data item="item"}
                <tr>
                  <td>{$item.id|default}</td>
                  <td>{$item.name|default}</td>
                  <td>{$item.resource|default}</td>
                  <td>{$item.privilege|default}</td>
                  <td>
                    <a class="btn btn-warning btn-xs" href="/right/edit/id/{$item.id|default}">Edit</a>
                    <a class="btn btn-danger btn-xs" href="/right/remove/id/{$item.id|default}">Remove</a>
                  </td> 
                </tr>
              {/foreach}
              </tbody>
            </table>
            {$this->paginationControl($data)}
          {else}
            <p>No data found</p>
          {/if}
    </div>
  </div>