
<div class="panel panel-green">
  <div class="panel-heading">{$pageLabel}</div>
    <div class="panel-body">
          {if !empty($data)}
          <table class="table table-hover table-striped">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>{order action="index" order="name" label="Name"}</th>
                  <th>{order action="index" order="type" label="Type"}</th>
                  <th>{order action="index" order="mime" label="Mime"}</th>
                  <th>{order action="index" order="size" label="Size"}</th>
                  <th>{order action="index" order="f.uploader" label="Uploader"}</th>
                  <th width="180"></th>
                </tr>
              </thead>
              <tbody>
              {foreach from=$data item="item"}
                <tr>
                  <td>{$item.id|default}</td>
                  <td>{$item.name|default|basename}</td>
                  <td>{$item.type|default}</td>
                  <td>{$item.mime|default}</td>
                  <td>{$item.size|default} kb</td>
                  <td>{$item.uploader|fullname}</td>
                  <td>
                    <a class="btn btn-info btn-xs" href="/file/view/id/{$item.id|default}">View</a>
                    <a class="btn btn-warning btn-xs" href="/file/edit/id/{$item.id|default}">Edit</a>
                    <a class="btn btn-danger btn-xs" href="/file/remove/id/{$item.id|default}">Remove</a>
                  </td>
                </tr>
              {/foreach}
              </tbody>
            </table>
            {$this->paginationControl($data)}
          {else}
            <p>No data found</p>
          {/if}
    </div>
  </div>
          