
<div class="panel panel-green">
  <div class="panel-heading">{$pageLabel}</div>
    <div class="panel-body">
	<ul class="list-group">
              <li class="list-group-item">ID: {$file->getId()|default}</li>
              <li class="list-group-item">Name: {$file->getName()|default|basename}</li>
              <li class="list-group-item">Type: {$file->getType()|default} ({$file->getMime()|default})</li>
              <li class="list-group-item">Size: {$file->getSize()|default} kb</li>
              <li class="list-group-item">Uploader: {$file->getUploader()|fullname}</li>
              {if $file->getType() == 'picture'}
              <li class="list-group-item">
              <a href="/file/picture/{$file->getPath()}" target="_blank">
                <img src="/file/picture/100x100/{$file->getPath()}" alt="{$file->getName()}" />
              </a>
              </li>
              {else}
              <li class="list-group-item"><a href="/file/download/{$file->getPath()}" class="btn btn-primary">Download</a></li>
              {/if}
       </ul>
    </div>
  </div>