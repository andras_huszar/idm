
<div class="panel panel-green">
  <div class="panel-heading">{$pageLabel}</div>
    <div class="panel-body">
          {if !empty($data)}
          <table class="table table-hover table-striped">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>Name</th>
                  <th width="180"></th>
                </tr>
              </thead>
              <tbody>
              {foreach from=$data item="item"}
                <tr>
                  <td>{$item.id|default}</td>
                  <td>{$item.name|default}</td>
                  <td>
                    <a class="btn btn-warning btn-xs" href="/role/edit/id/{$item.id|default}">Edit</a>
                    <a class="btn btn-info btn-xs" href="/role/rights/id/{$item.id|default}">Rigths</a>
                    <a class="btn btn-danger btn-xs" href="/role/remove/id/{$item.id|default}">Remove</a>
                  </td> 
                </tr>
              {/foreach}
              </tbody>
            </table>
            {$this->paginationControl($data)}
          {else}
            <p>No data found</p>
          {/if}
    </div>
  </div>