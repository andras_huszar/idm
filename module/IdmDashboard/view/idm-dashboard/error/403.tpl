<div class="center-wrapper">
	<div class="panel panel-green">
		<div class="panel-heading">Error</div>
		<div class="panel-body">
			<div class="note note-danger">
				<h4 class="box-heading">403</h4>
				<p>{$reason}</p>
			</div>
		</div>
	</div>
</div>