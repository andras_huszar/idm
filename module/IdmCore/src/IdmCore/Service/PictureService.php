<?php
namespace Application\Service;

use IdmBackend\Service\PictureService as BackendPictureService;

class PictureService extends BackendPictureService
{
    protected $config = array(
    	''
    );

    const MAX_SIZE = 1920;

    const CHMOD_DIR = 0777;

    const CHMOD_FILE = 0666;

    const DIR_UPLOAD = 'public/upload/';

    public function filter($value)
    {
        $result = null;
        if (preg_match('@^[0-9a-f]/[0-9a-f]{2}/[_0-9a-z]+\.jpg$@', $value)) {
            $result = $value;
        } else {
            $validator = new \WootApp_Validate_Url();
            if ($validator->isValid($value)) {
                $id = md5($value);
                $path = $this->_getPath($id);
                $result = $this->_getExistingPath($path);
                if (!$result) {
                    $client = new \Zend_Http_Client($value, array(
                        'adapter' => 'Zend_Http_Client_Adapter_Curl',
                        'curloptions' => array(
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_SSL_VERIFYPEER => false
                        )
                    ));
                    $client->setStream();
                    $response = $client->request();
                    if ($response->getStatus() == 200) {
                        $filename = tempnam(self::DIR_UPLOAD, 'down');
                        file_put_contents($filename, $response->getBody());
                        if ($result = $this->_convert($filename, $path)) {
                            $this->create($id, $result, $value);
                        }
                        unlink($filename);
                    }
                }
            }
        }
        return $result;
    }

    public function store($filename)
    {
        $id = md5_file($filename);
        $path = $this->_getPath($id);
        $result = $this->_getExistingPath($path);
        if (! $result) {
            if ($result = $this->_convert($filename, $path)) {
                $this->create($id, $path);
            }
        }
        return $result;
    }

    protected function _getPath($hash, $root = self::DIR_UPLOAD)
    {
        $filename = null;
        if (preg_match('/^(.)(..)(.+)$/', $hash, $path)) {
            $filename = $path[1] . '/';
            if (! file_exists($root . $filename)) {
                mkdir($root . $filename, self::CHMOD_DIR);
                chmod($root . $filename, self::CHMOD_DIR);
            }
            $filename .= $path[2] . '/';
            if (! file_exists($root . $filename)) {
                mkdir($root . $filename, self::CHMOD_DIR);
                chmod($root . $filename, self::CHMOD_DIR);
            }
            $filename .= base_convert($path[3], 16, 36);
        }
        return $filename;
    }

    protected function _getExistingPath($path, $root = self::DIR_UPLOAD)
    {
        $exists = glob($root . $path . '*');
        return ! empty($exists) ? substr(array_shift($exists), strlen($root)) : 0;
    }

    protected function _convert($filename, $path, $width = self::MAX_SIZE, $height = self::MAX_SIZE, $root = self::DIR_UPLOAD)
    {
        $result = null;
        $data = @getimagesize($filename);
        if ($data) {
            @ini_set('memory_limit', '256M');
            switch ($data[2]) {
                case IMAGETYPE_JPEG:
                    $source = imagecreatefromjpeg($filename);
                    break;
                case IMAGETYPE_PNG:
                    $source = imagecreatefrompng($filename);
                    break;
                case IMAGETYPE_GIF:
                    $source = imagecreatefromgif($filename);
                    break;
            }
        }
        if (! empty($source)) {
            $sw = $data[0];
            $sh = $data[1];
            $tw = $width;
            $th = round($width / $sw * $sh);
            if ($th > $height) {
                $th = $height;
                $tw = round($height / $sh * $sw);
            }
            if ($sw <= $tw && $sh <= $th) {
                $th = $sh;
                $tw = $sw;
            }
            $path .= "_{$tw}x{$th}.jpg";
            $target = imagecreatetruecolor($tw, $th);
            imagecopyresampled($target, $source, 0, 0, 0, 0, $tw, $th, $sw, $sh);
            if (imagejpeg($target, $root . $path, 95)) {
                chmod($root . $path, self::CHMOD_FILE);
                $result = $path;
            }
            imagedestroy($target);
        }
        return $result;
    }

    public function getAttributes($src, $thumb = false)
    {
        $attrs = array();

        $thumbProps = null;
        if ($thumb && isset($this->config[$thumb])) {
            preg_match('/^(\d+)x(\d+)([a-z]*)$/', $this->config[$thumb], $thumbProps);
            $src = $this->config[$thumb].'/'.$src;
        }

        $attrs['src'] = $src;

        if (preg_match('/_(\d+)x(\d+)\.jpg$/', $src, $size)) {
            $sw = (int) $size[1];
            $sh = (int) $size[2];
            if (!$thumbProps) {
                $attrs['width'] = $sw;
                $attrs['height'] = $sh;
            } else {
                $width = (int) $thumbProps[1];
                $height = (int) $thumbProps[2];
                if (empty($thumbProps[3])) {
                    $attrs['width'] = $thumbProps[1];
                    $attrs['height'] = $thumbProps[2];
                } else {
                    switch ($width) {
                    	case 0:
                    	    $tw = round($height / $sh * $sw);
                    	    $th = $height;
                    	    break;
                    	case 1:
                    	    $tw = round($sw / $height);
                    	    $th = round($sh / $height);
                    	    break;
                    	default:
                    	    $tw = $width;
                    	    $th = round($width / $sw * $sh);
                    	    if ($height > 0 && $th > $height) {
                    	        $th = $height;
                    	        $tw = round($height / $sh * $sw);
                    	    }
                    	    break;
                    }
                    if ($sw > $tw || $sh > $th) {
                        $attrs['width'] = $tw;
                        $attrs['height'] = $th;
                    } else {
                        $attrs['width'] = $sw;
                        $attrs['height'] = $sh;
                    }
                }
            }
        }
        return $attrs;
    }

}