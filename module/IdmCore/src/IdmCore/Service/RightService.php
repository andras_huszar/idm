<?php
namespace IdmCore\Service;

use IdmUser\Domain\Service\RightService as DomainService;
use IdmUser\Domain\Command\UpdateRightsFromArray;

class RightService extends DomainService
{

    /**
     * @var string
     */
    protected $entityClass = 'IdmUser\Domain\Entity\Right';

    /**
     * @return void|boolean
     */
    public function syncToDB()
    {
        $config = $this->getServiceLocator()->get('config');
        if (empty($config['navigation'])) {
            return;
        }

        $navs = $config['navigation'];
        $rights = array();
        foreach ($navs as $name => $pages) {
            $this->getNavPages($pages, $rights);
        }

        $conn = $this->getEntityManager()->getConnection();
        $cmd = new SyncRightsFromArray($conn);
        $count = $cmd->execute($rights);

        if ($count > 0) {
            $this->getLogger()->info("$count new right ");
            return true;
        }
    }

    /**
     * @param array $pages
     * @param array $result
     */
    protected function getNavPages($pages, &$result = array())
    {
    	foreach ($pages as $page) {
    	    $result[] = $page;
    	    if (isset($page['pages'])) {
    	        $this->getNavPages($page['pages'], $result);
    	        unset($page['pages']);
    	    }
    	}
    }

}