<?php
namespace IdmCore\Authentication\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use IdmCore\Authentication\AuthenticationAdapter;

class AuthenticationAdapterFactory implements FactoryInterface
{

    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $userService = $services->get('IdmUser\Domain\Service\UserService');
        return new AuthenticationAdapter($userService);
    }

}