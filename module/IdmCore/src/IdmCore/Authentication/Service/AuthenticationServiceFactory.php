<?php
namespace IdmCore\Authentication\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Session\Container;
use IdmCore\Authentication\AuthStorage;

class AuthenticationServiceFactory implements FactoryInterface
{

    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $storage = new AuthStorage();
        $adapter = $services->get('IdmCore\AuthenticationAdapter');
        return new AuthenticationService($storage, $adapter);
    }

}