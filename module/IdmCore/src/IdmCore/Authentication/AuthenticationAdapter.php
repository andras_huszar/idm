<?php
namespace IdmCore\Authentication;

use Zend\Authentication\Adapter\AbstractAdapter;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use IdmUser\Domain\Service\IdentityService;

class AuthenticationAdapter extends AbstractAdapter
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserService $service
     */
    public function __construct($userService)
    {
        $this->userService = $userService;
    }

    /**
     * @see \Zend\Authentication\Adapter\AdapterInterface::authenticate()
     */
    public function authenticate()
    {
        $code = Result::FAILURE;
        $identity = null;

        $email = $this->getIdentity();
        $password = $this->getCredential();

        $user = $this->userService->findByEmail($email);

        if ($user) {
            if ($this->userService->verifyPassword($user, $password)) {
                $code = Result::SUCCESS;
                $identity = $user->getId();
            } else {
                $code = Result::FAILURE_CREDENTIAL_INVALID;
            }
        } else {
        	$code = Result::FAILURE_IDENTITY_NOT_FOUND;
        }
        return new Result($code, $identity);
    }
}
