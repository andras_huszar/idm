<?php
namespace IdmCore\Authentication;

use Zend\Authentication\Storage\Session;
use Zend\Session\Container;

class AuthStorage extends Session
{

    public function __construct()
    {
    	parent::__construct('idm_auth');
    }

    public function setRememberMe($rememberMe = 0, $time = 1209600)
    {
        if ($rememberMe == 1) {
            $this->session->getManager()->rememberMe($time);
        }
    }

    public function forgetMe()
    {
        $this->session->getManager()->forgetMe();
    }

}
