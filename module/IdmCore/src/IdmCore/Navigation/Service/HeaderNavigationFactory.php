<?php
namespace IdmCore\Navigation\Service;

use Zend\Navigation\Service\AbstractNavigationFactory;

class HeaderNavigationFactory extends AbstractNavigationFactory
{

    protected function getName()
    {
        return 'header';
    }

}