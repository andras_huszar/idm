<?php
namespace IdmCore\View\Helper\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use IdmCore\View\Helper\Acl;

class AclFactory implements FactoryInterface
{

	/**
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService(ServiceLocatorInterface $helpers)
	{
	    $services = $helpers->getServiceLocator();
	    $acl = $services->get('IdmCore\Acl');
	    $role = $services->get('CurrentRole');
	    return new Acl($acl, $role);
	}

}