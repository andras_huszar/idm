<?php
namespace IdmCore\View\Helper\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use IdmCore\View\Helper\Params;

class ParamsFactory implements FactoryInterface
{

	/**
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService(ServiceLocatorInterface $helpers)
	{
	    $services = $helpers->getServiceLocator();
	    $app = $services->get('Application');
	    return new Params($app->getRequest(), $app->getMvcEvent());
	}

}