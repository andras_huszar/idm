<?php
namespace IdmCore\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Permissions\Acl\Acl as ZendAcl;

class Acl extends AbstractHelper
{

    protected $acl;

    public function __construct(ZendAcl $acl, $role)
    {
        $this->acl = $acl;
        $this->role = $role;
    }

    public function isAllowed($resource = null, $privilege = null)
    {
    	return $this->acl->isAllowed($this->role, $resource, $privilege);
    }

}