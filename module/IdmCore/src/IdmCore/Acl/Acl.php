<?php
namespace IdmCore\Acl;

use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class Acl extends ZendAcl
{

    const ROLE_DEFAULT = 'default';
    const ROLE_GUEST   = 'guest';

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        if (isset($config['roles'])) {
        	$this->addRoles($config['roles']);
        }
        if (isset($config['resources'])) {
        	$this->addResources($config['resources']);
        }
    }

    public function addRoles($roles)
    {
        foreach ($roles as $role => $parents) {
            if (!$this->hasRole($role)) {
        	    $this->addRole(new Role($role), (array)$parents);
            }
        }
    }

    public function addResources($resources)
    {
        foreach ($resources as $role => $roleResources) {
            foreach ($roleResources as $resource) {
                list($resourceName, $privileges) = $resource;
                if (!$this->hasResource($resourceName)) {
                    $this->addResource(new Resource($resourceName));
                }
                $this->allow($role, $resourceName, $privileges);
            }
        }
    }

}