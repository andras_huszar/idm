<?php
namespace IdmCore\Acl\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use IdmCore\Acl\Acl;

class AclFactory implements FactoryInterface
{

    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $config = $services->get('Config');
        $config = isset($config['acl']) ? $config['acl'] : array();

        $em = $services->get('Doctrine\ORM\EntityManager');
        $repository = $em->getRepository('IdmUser\Domain\Entity\Role');

        /* @var Doctrine\ORM\QueryBuilder */
        $qb = $repository->createQueryBuilder('r');
        $roles = $qb->getQuery()->getArrayResult();

        $acl = new Acl($config);
        return $acl;
    }

}