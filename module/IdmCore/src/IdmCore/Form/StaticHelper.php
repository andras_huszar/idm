<?php
namespace IdmCore\Form;

use Zend\Validator\Date;
use Zend\Filter\Callback;

class StaticHelper
{

    public static function dateSelect($name, $required = false)
    {
        return array(
            'name' => $name,
            'required' => $required,
            'filters' => array(
                new Callback(array(
                    'callback' => function ($date) {
                        // Convert the date to a specific format
                        if (is_array($date) && empty($date['year']) && empty($date['month']) && empty($date['day'])) {
                            return $date['year'].'-'.$date['month'].'-'.$date['day'];
                        } elseif (strtotime($date)) {
                            return $date;
                        }
                        return false;
                    }
                ))
            )
        );
    }

}