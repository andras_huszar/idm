<?php
namespace IdmCore\Form\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormHidden;
use Zend\Form\View\Helper\FormLabel;
use Zend\Form\View\Helper\FormInput;
use IdmCore\Form\Element;
use TwbBundle\Form\View\Helper\TwbBundleFormElement;

class FormPicture extends TwbBundleFormElement
{

    /**
     * @see \Zend\Form\View\Helper\FormHidden::getType()
     */
    protected function getType(ElementInterface $element)
    {
        return 'file';
    }

    public function render(ElementInterface $element)
    {
    	$value = $element->getValue();
		$url = $this->getView()->url('application/file', array(
			'action' => 'upload',
   			'type' => 'picture'
    	));

		$extras = ' data-thumbnail="150x150"';

    	$out = '<div data-picture-upload="'.$url.'"'.$extras.'>';
    	$out .= '<div class="target-area">';
    	$out .= '<label></label>';
    	if ($value) {
			$out .= '<img class="uploaded-picture" src="upload/' . $value . '" alt="" />';
		}
        $out .= '</div>';

    	$out .= parent::render($element);
    	$out .= '<input class="picture" type="file" name="file" />';

        $out .= '</div>';

    	return $out;
    }

}