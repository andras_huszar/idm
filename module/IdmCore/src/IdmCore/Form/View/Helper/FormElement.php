<?php
namespace IdmCore\Form\View\Helper;

use Zend\Form\ElementInterface;
use TwbBundle\Form\View\Helper\TwbBundleFormElement;
use IdmCore\Form\Element\Picture;

class FormElement extends TwbBundleFormElement
{

    public function render(ElementInterface $element)
    {
        $renderer = $this->getView();
        if (!method_exists($renderer, 'plugin')) {
            // Bail early if renderer is not pluggable
            return '';
        }

        if ($element instanceof Picture) {
            $helper = $renderer->plugin('formpicture');
            return $helper($element);
        }

        return parent::render($element);
    }

}