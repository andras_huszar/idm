<?php
namespace IdmCore\Form\View\Helper;

use Zend\Form\ElementInterface;
use TwbBundle\Form\View\Helper\TwbBundleFormRow;

class FormRow extends TwbBundleFormRow
{

    /**
     * @see \Zend\Form\View\Helper\FormRow::render()
     * @param \Zend\Form\ElementInterface $element
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $privilege = $element->getAttribute('privilege');
        if ($privilege) {
            $view = $this->getView();
            $resource = strtolower($view->params('CurrentResource'));
            if (!$view->acl()->isAllowed($resource, $privilege)) {
                return;
            }
        }
        return parent::render($element);
    }

}