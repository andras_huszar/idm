<?php
namespace IdmCore\ServiceManager;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\InitializerInterface;

class EntityManagerInitializer implements InitializerInterface
{

    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        if ($instance instanceof EntityManagerAwareInterface) {
            $instance->setEntityManager($serviceLocator->get('Doctrine\ORM\EntityManager'));
        }
    }

}