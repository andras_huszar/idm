<h1>500</h1>

<h2>Additional information</h2>
<pre>
{$exception->getFile()}:{$exception->getLine()}
{$exception->getMessage()}
{$exception->getTraceAsString()}
</pre>

<h2>Previous exceptions</h2>
{assign var="e" value=$exception->getPrevious()}
{if $e}
{while $e}
<pre>
{$e->getFile()}:{$e->getLine()}
{$e->getMessage()}
{$e->getTraceAsString()}
</pre>
{assign var="e" value=$e->getPrevious()}
{/while}
{/if}