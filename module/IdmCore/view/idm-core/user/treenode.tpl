
		<ul>
		{foreach from=$data item="group"}
		  <li>
		  	<a href="#">
		  		<strong>{$group->getName()}</strong>
		  		{assign var="leader" value=$group->getLeader()}
		  		{if $leader}<br/>{$leader->getFirstName()} {$leader->getLastName()}{/if}
		  		
		    	{assign var="members" value=$group->getMembers()}
		    	{if $members}
		    	<br/>
		    	{foreach from=$members item="member"}
		    		{if $member != $leader}
		    		{$member->getFirstName()} {$member->getLastName()}
		    		{/if}
		    	{/foreach}
		    	{/if}
		  	</a>
		    {assign var="children" value=$group->getChildren()}
		    {if $children}
		      {include file="treenode.tpl" data=$children}
		    {/if}
		  </li>
		{/foreach}
		</ul>
		