          
          <h2 class="sub-header">Users</h2>
          
          {$this->form($form, 'inline')}
          
          {if !empty($data)}
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>{order action="index" order="fullname" label="Full name"}</th>
                  <th>{order action="index" order="email" label="Email"}</th>
                  <th>{order action="index" order="group" label="Group"}</th>
                  <th>{order action="index" order="group_leader" label="Pincipal"}</th>
                  <th>{order action="index" order="is_active" label="Status"}</th>
                  <th width=100"></th>
                </tr>
              </thead>
              <tbody>
              {foreach from=$data item="item"}
                <tr>
                  <td>{$item.id|default}</td>
                  <td>{$item|fullname}</td>
                  <td>{$item.email|default}</td>
                  <td>{$item.group.name|default:"Uncategorized"}</td>
                  <td>{$item.principal|fullname}</td>
                  {*<td>{foreach from=$item.roles|default:[] item="role"}
                    {$role.name}
                  {/foreach}</td>*}
                  <td>{if $item.is_active}Active{else}Inactive{/if}</td>
                  <td>
                    <a href="/user/edit/id/{$item.id|default}">Edit</a>
                    <a href="/user/remove/id/{$item.id|default}">Remove</a>
                  </td> 
                </tr>
              {/foreach}
              </tbody>
            </table>
          </div>
          {$this->paginationControl($data)}
          {else}
          <p>No data found</p>
          {/if}
          