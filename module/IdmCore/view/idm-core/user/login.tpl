
      {$this->headLink()->appendStylesheet('css/signin.css')}
      
      <div class="form-signin">
      <h2 class="form-signin-heading">Please sign in</h2> 
      {$this->form()->render($form)}
      <p>Not a member? <a href="{url route="application/register"}">Register</a></p>
      </div>
      
