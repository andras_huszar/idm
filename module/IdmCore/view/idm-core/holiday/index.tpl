          
          <h2 class="sub-header">Holidays</h2>
          {if !empty($data)}
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>{order action="index" order="r.name" label="Name"}</th>
                  <th>{order action="index" order="h.start_date" label="Start date"}</th>
                  <th>{order action="index" order="h.end_date" label="End date"}</th>
                  <th>{order action="index" order="h.status" label="Status"}</th>
                  <th>{order action="index" order="h.comment" label="Comment"}</th>
                  <th width=10%"></th>
                </tr>
              </thead>
              <tbody>
              {foreach from=$data item="item"}
                <tr>
                  <td>{$item.id|default}</td>
                  <td>{$item.requester|fullname}</td>
                  <td>{$item.start_date|default|date}</td>
                  <td>{$item.end_date|default|date}</td>
                  <td>{$item.status|default|holiday:'status'}</td>
                  <td>{$item.comment|default}</td>
                  <td>
                    <a href="/holiday/edit/id/{$item.id|default}">Edit</a>
                    <a href="/holiday/remove/id/{$item.id|default}">Remove</a>
                  </td> 
                </tr>
              {/foreach}
              </tbody>
            </table>
          </div>
          {$this->paginationControl($data)}
          {else}
          <p>No data found</p>
          {/if}