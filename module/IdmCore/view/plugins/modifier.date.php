<?php
use IdmBackend\Service\UserService;

function smarty_modifier_date($date, $format = 'Y-m-d')
{
    if ($date instanceof \DateTime) {
        return $date->format($format);
    } elseif (is_numeric($date)) {
        return date($format, $date);
    } else {
        return date($format, strtotime($date));
    }
}