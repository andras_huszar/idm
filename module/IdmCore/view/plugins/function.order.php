<?php

use Zend\Stdlib\ArrayUtils;

function smarty_function_order($params, &$smarty) {
	/* @var $view \Zend\View\Model\ViewModel */
	$view = $smarty->getTemplateVars('this');
	$route = null;
	if (isset($params['route'])) {
		$route = $params['route'];
		unset($params['route']);
	}
	$order = $view->params()->fromRoute('order');
	$dir = $view->params()->fromRoute('dir');
	if (!$dir || $order != $params['order'] || $dir == 'desc') {
	    $params['dir'] = 'asc';
	} else {
	    $params['dir'] = 'desc';
	}
	$url = $view->url($route, $params);
	return '<a href="'.$url.'">'.$params['label'].'</a>';
}
