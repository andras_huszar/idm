<?php

function smarty_function_url($params, &$smarty) {
	$route = null;
	if (isset($params['route'])) {
		$route = $params['route'];
		unset($params['route']);
	}
	return $smarty->getTemplateVars('this')->url($route, $params, empty($params['reset']));
}
