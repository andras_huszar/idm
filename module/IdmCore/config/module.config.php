<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'IdmCore\Captcha\Controller\Captcha' => 'IdmCore\Captcha\Controller\CaptchaController',
        )
    ),
    'controller_plugins' => array(
    ),
    'form_elements' => array(
        'invokables' => array(
            'picture' => 'IdmCore\Form\Element\Picture'
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory'
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator'
        ),
        'factories' => array(
            'IdmCore\AuthenticationService' => 'IdmCore\Authentication\Service\AuthenticationServiceFactory',
            'IdmCore\AuthenticationAdapter' => 'IdmCore\Authentication\Service\AuthenticationAdapterFactory',
            'IdmCore\Acl'                   => 'IdmCore\Acl\Service\AclFactory',
            'IdmCore\DefaultNavigation'     => 'Zend\Navigation\Service\DefaultNavigationFactory',
            'IdmCore\HeaderNavigation'      => 'IdmCore\Navigation\Service\HeaderNavigationFactory'
        ),
        'initializers' => array(
        	'IdmCore\ServiceManager\EntityManagerInitializer',
        ),
    ),
    'gk_smarty' => array(
        'smarty_options' => array(
            'plugins_dir' => array(
            	__DIR__ . '/../../../vendor/smarty/smarty/libs/plugins',
            	__DIR__ . '/../../../vendor/smarty/smarty/distribution/libs/plugins',
            ),
        ),
        // Fix for partial renderer
        'helper_manager' => array(
            'invokables' => array(
                'partial' => 'Zend\View\Helper\Partial',
                'paginationcontrol' => 'Zend\View\Helper\PaginationControl',
            ),
        )
    ),
    'view_helpers' => array(
        'factories' => array(
            'acl'    => 'IdmCore\View\Helper\Service\AclFactory',
            'params' => 'IdmCore\View\Helper\Service\ParamsFactory'
        ),
        'invokables' => array(
            'formrow'     => 'IdmCore\Form\View\Helper\FormRow',
            'formelement' => 'IdmCore\Form\View\Helper\FormElement',
            'formPicture' => 'IdmCore\Form\View\Helper\FormPicture'
        )
    ),
);