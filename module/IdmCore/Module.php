<?php
namespace IdmCore;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\View\Helper\PaginationControl;
use Zend\ModuleManager\ModuleManager;
use Zend\Session\Config\SessionConfig;
use Zend\Session\Container;
use Zend\Session\SessionManager;

class Module
{
    public static $sm;

    protected $currentPage;

    public function getConfig()
    {
        $config = include __DIR__ . '/config/module.config.php';
        // Add smarty view plugin dirs for all module
        $config['gk_smarty']['smarty_options']['plugins_dir'] = array_merge(
            $config['gk_smarty']['smarty_options']['plugins_dir'],
            glob(__DIR__ . '/../*/view/plugins', GLOB_ONLYDIR)
        );
        return $config;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function init(ModuleManager $e)
    {
        // Set default pagination template
        PaginationControl::setDefaultViewPartial('paginator');
    }

    public function onBootstrap(MvcEvent $e)
    {
        $evm  = $e->getApplication()->getEventManager();
        $sm  = $e->getApplication()->getServiceManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($evm);

        if ($e->getResponse() instanceof \Zend\Http\Response) {
            $evm->attach(MvcEvent::EVENT_DISPATCH, array($this, 'checkUser'), 100);
            $evm->attach(MvcEvent::EVENT_RENDER, array($this, 'setCurrentModule'), 100);
            $evm->attach(MvcEvent::EVENT_RENDER, array($this, 'setCurrentPage'), 100);
            $evm->attach(MvcEvent::EVENT_RENDER, array($this, 'setLayoutTitle'), 100);
            $evm->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this,'setErrorLayot'), 100);
            $this->initSessions($e);
        }

        self::$sm = $sm;
    }

    public function initSessions(MvcEvent $e)
    {
        $sm = $e->getApplication()->getServiceManager();
        $config = $sm->get('config');
        $config = isset($config['session']) ? $config['session'] : array();

        $sessionConfig = new SessionConfig();
        if (isset($config['session'])) {
            $sessionConfig->setOptions($config['session']);
        }

        $storage = null;
        if ($sm->has('session_storage', false)) {
            $storage = $sm->get('session_storage');
        }
        $saveHandler = null;
        if ($sm->has('session_save_handler', false)) {
            $saveHandler = $sm->get('session_save_handler');
        }
        $sessionManager = new SessionManager($sessionConfig, $storage, $saveHandler);
		//var_dump($_SESSION, $_COOKIE);
        Container::setDefaultManager($sessionManager);
    }

    public function checkUser(MvcEvent $e)
    {
        $app = $e->getApplication();
        $sm = $app->getServiceManager();

        /* @var $routeMatch \Zend\Mvc\Router\RouteMatch */
        $routeMatch = $e->getRouteMatch();
        $routeName = $routeMatch->getMatchedRouteName();
        $names = explode('/', $routeName);

        // Protect only application module
        if ($names[0] !== 'application') {
            return;
        }

        // Check identity
        $authService = $sm->get('IdmCore\AuthenticationService');
        if ($authService->hasIdentity()) {
            $identityService = $sm->get('IdmUser\Domain\Service\IdentityService');
            $user = $identityService->getIdentityUser();
            $e->setParam('CurrentUser', $user);
            $role = new GenericRole('admin');
        } else {
            $e->setParam('CurrentUser', new \stdClass());
            $role = new GenericRole('guest');
        }

        $sm->setService('CurrentRole', $role);

        // Check access
        // @todo get acl config from db
        $resource = self::resourceName($routeMatch);
        $privilege = $routeMatch->getParam('action');

        $e->setParam('CurrentRole', $role);
        $e->setParam('CurrentResource', $resource);

        $acl = $sm->get('IdmCore\Acl');
        if (! $acl->isAllowed($role, $resource, $privilege)) {
            if ($role->getRoleId() === 'guest') {
                $e->setRouteMatch(new RouteMatch(array(
                    'controller' => 'IdmUser\Presentation\Controller\User',
                    'action' => 'login',
                )));
            } else {
                $e->getResponse()->setStatusCode(403);
                $e->setRouteMatch(new RouteMatch(array(
                    'controller' => 'IdmUser\Presentation\Controller\User',
                    'action' => 'denied',
                )));
            }
        }
    }

    public static function resourceName($routeMatch)
    {
        $name = $routeMatch->getParam('__CONTROLLER__');
        if ($name === null) {
            $name = $routeMatch->getParam('controller');
        }
        $module = ltrim(substr(strtolower($name), 0, strpos($name, '\\')), 'idm');
        $controller = substr(strtolower($name), strrpos($name, '\\')+1);
        if ($module == 'application') {
            $module = 'index';
        }
        return $module . '/' . $controller;
    }

    public function setCurrentModule(MvcEvent $e)
    {
        $parts = explode('/', $e->getParam('CurrentResource'));
        $moduleName = ucfirst($parts[0]);
        $e->setParam('CurrentModuleName', $moduleName);
    }

    public function setCurrentPage(MvcEvent $e)
    {
    	$services = $e->getApplication()->getServiceManager();

    	// Find active page
    	/* @var $navigation \Zend\Navigation\Navigation */
    	$navigation = $services->get('IdmCore\DefaultNavigation');

    	$page = $navigation->findBy('active', 1);
    	if (! $page) {
    		$e->getResponse()->setStatusCode(404);
    		return;
    	}
    	$this->currentPage = $page;

    	$services = $e->getApplication()->getServiceManager();
        /* @var $viewModel \Zend\View\Model\ViewModel */
        $viewModel = $e->getViewModel();
        foreach ($viewModel->getChildrenByCaptureTo('content') as $contentModel) {
            $contentModel->setVariable('pageLabel', $this->currentPage->getLabel());
        }
    }

    public function setLayoutTitle(MvcEvent $e)
    {
    	if (! $this->currentPage) {
    		return;
    	}

        $siteName = 'IDM';
        $services = $e->getApplication()->getServiceManager();
        $viewHelperManager = $services->get('viewHelperManager');
        $headTitleHelper = $viewHelperManager->get('headTitle');

        $headTitleHelper->setSeparator(' - ');
        $headTitleHelper->append($this->currentPage->getLabel());
        $headTitleHelper->append($e->getParam('CurrentModuleName'));
        $headTitleHelper->append($siteName);
    }

    public function setErrorLayot(MvcEvent $e)
    {
        $viewModel = $e->getViewModel();
        $viewModel->setTemplate('layout/blank');
    }

}
