<?php

namespace IdmHoliday\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Holiday
 *
 * @ORM\Table(name="holiday")
 * @ORM\Entity
 */
class Holiday
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date")
     */
    private $start_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date", nullable=true)
     */
    private $end_date;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string")
     */
    private $reason;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string")
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var \IdmUser\Domain\Entity\User
     *
     * @ORM\OneToOne(targetEntity="IdmUser\Domain\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="replacement_id", referencedColumnName="id", unique=true)
     * })
     */
    private $replacement;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="IdmUser\Domain\Entity\User", mappedBy="possible_replacements")
     */
    private $possible_replacements;

    /**
     * @var \IdmUser\Domain\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="IdmUser\Domain\Entity\User", inversedBy="holidays")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="requester_id", referencedColumnName="id")
     * })
     */
    private $requester;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->possible_replacements = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start_date
     *
     * @param \DateTime $startDate
     * @return Holiday
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get start_date
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set end_date
     *
     * @param \DateTime $endDate
     * @return Holiday
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;

        return $this;
    }

    /**
     * Get end_date
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return Holiday
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string 
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Holiday
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Holiday
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set replacement
     *
     * @param \IdmUser\Domain\Entity\User $replacement
     * @return Holiday
     */
    public function setReplacement(\IdmUser\Domain\Entity\User $replacement = null)
    {
        $this->replacement = $replacement;

        return $this;
    }

    /**
     * Get replacement
     *
     * @return \IdmUser\Domain\Entity\User 
     */
    public function getReplacement()
    {
        return $this->replacement;
    }

    /**
     * Add possible_replacements
     *
     * @param \IdmUser\Domain\Entity\User $possibleReplacements
     * @return Holiday
     */
    public function addPossibleReplacement(\IdmUser\Domain\Entity\User $possibleReplacements)
    {
        $this->possible_replacements[] = $possibleReplacements;

        return $this;
    }

    /**
     * Remove possible_replacements
     *
     * @param \IdmUser\Domain\Entity\User $possibleReplacements
     */
    public function removePossibleReplacement(\IdmUser\Domain\Entity\User $possibleReplacements)
    {
        $this->possible_replacements->removeElement($possibleReplacements);
    }

    /**
     * Get possible_replacements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPossibleReplacements()
    {
        return $this->possible_replacements;
    }

    /**
     * Set requester
     *
     * @param \IdmUser\Domain\Entity\User $requester
     * @return Holiday
     */
    public function setRequester(\IdmUser\Domain\Entity\User $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return \IdmUser\Domain\Entity\User 
     */
    public function getRequester()
    {
        return $this->requester;
    }
}
