<?php
namespace IdmHoliday\Domain\Filter;

use Zend\InputFilter\InputFilter;
use Doctrine\ORM\EntityManager;
use Zend\Validator\Digits;
use Zend\Validator\Callback;
use Zend\Filter\Boolean;
use IdmCore\Form\StaticHelper;

class HolidayFilter extends InputFilter
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;

        $this->add(array(
            'name' => 'id',
            'required' => false,
            'validators' => array(
                new Digits(),
            ),
        ));
        $this->add(array(
            'name' => 'status',
            'required' => false,
            'filters' => array(
                new Boolean()
            )
        ));
        $this->add(array(
            'name' => 'group',
            'required' => false
        ));
        $this->add(array(
            'name' => 'possible_replacements',
            'required' => false
        ));
        $startDate = StaticHelper::dateSelect('start_date');
        $this->add($startDate);
        $endDate = StaticHelper::dateSelect('fired_at');
        $endDate['validators'][] = new Callback(array(
            'message' => array(
                Callback::INVALID_VALUE => 'Invalid period is given.',
            ),
            'callback' => function($value, $context) {
                if ($value) {
                    if (!array_filter($context['start_date'])) {
                        return false;
                    }
                    return strtotime($value) > strtotime(implode('-', $context['start_date']));
                }
                return true;
            }
        ));
        $this->add($endDate);
    }
}