<?php
namespace IdmHoliday\Domain\Service;

use IdmBackend\Domain\AbstractEntityService;
use IdmHoliday\Domain\Entity\Holiday;
use IdmUser\Domain\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Zend\View\Model\ViewModel;

class HolidayService extends AbstractEntityService
{
    const StatusPending  = 0;
    const StatusAccepted = 1;
    const StatusDeclined = 2;

    /**
     * @var string
     */
    protected $entityClass = 'IdmHoliday\Domain\Entity\Holiday';

    /**
     * @var string
     */
    protected $inputFilterClass = 'IdmHoliday\Domain\Filter\HolidayFilter';

    public function prePersist(Holiday $holiday, LifecycleEventArgs $eventArgs)
    {
        if ($holiday->getRequester() === null) {
            $currentUser = $this->getServiceLocator()
                ->get('IdmUser\Domain\Service\IdentityService')
                ->getIdentityUser();
            $holiday->setRequester($currentUser);
            $this->sendNotificationEmail($currentUser);
        }
    }

    public function preUpdate(Holiday $holiday, LifecycleEventArgs $eventArgs)
    {
    }

    public function sendNotificationEmail(User $user)
    {
        $addressee = $user->getPrincipal();
        if ($addressee) {
            $messenger = $this->getServiceLocator()->get('Application\Service\MessageService');
            $viewModel = new ViewModel(array(
                'user' => $user,
                'addressee' => $addressee,
            ));
            $viewModel->setTemplate('messages/templates/holiday_request');
            $messenger->send($addressee, $viewModel);
        }
    }
}