<?php
namespace IdmHoliday\Presentation\Form;

use Zend\Form\Form;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Form\Element\ObjectMultiCheckbox;
use IdmHoliday\Domain\Service\HolidayService;
use IdmCore\Form\StaticHelper;

class HolidayForm extends Form
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        parent::__construct('user-form');

        $this->em = $em;

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'reason',
        	'type' => 'select',
            'options' => array(
                'label' => 'Reason',
            	'value_options' => array(
            		1 => 'Disease',
            		2 => 'Education',
            		3 => 'Vacation',
            	)
            ),
            'attributes' => array(
                'type' => 'select',
                'class' => 'reason',
            )
        ));
        $this->add(array(
            'name' => 'start_date',
            'type' => 'date-select',
            'allowEmpty' => true,
            'options' => array(
                'label' => 'Start date',
                'create_empty_option' => true,
                'min_year' => date('Y')-1,
                'max_year' => date('Y')+2
            ),
            'attributes' => array(
                'class' => 'start_date',
            )
        ));
        $this->add(array(
            'name' => 'end_date',
            'type' => 'date-select',
            'allowEmpty' => true,
            'options' => array(
                'label' => 'End date',
                'create_empty_option' => true,
                'min_year' => date('Y')-1,
                'max_year' => date('Y')+2
            ),
            'attributes' => array(
                'class' => 'end_date',
            )
        ));
        $this->add(array(
            'name' => 'comment',
            'type' => 'textarea',
            'options' => array(
                'label' => 'Comment',
            ),
            'attributes' => array(
                'class' => 'comment',
            )
        ));
        $this->add(array(
            'name' => 'status',
        	'type' => 'select',
            'options' => array(
                'label' => 'Status',
            	'value_options' => array(
            		HolidayService::StatusPending => 'Pending',
            		HolidayService::StatusAccepted => 'Accepted',
            		HolidayService::StatusDeclined => 'Declined',
            	)
            ),
            'attributes' => array(
                'type' => 'select',
                'class' => 'status',
                'privilege' => 'admin_requests'
            )
        ));
        $this->add(array(
            'name' => 'requester',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'label' => 'Requester',
                'object_manager' => $this->em,
                'target_class' => 'IdmUser\Domain\Entity\User',
                'property' => 'email'
            ),
            'attributes' => array(
                'class' => 'requester',
                'privilege' => 'admin_requests'
            )
        ));
        $this->add(array(
            'name' => 'possible_replacements',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'label' => 'Replacements',
                'object_manager' => $this->em,
                'target_class' => 'IdmUser\Domain\Entity\User',
                'property' => 'email'
            ),
            'attributes' => array(
                'class' => 'possible_replacements',
                'multiple' => 'multiple',
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Submit'
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-primary'
            )
        ));
    }
}