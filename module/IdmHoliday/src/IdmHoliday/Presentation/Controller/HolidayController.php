<?php
namespace IdmHoliday\Presentation\Controller;

use IdmBackend\Presentation\Controller\AbstractCrudController;
use Zend\View\Model\ViewModel;
use IdmHoliday\Presentation\Form\HolidayForm;

class HolidayController extends AbstractCrudController
{
    /**
     * @return \IdmHoliday\Domain\Service\HolidayService
     */
    public function getService()
    {
    	return $this->getServiceLocator()->get('IdmHoliday\Domain\Service\HolidayService');
    }

    /**
     * @see \IdmBackend\Presentation\Controller\AbstractCrudController::getForm()
     */
    public function getForm()
    {
        return new HolidayForm($this->getEntityManager());
    }

    /**
     * @return ViewModel
     */
    protected function getQuery()
    {
        $qb = $this->getService()->createQueryBuilder();
        $qb->select('h, partial r.{id,first_name,last_name}')
            ->leftJoin('h.requester', 'r')
            ->orderBy('h.id', 'asc');
		$order = $this->params('order');
        if ($order) {
            $qb->orderBy($order, $this->params('dir', 'asc'));
        }
        return $qb->getQuery();
    }
}