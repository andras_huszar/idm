<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'idm_holiday_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/IdmHoliday/Domain/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'IdmHoliday\Domain\Entity' => 'idm_holiday_driver'
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'IdmHoliday\Presentation\Controller\Holiday' => 'IdmHoliday\Presentation\Controller\HolidayController',
        )
    ),
    'service_manager' => array(
        'invokables' => array(
            'IdmHoliday\Domain\Service\HolidayService' => 'IdmHoliday\Domain\Service\HolidayService',
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    ),
    'acl' => include __DIR__ . '/acl.config.php',
    'navigation' => include __DIR__ . '/navigation.config.php',
    'router' => include __DIR__ . '/routes.config.php'
);