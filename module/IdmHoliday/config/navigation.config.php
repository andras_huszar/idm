<?php
return array(
    'default' => array(
        array(
            'label' => 'Holiday',
            'route' => 'application/holiday',
            'controller' => 'IdmHoliday\Presentation\Controller\Holiday',
            'action' => 'index',
            'pages' => array(
                array(
                    'label' => 'Add holiday',
                    'route' => 'application/holiday',
                    'controller' => 'IdmHoliday\Presentation\Controller\Holiday',
                    'action' => 'add'
                ),
                array(
                    'label' => 'Edit holiday',
                    'route' => 'application/holiday',
                    'controller' => 'IdmHoliday\Presentation\Controller\Holiday',
                    'action' => 'edit',
                    'visible' => false
                )
            )
        ),
    )
);