<?php
return array(
    'routes' => array(
        'application' => array(
            'child_routes' => array(
                'holiday' => array(
                    'type' => 'Segment',
                    'options' => array(
                        'route' => 'holiday[/:action][/id/:id][/page/:page][/order/:order][/dir/:dir][/limit/:limit]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'page' => '[0-9]+',
                            'id' => '[0-9]+'
                        ),
                        'defaults' => array(
                            'controller' => 'IdmHoliday\Presentation\Controller\Holiday',
                            'page' => 1,
                            'id' => 0,
                            'limit' => 25
                        )
                    )
                )
            )
        )
    )
);