<?php
return array(
    'resources' => array(
        'member' => array(
            array(
                'holiday/holiday',
                array(
                    'index',
                    'add',
                    'edit',
                    'remove'
                )
            ),
        ),
        'leader' => array(
            array(
                'holiday/holiday',
                array(
                    'admin_requests'
                )
            ),
        ),
        'admin' => array(
        )
    ),
);