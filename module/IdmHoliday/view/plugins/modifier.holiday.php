<?php
use IdmHoliday\Domain\Service\HolidayService;

function smarty_modifier_holiday($value, $field)
{
    if ($field == 'status') {
        switch ($value) {
            case HolidayService::StatusPending:
                return 'Pending';
                break;
            case HolidayService::StatusAccepted:
                return 'Accepted';
                break;
            case HolidayService::StatusDeclined:
                return 'Declined';
                break;
        }
    }
}