<?php
return array(
    'doctrine' => array(
        'configuration' => array(
            'orm_default' => array(
                'naming_strategy' => 'IdmBackend\Doctrine\NamingStrategy',
                'types' => array(
                    'file' => 'IdmBackend\Doctrine\Types\FileType',
                )
            )
        )
    ),
    'service_manager' => array(
        'invokables' => array(
            'IdmBackend\Doctrine\NamingStrategy' => 'IdmBackend\Doctrine\NamingStrategy'
        )
    )
);