<?php
namespace IdmBackend;

use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function onBootstrap(MvcEvent $e)
    {
        $sm = $e->getApplication()->getServiceManager();
        $em = $sm->get('Doctrine\ORM\EntityManager');

        // Custom dql functionallity
        $em->getConfiguration()->addCustomStringFunction('CONVERT', 'IdmBackend\Doctrine\Dql\ConvertUsing');

//         // Register entity listeners
//         // @todo move to config file
//         $config = array(
//         	'IdmUser\Domain\Entity\User' => 'IdmUser\Domain\Event\UserListener',
//         );
//         foreach ($config as $entityClass => $listeners) {
//             $metadata = $em->getClassMetadata($entityClass);
//             foreach ((array) $listeners as $listenerClass) {
//                 $listener = $services->get($listenerClass);
//                 $em->getConfiguration()->getEntityListenerResolver()->register($listener);
//                 Doctrine\ORM\Mapping\Builder\EntityListenerBuilder::bindEntityListener($metadata, $listenerClass);
//             }
//         }
    }
}
