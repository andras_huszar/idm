<?php
namespace IdmBackend\Doctrine;

use Doctrine\ORM\Mapping\DefaultNamingStrategy;

class NamingStrategy extends DefaultNamingStrategy
{

    public function referenceColumnName()
    {
        return 'id';
    }

    public function joinColumnName($propertyName)
    {
        return $propertyName . '_' . $this->referenceColumnName();
    }

    public function joinTableName($sourceEntity, $targetEntity, $propertyName = null)
    {
        return strtolower($this->classToTableName($sourceEntity) . '__' .
                $this->classToTableName($targetEntity));
    }

    public function joinKeyColumnName($entityName, $referencedColumnName = null)
    {
        return strtolower($this->classToTableName($entityName) . ($referencedColumnName ?: '_' . $this->referenceColumnName()));
    }

    public static function getFormName($entityName)
    {
        return  str_replace('\\Entity\\', '\\Form\\', $entityName) . 'Form';
    }

}