<?php

namespace IdmBackend\Doctrine\Types;

use Doctrine\DBAL\Types\StringType;

class FileType extends StringType
{

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'file';
    }

}