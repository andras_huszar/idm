<?php
namespace IdmBackend\Presentation\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\Request;
use Zend\EventManager\EventManagerInterface;
use Doctrine\ORM\Query;
use IdmUser\Domain\Entity\User;
use Zend\Form\Form;

abstract class AbstractCrudController extends AbstractActionController
{
    /**
     * @var \Zend\Session\Container
     */
    protected $session;

    /**
     * @var array
     */
    protected $limitOptions = array(
    	5,
        10,
        25,
        50,
        100
    );

    /**
     * @return \IdmBackend\Domain\AbstractEntityService
     */
    abstract public function getService();

    /**
     * @return \Zend\Form\Form
     */
    abstract public function getForm();

    /**
     * @return User
     */
    public function getIdentityUser()
    {
    	return $this->getServiceLocator()
    	   ->get('IdmUser\Domain\Service\IdentityService')
    	   ->getIdentityUser();
    }

    /**
     * @return \Zend\Session\Container
     */
    public function getSession()
    {
        if ($this->session === null) {
            $this->session = new \Zend\Session\Container(__CLASS__);
        }
        return $this->session;
    }

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
		$page = (int) $this->params('page');
		$limit = (int) $this->params('limit', 25);

        $data = $this->getService()
            ->createPaginator($this->getQuery())
			->setCurrentPageNumber($page)
			->setItemCountPerPage($limit);

		$view = new ViewModel(array(
			'data' => $data,
			'limitOptions' => $this->limitOptions
		));
        return $view;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getQuery()
    {
        $qb = $this->getService()->createQueryBuilder();
		$order = $this->params('order');
        if ($order) {
            $qb->orderBy($qb->getRootAlias().'.'.$order, $this->params('dir', 'asc'));
        }
        return $qb->getQuery();
    }

    /**
     * @param \Zend\Form\Form $entity
     * @param object $entity
     */
    protected function prepareForm(Form $form, $entity)
    {
         if (!is_object($entity)) {
             throw new \Exception('Invalid entity, must be an object');
         }
         $hydrator = $this->getService()->getHydrator();
         $inputFilter = $this->getService()->getInputFilter();
         $form->setHydrator($hydrator);
         $form->setInputFilter($inputFilter);
         $form->bind($entity);
    }

    /**
     * @return ViewModel
     */
    public function addAction()
    {
        $service = $this->getService();
        $request = $this->getRequest();

        $entity = $service->create();
        $form = $this->getForm();
        $this->prepareForm($form, $entity);

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $form->setData($data);
            if ($form->isValid()) {
                $service->save($entity);
                return $this->redirectToIndex();
            }
        }
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view->setTemplate('idm-application/form');
    }

    /**
     * @return ViewModel
     */
    public function editAction()
    {
        $service = $this->getService();
        $request = $this->getRequest();

        $id = (int) $this->params('id');
        $entity = $service->findById($id);
        if (! $entity) {
            $this->notFoundAction();
        }
        $form = $this->getForm();
        $this->prepareForm($form, $entity);

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $form->setData($data);
            if ($form->isValid()) {
                $service->save($entity);
                return $this->redirectToIndex();
            }
        }
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view->setTemplate('idm-application/form');
    }

    /**
     * @return ViewModel
     */
    public function removeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($request->getPost('submit') == 'yes') {
                $service = $this->getService();
                $id = (int) $this->params('id');
                if ($entity = $service->findById($id)) {
                    $service->remove($entity);
                }
            }
            return $request->isXmlHttpRequest()
                ? $this->response
                : $this->redirectToIndex();
        }
        $view = new ViewModel();
        $view->setTemplate('idm-application/confirm')
            ->setTerminal($request->isXmlHttpRequest());
        return $view;
    }

    /**
     * Redirect to current controller's index page
     * @param string $route
     */
	protected function redirectToIndex($route = null)
	{
	    //$routeMatch = $this->getEvent()->getRouteMatch();
		return $this->redirect()->toRoute(null, array(
		    'controller' => $this->params('__CONTROLLER__'),
		    'action' => 'index'
	    ));
	}

    /**
     * Redirect to current controller's index page
     * @param string $route
     */
	protected function redirectToReferrer()
	{
		return $this->redirect()->toUrl($this->getRequest()->getHeader('referer')->getUri());
	}

}