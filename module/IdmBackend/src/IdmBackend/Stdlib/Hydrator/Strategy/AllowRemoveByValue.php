<?php
namespace IdmBackend\Stdlib\Hydrator\Strategy;

use Zend\Stdlib\Exception\LogicException;
use DoctrineModule\Stdlib\Hydrator\Strategy\AbstractCollectionStrategy;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Filter\Word\UnderscoreToCamelCase;

class AllowRemoveByValue extends AbstractCollectionStrategy
{

    public function hydrate($value)
    {
        $filter = new UnderscoreToCamelCase();
        // AllowRemove strategy need "adder" and "remover"
        $adder   = 'add' . Inflector::singularize(ucfirst($filter->filter($this->collectionName)));
        $remover = 'remove' . Inflector::singularize(ucfirst($filter->filter($this->collectionName)));

        if (!method_exists($this->object, $adder) || !method_exists($this->object, $remover)) {
            throw new LogicException(sprintf(
                'AllowRemove strategy for DoctrineModule hydrator requires both %s and %s to be defined in %s
                 entity domain code, but one or both seem to be missing',
                $adder, $remover, get_class($this->object)
            ));
        }

        $collection = $this->getCollectionFromObjectByValue()->toArray();
        $toAdd      = new ArrayCollection(array_udiff($value, $collection, array($this, 'compareObjects')));
        $toRemove   = new ArrayCollection(array_udiff($collection, $value, array($this, 'compareObjects')));

        foreach ($toAdd as $add) {
        	$this->object->$adder($add);
        }
        foreach ($toRemove as $remove) {
        	$this->object->$remover($remove);
        }

        return $collection;
    }

    /**
     * Return the collection by value (using the public API)
     *
     * @return Collection
     */
    protected function getCollectionFromObjectByValue()
    {
        $filter = new UnderscoreToCamelCase();
        $object = $this->getObject();
        $getter = 'get' . ucfirst($filter->filter($this->getCollectionName()));

        if (!method_exists($object, $getter)) {
            throw new InvalidArgumentException(sprintf(
                'The getter %s to access collection %s in object %s does not exist',
                $getter,
                $this->getCollectionName(),
                get_class($object)
            ));
        }

        return $object->$getter();
    }

}