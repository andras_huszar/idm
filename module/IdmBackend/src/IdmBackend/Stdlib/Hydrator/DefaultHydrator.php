<?php

namespace IdmBackend\Stdlib\Hydrator;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use DoctrineModule\Stdlib\Hydrator\Strategy\AllowRemoveByReference;
use Zend\Filter\Word\CamelCaseToDash;
use Zend\Filter\Word\DashToCamelCase;
use Zend\Filter\Word\UnderscoreToCamelCase;
use IdmBackend\Stdlib\Hydrator\Strategy\DateStrategy;
use IdmBackend\Stdlib\Hydrator\Strategy\FileStrategy;

class DefaultHydrator extends DoctrineHydrator
{

    protected static $fieldNameFilter;

    /**
     * Hydrate $object with the provided $data.
     *
     * @param  array  $data
     * @param  object $object
     * @return object
     */
    public function hydrate(array $data, $object)
    {
        $object = parent::hydrate($data, $object);
        return $object;
    }

    /**
     * Extract values from an object
     *
     * @param  array  $data
     * @param  object $object
     * @return object
     */
    public function extract($object)
    {
        $data = parent::extract($object);
        return $data;
    }

    /**
     * Extract values from an object using a by-value logic (this means that it uses the entity
     * API, in this case, getters)
     *
     * @param  object $object
     * @throws RuntimeException
     * @return array
     */
    protected function extractByValue($object)
    {
        $fieldNames = array_merge($this->metadata->getFieldNames(), $this->metadata->getAssociationNames());
        $methods    = get_class_methods($object);

        $data = array();
        foreach ($fieldNames as $fieldName) {
            $getter = 'get' . ucfirst($this->getCamelCaseFilter()->filter($fieldName));

            // Ignore unknown fields
            if (!in_array($getter, $methods)) {
                continue;
            }

            $data[$fieldName] = $this->extractValue($fieldName, $object->$getter());
        }

        return $data;
    }

    /**
     * Hydrate the object using a by-value logic (this means that it uses the entity API, in this
     * case, setters)
     *
     * @param  array  $data
     * @param  object $object
     * @throws RuntimeException
     * @return object
     */
    protected function hydrateByValue(array $data, $object)
    {
        $object   = $this->tryConvertArrayToObject($data, $object);
        $metadata = $this->metadata;

        foreach ($data as $field => $value) {
            $value  = $this->handleTypeConversions($value, $metadata->getTypeOfField($field));
            $setter = 'set' . ucfirst($this->getCamelCaseFilter()->filter($field));

            if ($metadata->hasAssociation($field)) {
                $target = $metadata->getAssociationTargetClass($field);

                if ($metadata->isSingleValuedAssociation($field)) {
                    if (!method_exists($object, $setter)) {
                        continue;
                    }

                    $value = $this->toOne($target, $this->hydrateValue($field, $value));
                    $object->$setter($value);
                } elseif ($metadata->isCollectionValuedAssociation($field)) {
                    $this->toMany($object, $field, $target, $value);
                }
            } else {
                if (!method_exists($object, $setter)) {
                    continue;
                }

                $getter = 'get' . ucfirst($this->getCamelCaseFilter()->filter($field));

                $value = $this->hydrateValue($field, $value);
                $oldValue = $object->$getter();

                if (!$this->makeSureFieldIsModified($field, $value, $oldValue)) {
                    continue;
                }

                $object->$setter($value);
            }
        }

        return $object;
    }

    /**
     * Prepare the hydrator by adding strategies to every collection valued associations
     *
     * @return void
     */
    protected function prepare()
    {
        $metadata     = $this->metadata;
        $associations = $metadata->getAssociationNames();

        foreach ($associations as $association) {
            if ($metadata->isCollectionValuedAssociation($association)) {
                if ($this->byValue) {
                    $this->addStrategy($association, new Strategy\AllowRemoveByValue());
                } else {
                    $this->addStrategy($association, new AllowRemoveByReference());
                }
            }
        }
    }

    protected function getCamelCaseFilter()
    {
    	if (self::$fieldNameFilter === null) {
    	    self::$fieldNameFilter = new UnderscoreToCamelCase();
    	}
    	return self::$fieldNameFilter;
    }

    /**
     * Handle various type conversions that should be supported natively by Doctrine (like DateTime)
     *
     * @param  mixed  $value
     * @param  string $typeOfField
     * @return DateTime
     */
    protected function makeSureFieldIsModified($field, $value, $oldValue)
    {
        switch($this->metadata->getTypeOfField($field)) {
            case 'datetime':
            case 'time':
            case 'date':
                if (!$value && !$oldValue) {
                    return false;
                }
                if (!$value && $oldValue || $value && !$oldValue) {
                    return true;
                }
                return $value->getTimestamp() !== $oldValue->getTimestamp();
                break;
        }

        return true;
    }

}