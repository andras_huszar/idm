<?php
namespace IdmBackend\Domain;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Paginator\Paginator;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use IdmBackend\Stdlib\Hydrator\DefaultHydrator;
use IdmBackend\Doctrine\NamingStrategy;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Mapping\Builder\EntityListenerBuilder;
use IdmCore\ServiceManager\EntityManagerAwareInterface;

class AbstractEntityService implements ServiceLocatorAwareInterface,
                                       EventManagerAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var \Zend\EventManager\EventManagerInterface
     */
    protected $events;

    /**
     * @var \Zend\Stdlib\HydratorInterface
     */
    protected $logger;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repository;

    /**
     * @var \Zend\Stdlib\HydratorInterface
     */
    protected $hydrator;

    /**
     * @var \Zend\InputFilter\InputFilterInterface
     */
    protected $inputFilter;

    /**
     * @var array
     */
    protected $entityListeners;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var string
     */
    protected $entityAlias;

    /**
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @param EventManagerInterface $eventManager
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(__CLASS__, get_called_class()));
        $this->events = $events;
    }

    /**
     * @return \Zend\EventManager\EventManagerInterface
     */
    public function getEventManager()
    {
        if ($this->events === null) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }

    /**
     * @return \Zend\EventManager\EventManagerInterface
     */
    public function events()
    {
        return $this->getEventManager();
    }

    /**
     * @return \Zend\Log\Logger
     */
    public function getLogger()
    {
        if ($this->logger === null) {
            $this->logger = $this->getServiceLocator()->get('IdmBackend\Logger');
        }
        return $this->logger;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        if ($this->entityManager === null) {
            $this->entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->entityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        if ($this->repository === null) {
            $this->repository = $this->getEntityManager()->getRepository($this->entityClass);
        }
        return $this->repository;
    }

    /**
     * @return InputFilterInterface $inputFilter
     */
    public function getInputFilter()
    {
        if ($this->inputFilter === null) {
            $this->inputFilter = new $this->inputFilterClass($this->getEntityManager());
        }
        return $this->inputFilter;
    }

    /**
     * @param InputFilterInterface $inputFilter
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        $this->inputFilter = $inputFilter;
    }

    /**
     * @return \Zend\Stdlib\Hydrator\HydratorInterface
     */
    public function getHydrator()
    {
        if ($this->hydrator === null) {
            $hydrator = new DefaultHydrator($this->getEntityManager(), $this->entityClass, true);
            $this->setHydrator($hydrator);
        }
        return $this->hydrator;
    }

    /**
     * @param \Zend\Stdlib\Hydrator\HydratorInterface
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * Find entity by params
     *
     * @param array $id
     * @return object
     */
    public function findBy($params)
    {
    	$entity = $this->getRepository()->findBy($params);
    	return $entity;
    }

    /**
     * Find one entity by id
     *
     * @param int $id
     * @return object
     */
    public function findById($id)
    {
    	$entity = $this->getRepository()->find($id);
    	return $entity;
    }

	/**
	 * @param \Doctrine\ORM\Query $query
	 * @return \Zend\Paginator\Paginator
	 */
	public function createPaginator($query = null, $hydrationMode = null)
	{
		if ($query !== null) {
		    if ($query instanceof QueryBuilder) {
		        $query = $query->getQuery();
		    } elseif (!$query instanceof Query) {
		        throw \Exception('Invalid query parameter');
		    }
	    } else {
	        $query = $this->createQueryBuilder()->getQuery();
	    }
	    if ($hydrationMode === null) {
	        $hydrationMode = Query::HYDRATE_ARRAY;
	    }
	    $query->setHydrationMode($hydrationMode);

	    $adapter   = new DoctrineAdapter(new ORMPaginator($query));
		$paginator = new Paginator($adapter);
		return $paginator;
	}

	/**
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	public function createQueryBuilder($alias = null)
	{
       return $this->getRepository()
        	->createQueryBuilder($alias ?: $this->getAlias());
	}

    /**
     * Create entity
     *
     * @param object $entity
     */
    public function getAlias()
    {
        if ($this->entityAlias === null) {
            $name = explode('\\', $this->entityClass);
            $name = end($name);
            $this->entityAlias = strtolower(substr($name, 0, 1));
        }
        return $this->entityAlias;
	}

    /**
     * Create entity
     *
     * @param object $entity
     */
    public function create()
    {
        return new $this->entityClass;
	}

    /**
     * Save entity
     *
     * @param object $entity
     */
    public function save($entity)
    {
        $this->setEntityListener($this);
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    /**
     * Delete entity
     *
     * @param object $entity
     */
    public function remove($entity)
    {
        $this->setEntityListener($this);
        $em = $this->getEntityManager();
        $em->remove($entity);
        $em->flush();
    }

    /**
     * Set entity listener
     *
     * @param object $listener
     */
    public function setEntityListener($listener)
    {
        $listenerClass = get_class($listener);
        if (!isset($this->entityListeners[$listenerClass])) {
            $em = $this->getEntityManager();
            $metadata = $em->getClassMetadata($this->entityClass);
            $em->getConfiguration()->getEntityListenerResolver()->register($listener);
            EntityListenerBuilder::bindEntityListener($metadata, $listenerClass);
        }
    }

    /**
     * Get changesets from doctrine
     * @return array
     */
    protected function getChangeSets($entity)
    {
        $em = $this->getEntityManager();
        $uow = $em->getUnitOfWork();
        $uow->computeChangeSets(); // do not compute changes if inside a listener
        $changeset = $uow->getEntityChangeSet($user);
        return $changeset;
    }

    /**
     * Update changeset in unit of work
     * @return array
     */
    protected function updateChangeSets($entity)
    {
        $unitOfWork = $this->getEntityManager()->getUnitOfWork();
        $classMetadata = $this->getEntityManager()->getClassMetadata(get_class($entity));
        $unitOfWork->computeChangeSet($classMetadata, $entity);
    }

}