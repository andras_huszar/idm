<?php
return array(
    'idm' => array(
        'files' => array(
            'filters' => array(
                'document' => array(
                    'max_file_size' => 2048000
                ),
                'picture' => array(
                    'max_file_size' => 2048000
                ),
                'archive' => array(
                    'max_file_size' => 2048000
                ),
            ),
            'thumbnails' => array(
                '25x25',
                '100x100',
                '150x150'
            )
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'idm_file_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/IdmFile/Domain/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'IdmFile\Domain\Entity' => 'idm_file_driver'
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'IdmFile\Presentation\Controller\File' => 'IdmFile\Presentation\Controller\FileController'
        )
    ),
    'service_manager' => array(
        'invokables' => array(
            'IdmFile\Domain\Service\FileService' => 'IdmFile\Domain\Service\FileService',
            'IdmFile\Domain\Service\PictureService' => 'IdmFile\Domain\Service\PictureService'
        ),
        'abstract_factories' => array(
            'IdmFile\Domain\Filter\Service\FileFilterAbstractFactory'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    ),
    'acl' => include __DIR__ . '/acl.config.php',
    'navigation' => include __DIR__ . '/navigation.config.php',
    'router' => include __DIR__ . '/routes.config.php',
);