<?php
return array(
    'resources' => array(
        'guest' => array(
        ),
        'member' => array(
            array(
                'file/file',
                array(
                    'index',
                    'upload',
                    'view',
                    'edit',
                    'remove',
                    'download',
                    'picture'
                )
            )
        ),
        'leader' => array(
        ),
        'admin' => array(
        )
    ),
);