<?php
return array(
    'default' => array(
        array(
            'label' => 'Files',
            'route' => 'application/file',
            'controller' => 'IdmFile\Presentation\Controller\File',
            'action' => 'index',
            'pages' => array(
                array(
                    'label' => 'View file',
                    'route' => 'application/file',
                    'controller' => 'IdmFile\Presentation\Controller\File',
                    'action' => 'view',
               		'visible' => false
                ),
                array(
                    'label' => 'Upload file',
                    'route' => 'application/file',
                    'controller' => 'IdmFile\Presentation\Controller\File',
                    'action' => 'upload'
                ),
                array(
                    'label' => 'Edit file',
                    'route' => 'application/file',
                    'controller' => 'IdmFile\Presentation\Controller\File',
                    'action' => 'edit',
               		'visible' => false
                )
            )
        )
    )
);