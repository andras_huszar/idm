<?php
return array(
    'routes' => array(
        'application' => array(
            'child_routes' => array(
                'file-download' => array(
    				'type'	=> 'Regex',
    				'options' => array(
    					'spec' => 'file/download/%path%',
    					'regex'	=> 'file/download/(?<path>.+)',
    					'defaults' => array(
                            'controller' => 'IdmFile\Presentation\Controller\File',
    					    'action' => 'download'
    					)
    				)
                ),
                'file-picture' => array(
    				'type'	=> 'Regex',
    				'options' => array(
    					'spec' => 'file/picture/%path%',
    					'regex'	=> 'file/picture/(?<path>.+)',
    					'defaults' => array(
                            'controller' => 'IdmFile\Presentation\Controller\File',
    					    'action' => 'picture'
    					)
    				)
                ),
                'file' => array(
                    'type' => 'Segment',
                    'options' => array(
                        'route' => 'file[/:action][/type/:type][/id/:id][/page/:page][/order/:order][/dir/:dir][/limit/:limit]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'type' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'page' => '[0-9]+',
                            'id' => '[0-9]+'
                        ),
                        'defaults' => array(
                            'controller' => 'IdmFile\Presentation\Controller\File',
                            'page' => 1,
                            'id' => 0,
                            'limit' => 10
                        )
                    )
                )
            )
        )
    )
);