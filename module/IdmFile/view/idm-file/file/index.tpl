          
          <h2 class="sub-header">{$pageLabel}</h2>
          
          {if count($data)}
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>{order action="index" order="name" label="Name"}</th>
                  <th>{order action="index" order="type" label="Type"}</th>
                  <th>{order action="index" order="mime" label="Mime"}</th>
                  <th>{order action="index" order="size" label="Size"}</th>
                  <th>{order action="index" order="f.uploader" label="Uploader"}</th>
                  <th width="100"></th>
                </tr>
              </thead>
              <tbody>
              {foreach from=$data item="item"}
                <tr>
                  <td>{$item.id|default}</td>
                  <td>{$item.name|default|basename}</td>
                  <td>{$item.type|default}</td>
                  <td>{$item.mime|default}</td>
                  <td>{$item.size|default} kb</td>
                  <td>{$item.uploader|fullname}</td>
                  <td>
                    <a href="/file/view/id/{$item.id|default}">View</a>
                    <a href="/file/edit/id/{$item.id|default}">Edit</a>
                    <a href="/file/remove/id/{$item.id|default}">Remove</a>
                  </td>
                </tr>
              {/foreach}
              </tbody>
            </table>
          </div>
          {$this->paginationControl($data)}
          {else}
          <p>No data found</p>
          {/if}
          