
			<h2>{$pageLabel}</h2>
			<ul>
              <li>ID: {$file->getId()|default}</li>
              <li>Name: {$file->getName()|default|basename}</li>
              <li>Type: {$file->getType()|default} ({$file->getMime()|default})</li>
              <li>Size: {$file->getSize()|default} kb</li>
              <li>Uploader: {$file->getUploader()|fullname}</li>
              {if $file->getType() == 'picture'}
              <li>
              	<a href="/file/picture/{$file->getPath()}" target="_blank">
              	  <img src="/file/picture/100x100/{$file->getPath()}" alt="{$file->getName()}" />
              	</a>
              </li>
              {else}
              <li><a href="/file/download/{$file->getPath()}" class="btn btn-primary">Download</a></li>
              {/if}
			</ul>