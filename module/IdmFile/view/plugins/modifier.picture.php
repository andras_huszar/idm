<?php
function smarty_modifier_picture($path, $thumb)
{
    $service = IdmCore\Module::$sm->get('IdmFile\Domain\Service\FileService');
    return $service->getPicture($thumb.'/'.$path);
}