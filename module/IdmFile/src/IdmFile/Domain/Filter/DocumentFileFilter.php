<?php
namespace IdmFile\Domain\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Filter\File\RenameUpload;
use Zend\Validator\File\Size;
use Zend\Validator\File\MimeType;
use Zend\Validator\File\ImageSize;

class DocumentFileFilter extends InputFilter
{
    public function __construct($config)
    {
        $this->add(array(
            'name' => 'type',
            'required' => false
        ));
        $this->add(array(
            'name' => 'file',
            'required' => true,
            'validators' => array(
                new Size(array(
                    'max' => $config['max_file_size']
                )),
                new MimeType(array(
                    'mimeType' => 'application/pdf'
                		          /*application/vnd.oasis.opendocument.text,application/vnd.oasis.opendocument.text-master,' . // odm/odt
                		          'application/xml,text/xml,' . // xml
                		          'application/excel,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,', // xls
                		          'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document' // doc*/
                ))
            )
        ));
    }
}