<?php
namespace IdmFile\Domain\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Filter\File\RenameUpload;
use Zend\Validator\File\Size;
use Zend\Validator\File\MimeType;
use Zend\Validator\File\ImageSize;

class ArchiveFileFilter extends InputFilter
{
    public function __construct($config)
    {
        $this->add(array(
            'name' => 'type',
            'required' => false
        ));
        $this->add(array(
            'name' => 'picture',
            'required' => true,
            'validators' => array(
                new Size(array(
                    'max' => $config['max_file_size']
                )),
                new MimeType(array(
                    'mimeType' => 'application/x-compressed,application/x-zip-compressed,application/zip,multipart/x-zip'
                                . 'application/x-tar,application/x-compressed,application/x-gzip,application/x-gzip,multipart/x-gzip'
                		        . 'application/x-rar-compressed, application/octet-stream'
                ))
            )
        ));
    }
}