<?php
namespace IdmFile\Domain\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Filter\File\RenameUpload;
use Zend\Validator\File\Size;

class FileFilter extends InputFilter
{
    public function __construct($config)
    {
        $this->add(array(
            'name' => 'picture',
            'required' => true,
            'validators' => array(
                new Size(array(
                    'max' => 204800
                ))
            )
        ));
    }
}