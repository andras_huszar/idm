<?php
namespace IdmFile\Domain\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Filter\File\RenameUpload;
use Zend\Validator\File\Size;
use Zend\Validator\File\MimeType;
use Zend\Validator\File\ImageSize;

class PictureFileFilter extends InputFilter
{
    public function __construct($config)
    {
        $this->add(array(
            'name' => 'type',
            'required' => false
        ));
        $this->add(array(
            'name' => 'file',
            'required' => true,
            'validators' => array(
                new Size(array(
                    'max' => $config['max_file_size']
                )),
                new MimeType(array(
                    'mimeType' => 'image/jpeg,image/png,image/x-png'
                )),
                new ImageSize(array(
                    'maxWidth' => 1920,
                    'maxHeight' => 1200
                ))
            )
        ));
    }
}