<?php
namespace IdmFile\Domain\Filter\Service;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FileFilterAbstractFactory implements AbstractFactoryInterface
{
    public function canCreateServiceWithName(ServiceLocatorInterface $services, $name, $requestedName)
    {
        if (substr($requestedName, -10) == 'FileFilter'){
            return true;
        }
        return false;
    }

    public function createServiceWithName(ServiceLocatorInterface $services, $name, $requestedName)
    {
        $config = $services->get('Config');
        $filters = $config['idm']['files']['filters'];

        $class = explode('\\', $requestedName);
        $type = strtolower(substr(end($class), 0, -10));

        if (!isset($filters[$type])) {
        	throw new \Exception("Missing filetype config for `$type`");
        }
        return new $requestedName($filters[$type]);
    }
}