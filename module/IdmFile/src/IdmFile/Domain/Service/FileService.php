<?php
namespace IdmFile\Domain\Service;

use IdmBackend\Domain\AbstractEntityService;
use IdmFile\Domain\Entity\File;
use Zend\InputFilter\InputFilter;

class FileService extends AbstractEntityService
{
    /**
     * @var string
     */
    protected $entityClass = 'IdmFile\Domain\Entity\File';

    /**
     * @var string
     */
    protected $inputFilterClass = 'IdmFile\Domain\Filter\FileFilter';

    const MAX_SIZE = 1920;

    const CHMOD_DIR = 0777;

    const CHMOD_FILE = 0666;

    const DIR_UPLOAD = 'public/upload';

    /**
     * @param string $type
     * @return InputFilter
     */
    public function getFileFilter($type)
    {
        $class = 'IdmFile\\Domain\\Filter\\'.ucfirst($type).'FileFilter';
        return $this->getServiceLocator()->get($class);
    }

    /**
     * Create a new upload
     * @param unknown $type
     * @param unknown $data
     * @return \IdmFile\Domain\Entity\File
     */
    public function upload($type, $data)
    {
        $uploader = $this->getServiceLocator()
    	    ->get('IdmUser\Domain\Service\IdentityService')
    	    ->getIdentityUser();

        $tmpname = $data['tmp_name'];
        $id = md5_file($tmpname);
        $path = $this->_getPath($id);

        if ($type == 'picture') {
            $path = $this->storeImage($tmpname, $path);
        } else {
            move_uploaded_file($tmpname, self::DIR_UPLOAD . $path);
        }

        $file = new File();
        $file->setPath($path)
            ->setType($type)
            ->setName($data['name'])
            ->setMime($data['type'])
            ->setSize($data['size'])
            ->setUploader($uploader);

        $this->save($file);
        return $file;
    }

    /**
     * @param string|File $fileOrPath
     * @return string
     */
    public static function realPath($fileOrPath)
    {
        if ($fileOrPath instanceof File) {
            $fileOrPath = $fileOrPath->getPath();
        }
        return self::DIR_UPLOAD . '/' . $fileOrPath;
    }

    /**
     * @param string $path
     * @return File
     */
    public function getFileByPath($path)
    {
        if (!file_exists(self::realPath($path))) {
            return false;
        }
        if (!($file = $this->getRepository()->findOneBy(array('path' => $path)))) {
            return false;
        }
        return $file;
    }

    /**
     * @param string $path
     * @return boolean|File
     */
    public function getPicture($path)
    {
        $dir = self::DIR_UPLOAD;
        if (preg_match('@^(?:(\d+)x(\d+)([^/]*)/)?([^\.]+)\.(jpg|png|gif)$@i', $path, $match)) {

            $path = "{$match[4]}.{$match[5]}";

            if (empty($match[0]) || empty($match[1])) {
                return $path;
            }

            // Crop image
            $cache = "{$match[1]}x{$match[2]}{$match[3]}";

            $config = $this->getServiceLocator()->get('Config');
            $thumbnails = $config['idm']['files']['thumbnails'];
            if (!in_array($cache, $thumbnails)) {
                return false;
            }

            $thumbPath = "{$cache}/{$match[4]}.{$match[5]}";
            $thumbnail = $this->createPath(
                $thumbPath,
                $dir,
                intval('0755', 8)
            );
            if (!file_exists($thumbnail)) {
                $this->createThumbnail($dir . '/' . $path, $thumbnail, $match[1], $match[2], $match[3]);
                chmod($thumbnail, intval('0755', 8));
            }
            return $thumbPath;
		}
		return false;
    }

    protected function _getPath($hash)
    {
        $dir = self::DIR_UPLOAD;
        $filename = null;
        if (preg_match('/^(.)(..)(.+)$/', $hash, $path)) {
            $filename = $path[1] . '/';
            if (! file_exists($dir . '/' . $filename)) {
                mkdir($dir . '/' . $filename, self::CHMOD_DIR);
                chmod($dir . '/' . $filename, self::CHMOD_DIR);
            }
            $filename .= $path[2] . '/';
            if (! file_exists($dir . '/' . $filename)) {
                mkdir($dir . '/' . $filename, self::CHMOD_DIR);
                chmod($dir . '/' . $filename, self::CHMOD_DIR);
            }
            $filename .= base_convert($path[3], 16, 36);
        }
        return $filename;
    }

    protected function storeImage($filename, $path, $width = self::MAX_SIZE, $height = self::MAX_SIZE)
    {
        $dir = self::DIR_UPLOAD;
        $result = null;
        $data = @getimagesize($filename);
        if ($data) {
            @ini_set('memory_limit', '256M');
            switch ($data[2]) {
                case IMAGETYPE_JPEG:
                    $source = imagecreatefromjpeg($filename);
                    break;
                case IMAGETYPE_PNG:
                    $source = imagecreatefrompng($filename);
                    break;
                case IMAGETYPE_GIF:
                    $source = imagecreatefromgif($filename);
                    break;
            }
        }
        if (! empty($source)) {
            $sw = $data[0];
            $sh = $data[1];
            $tw = $width;
            $th = round($width / $sw * $sh);
            if ($th > $height) {
                $th = $height;
                $tw = round($height / $sh * $sw);
            }
            if ($sw <= $tw && $sh <= $th) {
                $th = $sh;
                $tw = $sw;
            }
            $path .= "_{$tw}x{$th}.jpg";
            $target = imagecreatetruecolor($tw, $th);
            imagecopyresampled($target, $source, 0, 0, 0, 0, $tw, $th, $sw, $sh);
            if (imagejpeg($target, $dir . '/' . $path, 95)) {
                chmod($dir . '/' . $path, self::CHMOD_FILE);
                $result = $path;
            }
            imagedestroy($target);
        }
        return $result;
    }

    protected function createPath($filename, $path, $perm=0777)
    {
        $part = explode('/', $filename);
        while (count($part) > 1) {
            $path .= '/' . array_shift($part);
            if (!is_dir($path)) {
                mkdir($path);
                chmod($path, $perm);
            }
        }
        $path .= '/' . array_shift($part);
        return $path;
    }

    public function createThumbnail($sourceFile, $targetFile, $width, $height, $fit)
    {
        $result = false;
        $data = @getimagesize($sourceFile);
        if ($data) {
            switch ($data[2]) {
            	case IMAGETYPE_JPEG:
            	    $source = imagecreatefromjpeg($sourceFile);
            	    break;
            	case IMAGETYPE_PNG:
            	    $source = imagecreatefrompng($sourceFile);
            	    break;
            	case IMAGETYPE_GIF:
            	    $source = imagecreatefromgif($sourceFile);
            	    break;
            }
        }
        if (!empty($source)) {
            $sw = $data[0];
            $sh = $data[1];
            //cut edges
            if (empty($fit)) {
                if ($height > 0) {
                    $tw = $sw;
                    $th = round($height / $width * $sw);
                    if ($th > $sh) {
                        $th = $sh;
                        $tw = round($width / $height * $sh);
                        $sx = round(($sw - $tw) / 2);
                        $sy = 0;
                    } else {
                        $sx = 0;
                        $sy = round(($sh - $th) / 2);
                    }
                    $target = imagecreatetruecolor($tw, $th);
                    imagecopy($target, $source, 0, 0, $sx, $sy, $tw, $th);
                    imagedestroy($source);
                    $source = $target;
                    $sw = $tw;
                    $sh = $th;
                }
            }
            //divider mode
            if ($width == 1) {
                $tw = round($sw / $height);
                $th = round($sh / $height);
            }
            //calculate target size
            elseif ($width > 0) {
                $tw = $width;
                $th = round($width / $sw * $sh);
                if ($height > 0)
                {
                    if ($th > $height)
                    {
                        $th = $height;
                        $tw = round($height / $sh * $sw);
                    }
                }
            } else {
                $th = $height;
                $tw = round($height / $sh * $sw);
            }
            //resize
            if ($sw > $tw || $sh > $th) {
                $target = imagecreatetruecolor($tw, $th);
                imagecopyresampled($target, $source, 0, 0, 0, 0, $tw, $th, $sw, $sh);
                imagedestroy($source);
            } else {
                $target = $source;
            }
            switch ($data[2]) {
            	case IMAGETYPE_JPEG:
            	    $result = imagejpeg($target, $targetFile, 90);
            	    break;
            	case IMAGETYPE_PNG:
            	    $result = imagepng($target, $targetFile, 9);
            	    break;
            	case IMAGETYPE_GIF:
            	    $result = imagegif($target, $targetFile);
            	    break;
            }
            imagedestroy($target);
        }
        return $result;
    }

    public function getImageAttrs($src, $thumb = false)
    {
        $attrs = array();

        $thumbProps = null;
        if ($thumb && isset($this->config[$thumb])) {
            preg_match('/^(\d+)x(\d+)([a-z]*)$/', $this->config[$thumb], $thumbProps);
            $src = $this->config[$thumb].'/'.$src;
        }

        $attrs['src'] = $src;

        if (preg_match('/_(\d+)x(\d+)\.jpg$/', $src, $size)) {
            $sw = (int) $size[1];
            $sh = (int) $size[2];
            if (!$thumbProps) {
                $attrs['width'] = $sw;
                $attrs['height'] = $sh;
            } else {
                $width = (int) $thumbProps[1];
                $height = (int) $thumbProps[2];
                if (empty($thumbProps[3])) {
                    $attrs['width'] = $thumbProps[1];
                    $attrs['height'] = $thumbProps[2];
                } else {
                    switch ($width) {
                        case 0:
                            $tw = round($height / $sh * $sw);
                            $th = $height;
                            break;
                        case 1:
                            $tw = round($sw / $height);
                            $th = round($sh / $height);
                            break;
                        default:
                            $tw = $width;
                            $th = round($width / $sw * $sh);
                            if ($height > 0 && $th > $height) {
                                $th = $height;
                                $tw = round($height / $sh * $sw);
                            }
                            break;
                    }
                    if ($sw > $tw || $sh > $th) {
                        $attrs['width'] = $tw;
                        $attrs['height'] = $th;
                    } else {
                        $attrs['width'] = $sw;
                        $attrs['height'] = $sh;
                    }
                }
            }
        }
        return $attrs;
    }
}