<?php

namespace IdmFile\Presentation\Form\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;

class FileForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('file\presentation\form');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'path',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'path-field',
                'id' => 'path-field',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Path',
            ),
        ));

        $this->add(array(
            'name' => 'title',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'title-field',
                'id' => 'title-field',
                'placeholder' => 'Appers as image alt text',
            ),
            'options' => array(
                'label' => 'Title',
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
        ));
    }
}