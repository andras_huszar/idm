<?php
namespace IdmFile\Presentation\Form;

use Zend\Form\Form;

class UploadForm extends Form
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    /**
     *
     * @var UserFilter
     */
    protected $inputFilter;

    protected $typeOptions = array(
    	'document' => 'Document',
        'picture' => 'Picture',
        'archive' => 'Archive'
    );

    /**
     *
     * @param EntityManager $em
     */
    public function __construct()
    {
        parent::__construct('file-form');

        $this->setAttribute('id', 'upload-form');
        $this->add(array(
            'name' => 'type',
        	'type' => 'Select',
            'options' => array(
                'label' => 'Type',
                'value_options' => $this->typeOptions,
            ),
            'attributes' => array(
                'class' => 'name'
            )
        ));
        $this->add(array(
            'name' => 'file',
        	'type' => 'File',
            'options' => array(
                'label' => 'Choose a file'
            ),
            'attributes' => array(
                'type' => 'file',
                'class' => 'file'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Submit'
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-primary'
            )
        ));
    }
}