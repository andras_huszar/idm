<?php
namespace IdmFile\Presentation\Controller;

use IdmBackend\Presentation\Controller\AbstractCrudController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Doctrine\ORM\Query;
use IdmBackend\Form\FileForm;
use IdmFile\Domain\Entity\File;
use IdmFile\Presentation\Form\UploadForm;
use IdmFile\Domain\Service\FileService;

class FileController extends AbstractCrudController
{
	/**
	 * @return \IdmFile\Domain\Service\FileService
	 */
	public function getService()
	{
	    return $this->getServiceLocator()->get('IdmFile\Domain\Service\FileService');
	}

	/**
	 * @return \IdmFile\Domain\Service\PictureService
	 */
	public function getPictureService()
	{
	    return $this->getServiceLocator()->get('IdmFile\Domain\Service\PictureService');
	}

	/**
	 * @see \IdmBackend\Presentation\Controller\AbstractCrudController::getForm()
	 */
	public function getForm()
	{
		return new FileForm();
	}

    /**
     * @return Query
     */
    protected function getQuery()
    {
        $qb = $this->getService()->createQueryBuilder('f');
    	$qb->select('f, u')
    	   ->leftJoin('f.uploader', 'u');
        return $qb->getQuery();
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
	public function viewAction()
	{
		if (!($id = $this->params('id')) || !($file = $this->getService()->findById($id))) {
			return $this->notFoundAction();
		}
		return new ViewModel(array(
			'file' => $file
		));
	}

	/**
	 * @return \Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
	 */
	public function uploadAction()
	{
		$form = new UploadForm();

		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
			if (isset($data['type'])) {
				$type = $data['type'];
			} else {
				$type = $this->params('type');
			}

			$service = $this->getService();
			$inputFilter = $service->getFileFilter($type);
			$form->setData($data)->setInputFilter($inputFilter);

			if ($form->isValid()) {
        		$file = $service->upload($type, $data['file']);
        		if ($request->isXmlHttpRequest()) {
            		return new JsonModel(array(
            		    'success' => true,
            		    'id' => $file->getId(),
            		    'path' => $file->getPath()
            		));
        		} else {
        			$this->redirect()->toRoute('application/file', array(
        		        'action' => 'view',
        			    'id' => $file->getId()
        			));
        		}
			}
		}
		$view = new ViewModel(array(
			'form' => $form
		));
		return $view->setTemplate('idm-application/form');
	}

	/**
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function pictureAction()
	{
	    if (!($path = $this->params('path')) || !($fileName = $this->getService()->getPicture($path))) {
	        return $this->notFoundAction();
	    }

	    $response = $this->getResponse();
		$response->setContent(file_get_contents(FileService::realPath($fileName)));
		$response->getHeaders()->addHeaderLine('Content-Type', 'image/jpeg');
        return $response;
	}

	/**
	 * @return \Zend\Http\Response\Stream
	 */
	public function downloadAction()
	{
	    if (!($path = $this->params('path')) || !($file = $this->getService()->getFileByPath($path))) {
	    	return $this->notFoundAction();
	    }

        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen(FileService::realPath($file), 'r'));
        $response->setStatusCode(200);

        $headers = new \Zend\Http\Headers();
        $headers->addHeaderLine('Content-Type', $file->getMime())
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $file->getName() . '"')
                ->addHeaderLine('Content-Length', $file->getSize());

        $response->setHeaders($headers);
        return $response;
	}
}