<form class="form-inline" role="form">
<div class="form-group">
<select class="form-control" onchange="document.location.href=this.value">
{section name=limit loop=$limitOptions name=idx}
  <option value="{url page=$first limit=$limitOptions[idx]}"{if $limitOptions[idx] == $this->params()->fromRoute('limit')} selected="selected"{/if}>{$limitOptions[idx]}</option>
{/section}
</select>
</div>
</form>
{if $pageCount > 1}
<ul class="pagination">
  <li><a href="{url page=$first}">&laquo;</a></li>
{foreach from=$pagesInRange item="page"}
{if $page != $current}
  <li><a href="{url page=$page}">{$page}</a></li>
{else}
  <li class="active"><a href="#">{$page} <span class="sr-only">(current)</span></a></li>
{/if}
{/foreach}
  <li><a href="{url page=$last}">&raquo;</a></li>
</ul>
{/if}