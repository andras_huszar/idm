<?php
return array(
    'routes' => array(
        'application' => array(
            'type' => 'Literal',
            'options' => array(
                'route' => '/',
                'defaults' => array(
                    'controller' => 'IdmApplication\Controller\Index',
                    'action' => 'index'
                )
            ),
            'may_terminate' => true,
            'child_routes' => array(
            )
        )
    )
);