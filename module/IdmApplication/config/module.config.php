<?php
return array(
    'application' => array(
        'name' => 'IDM',
        'description' => 'Identity manager solution for small and medium corporations',
        'author' => 'Andras Huszar',
        'author_url' => '',
        'version' => '0.2'
    ),
    'controllers' => array(
        'invokables' => array(
            'IdmApplication\Controller\Index' => 'IdmApplication\Controller\IndexController',
        )
    ),
    'view_manager' => array(
        'default_suffix' => '.tpl',
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/idm-application/layout.tpl',
            'error/403'     => __DIR__ . '/../view/idm-application/error/403.tpl',
            'error/404'     => __DIR__ . '/../view/idm-application/error/404.tpl',
            'error/index'   => __DIR__ . '/../view/idm-application/error/index.tpl',
            'layout/blank'  => __DIR__ . '/../view/idm-application/blank.tpl',
            'form'          => __DIR__ . '/../view/idm-application/form.tpl',
            'paginator'     => __DIR__ . '/../view/idm-application/paginator.tpl',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view'
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'acl' => include __DIR__ . '/acl.config.php',
    'navigation' => include __DIR__ . '/navigation.config.php',
    'router' => include __DIR__ . '/routes.config.php',
);