<?php
return array(
    'restricted_route' => 'application',
    'roles' => array(
        'guest' => null,
        'member' => 'guest',
        'leader' => 'member',
        'admin' => 'leader',
    ),
    'resources' => array(
        'member' => array(
            array(
                'index/index',
                null
            )
        )
    )
);