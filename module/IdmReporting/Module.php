<?php
namespace IdmReporting;

use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function onBootstrap(MvcEvent $e)
    {
        $sm = $e->getApplication()->getServiceManager();
        $em = $sm->get('Doctrine\ORM\EntityManager');

        // Add report listener
        /* @var $em \Doctrine\ORM\EntityManager */
        $listener = $sm->get('IdmReporting\Domain\Event\ReportingListener');
        $em->getEventManager()->addEventListener($listener->getSubscribedEvents(), $listener);
    }
}
