<?php
namespace IdmReporting\Domain\Event\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use IdmReporting\Domain\Event\ReportingListener;

class ReportingListenerFactory implements FactoryInterface
{

	/**
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService(ServiceLocatorInterface $services)
	{
		$service = $services->get('IdmReporting\Domain\Service\ReportService');
		return new ReportingListener($service);
	}

}