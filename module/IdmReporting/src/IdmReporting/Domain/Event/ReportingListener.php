<?php
namespace IdmReporting\Domain\Event;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;

class ReportingListener implements EventSubscriber
{

	protected $reportService;

    /**
     * @var array
     */
    protected $events = array(
        'prePersist',
        'preUpdate'
    );

    public function __construct($reportService)
    {
    	$this->reportService = $reportService;
    }

    public function getSubscribedEvents()
    {
        return $this->events;
    }

    public function prePersist(LifecycleEventArgs $event)
    {
    	$entity = $event->getEntity();
    	if (method_exists($entity, 'setUuid')) {
    		$this->generateEntityUuid($entity);
    	}
    	$this->reportService->createNewReport($entity);
    }

    public function preUpdate(LifecycleEventArgs $event)
    {
    	$entity = $event->getEntity();
    	$this->reportService->createNewReport($entity);
    }

    protected function generateEntityUuid($entity)
    {
    	$uuid = sha1(sha1(sha1(microtime())));
    	$entity->setUuid($uuid);
    }

}