<?php
namespace IdmReporting\Presentation\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ReportController extends AbstractActionController
{
    /**
     * @return \IdmHoliday\Domain\Service\HolidayService
     */
    public function getService()
    {
    	return $this->getServiceLocator()->get('IdmHoliday\Domain\Service\HolidayService');
    }

    /**
     * @return ViewModel
     */
    protected function getQuery()
    {
        $qb = $this->getService()->createQueryBuilder();
        $qb->select('h, partial r.{id,first_name,last_name}')
            ->leftJoin('h.requester', 'r')
            ->orderBy('h.id', 'asc');
		$order = $this->params('order');
        if ($order) {
            $qb->orderBy($order, $this->params('dir', 'asc'));
        }
        return $qb->getQuery();
    }
}