<?php
return array(
    'default' => array(
        array(
            'label' => 'Reports',
            'route' => 'application/report',
            'controller' => 'Report',
            'action' => 'index'
        )
    )
);