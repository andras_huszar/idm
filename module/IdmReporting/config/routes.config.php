<?php
return array(
    'routes' => array(
        'application' => array(
            'child_routes' => array(
                'report' => array(
                    'type' => 'Segment',
                    'options' => array(
                        'route' => 'report[/:action][/id/:id][/page/:page]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'page' => '[0-9]+',
                            'id' => '[0-9]+'
                        ),
                        'defaults' => array(
                            'controller' => 'IdmReporting\Presentation\Controller\Report',
                            'page' => 1,
                            'id' => 0,
                        ),
                    ),
                )
            )
        )
    )
);