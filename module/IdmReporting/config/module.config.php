<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'idm_reporting_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/IdmReporting/Domain/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'IdmReporting\Domain\Entity' => 'idm_reporting_driver'
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'IdmReporting\Presentation\Controller\Report' => 'IdmReporting\Presentation\Controller\ReportController',
        )
    ),
    'service_manager' => array(
        'invokables' => array(
            'IdmReporting\Domain\Service\ReportService' => 'IdmReporting\Domain\Service\ReportService',
        ),
        'factories' => array(
        	'IdmReporting\Domain\Event\ReportingListener' => 'IdmReporting\Domain\Event\Service\ReportingListenerFactory'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    ),
    'acl' => include __DIR__ . '/acl.config.php',
    'navigation' => include __DIR__ . '/navigation.config.php',
    'router' => include __DIR__ . '/routes.config.php',
);