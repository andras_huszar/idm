<?php
function smarty_modifier_fullname($user)
{
    return \IdmUser\Domain\Service\UserService::fullName($user);
}