          
          <h2 class="sub-header">Rights</h2>
          {if !empty($data)}
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>Name</th>
                  <th>Resource</th>
                  <th>Privilege</th>
                  <th width=100"></th>
                </tr>
              </thead>
              <tbody>
              {foreach from=$data item="item"}
                <tr>
                  <td>{$item.id|default}</td>
                  <td>{$item.name|default}</td>
                  <td>{$item.resource|default}</td>
                  <td>{$item.privilege|default}</td>
                  <td>
                    <a href="/role/edit/id/{$item.id|default}">Edit</a>
                    <a href="/role/remove/id/{$item.id|default}">Remove</a>
                  </td> 
                </tr>
              {/foreach}
              </tbody>
            </table>
          </div>
          {else}
          <p>No data found</p>
          {/if}