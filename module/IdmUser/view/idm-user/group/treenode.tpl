
		<ul>
		{foreach from=$data item="group"}
		  <li>
		  	<a href="#">
		  		{$group->getName()}
		  	</a>
		    {assign var="children" value=$group->getChildren()}
		    {if $children}
		      {include file="treenode.tpl" data=$children}
		    {/if}
		  </li>
		{/foreach}
		</ul>
		