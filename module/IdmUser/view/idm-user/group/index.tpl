          
          <h2 class="sub-header">Groups</h2>
          {if !empty($data)}
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="1%">#</th>
                  <th>{order action="index" order="g.name" label="Name"}</th>
                  <th>{order action="index" order="p.name" label="Parent"}</th>
                  <th>{order action="index" order="l.last_name" label="Group leader"}</th>
                  <th width=10%"></th>
                </tr>
              </thead>
              <tbody>
              {foreach from=$data item="item"}
                <tr>
                  <td>{$item.id|default}</td>
                  <td>{$item.name|default}</td>
                  <td>{$item.parent.name|default}</td>
                  <td>
		  			{if $item.leader}{$item.leader.first_name} {$item.leader.last_name}{/if}
                  </td>
                  <td>
                    <a href="/group/edit/id/{$item.id|default}">Edit</a>
                    <a href="/group/remove/id/{$item.id|default}">Remove</a>
                  </td> 
                </tr>
              {/foreach}
              </tbody>
            </table>
          </div>
          {$this->paginationControl($data)}
          {else}
          <p>No data found</p>
          {/if}