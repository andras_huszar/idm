<?php
return array(
    'routes' => array(
        'application' => array(
            'child_routes' => array(
                // application/login
                'login' => array(
                    'type' => 'Literal',
                    'options' => array(
                        'route' => 'login',
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\User',
                            'action' => 'login'
                        )
                    ),
                ),
                // application/logout
                'logout' => array(
                    'type' => 'Literal',
                    'options' => array(
                        'route' => 'logout',
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\User',
                            'action' => 'logout'
                        )
                    )
                ),
                // application/register
                'register' => array(
                    'type' => 'Literal',
                    'options' => array(
                        'route' => 'register',
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\User',
                            'action' => 'register'
                        )
                    )
                ),
                // application/success
                'success' => array(
                    'type' => 'Literal',
                    'options' => array(
                        'route' => 'success',
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\User',
                            'action' => 'success'
                        )
                    )
                ),
                // application/hierarchy
                'hierarchy' => array(
                    'type' => 'Literal',
                    'options' => array(
                        'route' => 'hierarchy',
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\User',
                            'action' => 'hierarchy'
                        )
                    )
                ),
                'profile' => array(
                    'type' => 'Segment',
                    'options' => array(
                        'route' => 'profile[/:action][/id/:id]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                        ),
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\Profile',
                            'action' => 'index'
                        ),
                    ),
                ),
                'user' => array(
                    'type' => 'Segment',
                    'options' => array(
                        'route' => 'user[/:action][/id/:id][/page/:page][/order/:order][/dir/:dir][/limit/:limit]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'page' => '[0-9]+',
                            'id' => '[0-9]+'
                        ),
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\User',
                            'page' => 1,
                            'id' => 0,
                            'limit' => 10
                        ),
                    ),
                ),
                'role' => array(
                    'type' => 'Segment',
                    'options' => array(
                        'route' => 'role[/:action][/id/:id][/page/:page][/order/:order][/dir/:dir][/limit/:limit]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'page' => '[0-9]+',
                            'id' => '[0-9]+'
                        ),
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\Role',
                            'page' => 1,
                            'id' => 0,
                            'limit' => 10
                        ),
                    ),
                ),
                'right' => array(
                    'type' => 'Segment',
                    'options' => array(
                        'route' => 'right[/:action][/id/:id][/page/:page][/order/:order][/dir/:dir][/limit/:limit]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'page' => '[0-9]+',
                            'id' => '[0-9]+'
                        ),
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\Right',
                            'page' => 1,
                            'id' => 0,
                            'limit' => 10
                        ),
                    ),
                ),
                'group' => array(
                    'type' => 'Segment',
                    'options' => array(
                        'route' => 'group[/:action][/id/:id][/page/:page][/order/:order][/dir/:dir][/limit/:limit]',
                        'constraints' => array(
                            'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            'page' => '[0-9]+',
                            'id' => '[0-9]+'
                        ),
                        'defaults' => array(
                            'controller' => 'IdmUser\Presentation\Controller\Group',
                            'page' => 1,
                            'id' => 0,
                            'limit' => 10
                        ),
                    ),
                )
            )
        )
    )
);