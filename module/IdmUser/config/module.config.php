<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'idm_user_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/IdmUser/Domain/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'IdmUser\Domain\Entity' => 'idm_user_driver'
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'IdmUser\Presentation\Controller\User' => 'IdmUser\Presentation\Controller\UserController',
            'IdmUser\Presentation\Controller\Role' => 'IdmUser\Presentation\Controller\RoleController',
            'IdmUser\Presentation\Controller\Right' => 'IdmUser\Presentation\Controller\RightController',
            'IdmUser\Presentation\Controller\Group' => 'IdmUser\Presentation\Controller\GroupController',
            'IdmUser\Presentation\Controller\Profile' => 'IdmUser\Presentation\Controller\ProfileController',
        )
    ),
    'service_manager' => array(
            'invokables' => array(
                'IdmUser\Domain\Service\UserService' => 'IdmUser\Domain\Service\UserService',
                'IdmUser\Domain\Service\RoleService' => 'IdmUser\Domain\Service\RoleService',
                'IdmUser\Domain\Service\RightService' => 'IdmUser\Domain\Service\RightService',
                'IdmUser\Domain\Service\GroupService' => 'IdmUser\Domain\Service\GroupService',
                'IdmUser\Domain\Service\HolidayService' => 'IdmUser\Domain\Service\HolidayService',
            ),
            'factories' => array(
                'IdmUser\Domain\Service\IdentityService' => 'IdmUser\Domain\Service\IdentityServiceFactory',
            )
    ),
    'form_manager' => array(
            'invokables' => array(
                'IdmUser\Presentation\Form\UserForm' => 'IdmUser\Presentation\Form\UserForm',
                'IdmUser\Presentation\Form\GroupForm' => 'IdmUser\Presentation\Form\GroupForm',
                'IdmUser\Presentation\Form\RoleForm' => 'IdmUser\Presentation\Form\RoleForm',
                'IdmUser\Presentation\Form\RightForm' => 'IdmUser\Presentation\Form\RightForm',
                'IdmUser\Presentation\Form\LoginForm' => 'IdmUser\Presentation\Form\LoginForm',
                'IdmUser\Presentation\Form\SearchUserForm' => 'IdmUser\Presentation\Form\SearchUserForm',
            ),
            'factories' => array(
                'IdmUser\Presentation\Form\RequestAccessForm' => 'IdmUser\Presentation\Form\Service\RequestAccessFormFactory',
                'IdmUser\Presentation\Form\GrantAccessForm' => 'IdmUser\Presentation\Form\Service\GrantAccessFormFactory',
                'IdmUser\Presentation\Form\RequestPermissionForm' => 'IdmUser\Presentation\Form\Service\RequestPermissionFormFactory',
                'IdmUser\Presentation\Form\GrantPermissionForm' => 'IdmUser\Presentation\Form\Service\GrantPermissionFormFactory',
                'IdmUser\Presentation\Form\ProfileForm' => 'IdmUser\Presentation\Form\Service\ProfileFormFactory',
                'IdmUser\Presentation\Form\ChangePasswordForm' => 'IdmUser\Presentation\Form\Service\ChangePasswordFormFactory',
                'IdmUser\Presentation\Form\RegisterForm' => 'IdmUser\Presentation\Form\Service\RegisterFormFactory',
            )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    ),
    'acl' => include __DIR__ . '/acl.config.php',
    'navigation' => include __DIR__ . '/navigation.config.php',
    'router' => include __DIR__ . '/routes.config.php'
);