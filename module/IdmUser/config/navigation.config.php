<?php
return array(
    'default' => array(
        array(
            'label' => 'Users',
            'route' => 'application/user',
            'controller' => 'IdmUser\Presentation\Controller\User',
            'action' => 'index',
            'pages' => array(
                array(
                    'label' => 'Add user',
                    'route' => 'application/user',
                    'controller' => 'IdmUser\Presentation\Controller\User',
                    'action' => 'add'
                ),
                array(
                    'label' => 'Edit user',
                    'route' => 'application/user',
                    'controller' => 'IdmUser\Presentation\Controller\User',
                    'action' => 'edit',
                    'visible' => false
                ),
                array(
                    'label' => 'Hierarchy',
                    'route' => 'application/hierarchy',
                    'controller' => 'IdmUser\Presentation\Controller\User',
                    'action' => 'hierarchy'
                ),
            )
        ),
        array(
            'label' => 'Roles',
            'route' => 'application/role',
            'controller' => 'IdmUser\Presentation\Controller\Role',
            'action' => 'index',
            'pages' => array(
                array(
                    'label' => 'Add role',
                    'route' => 'application/role',
                    'controller' => 'IdmUser\Presentation\Controller\Role',
                    'action' => 'add'
                ),
                array(
                    'label' => 'Edit role',
                    'route' => 'application/role',
                    'controller' => 'IdmUser\Presentation\Controller\Role',
                    'action' => 'edit',
                    'visible' => false
                ),
                array(
                    'label' => 'Edit rigths',
                    'route' => 'application/role',
                    'controller' => 'IdmUser\Presentation\Controller\Role',
                    'action' => 'rights',
                    'visible' => false
                )
            )
        ),
        array(
            'label' => 'Rigths',
            'route' => 'application/right',
            'controller' => 'IdmUser\Presentation\Controller\Right',
            'action' => 'index',
            'pages' => array(
                array(
                    'label' => 'Add right',
                    'route' => 'application/right',
                    'controller' => 'IdmUser\Presentation\Controller\Right',
                    'action' => 'add'
                ),
                array(
                    'label' => 'Edit right',
                    'route' => 'application/right',
                    'controller' => 'IdmUser\Presentation\Controller\Right',
                    'action' => 'edit',
                    'visible' => false
                )
            )
        ),
        array(
            'label' => 'Groups',
            'route' => 'application/group',
            'controller' => 'IdmUser\Presentation\Controller\Group',
            'action' => 'index',
            'pages' => array(
                array(
                    'label' => 'Add group',
                    'route' => 'application/group',
                    'controller' => 'IdmUser\Presentation\Controller\Group',
                    'action' => 'add'
                ),
                array(
                    'label' => 'Edit group',
                    'route' => 'application/group',
                    'controller' => 'IdmUser\Presentation\Controller\Group',
                    'action' => 'edit',
                    'visible' => false
                ),
                array(
                    'label' => 'Tree',
                    'route' => 'application/group',
                    'controller' => 'IdmUser\Presentation\Controller\Group',
                    'action' => 'tree'
                )
            )
        ),
        array(
            'label' => 'Register',
            'route' => 'application/register',
            'controller' => 'IdmUser\Presentation\Controller\User',
            'action' => 'register',
            'visible' => false
        ),
        array(
            'label' => 'Login',
            'route' => 'application/login',
            'controller' => 'IdmUser\Presentation\Controller\User',
            'action' => 'login',
            'visible' => false
        ),
        array(
            'label' => 'Logout',
            'route' => 'application/logout',
            'controller' => 'IdmUser\Presentation\Controller\User',
            'action' => 'logout',
            'visible' => false
        ),
        array(
            'label' => 'Profile',
            'route' => 'application/user',
            'controller' => 'IdmUser\Presentation\Controller\User',
            'action' => 'profile',
            'visible' => false
        ),
        array(
            'label' => 'Success',
            'route' => 'application/user',
            'controller' => 'IdmUser\Presentation\Controller\User',
            'action' => 'success',
            'visible' => false
        )
    ),
    'header' => array(
        array(
            'label' => 'View Profile',
            'route' => 'application/profile',
            'controller' => 'IdmUser\Presentation\Controller\Profile',
            'action' => 'view',
        ),
        array(
            'label' => 'Edit',
            'route' => 'application/profile',
            'controller' => 'IdmUser\Presentation\Controller\Profile',
            'action' => 'edit',
        ),
        array(
            'label' => 'Change password',
            'route' => 'application/profile',
            'controller' => 'IdmUser\Presentation\Controller\Profile',
            'action' => 'change-password'
        ),
        array(
            'label' => 'Logout',
            'route' => 'application/logout',
            'controller' => 'IdmUser\Presentation\Controller\User',
            'action' => 'logout',
        )
    ),
    'profile' => array(
        array(
            'label' => 'View Profile',
            'route' => 'application/profile',
            'controller' => 'IdmUser\Presentation\Controller\Profile',
            'action' => 'view',
        ),
        array(
            'label' => 'Edit',
            'route' => 'application/profile',
            'controller' => 'IdmUser\Presentation\Controller\Profile',
            'action' => 'edit',
        ),
        array(
            'label' => 'Change password',
            'route' => 'application/profile',
            'controller' => 'IdmUser\Presentation\Controller\Profile',
            'action' => 'change-password'
        )
    )
);