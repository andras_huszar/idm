<?php
namespace IdmUser\Infrastructure\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

    public function getGroupLeaderOptions()
    {
        $result = $this->createQueryBuilder('u')
            ->where('u.is_senior = 1')
            ->orWhere('u.is_lead = 1')
            ->getQuery()
            ->getResult();
        return $result;
    }

}