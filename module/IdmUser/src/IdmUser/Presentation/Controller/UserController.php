<?php
namespace IdmUser\Presentation\Controller;

use IdmBackend\Presentation\Controller\AbstractCrudController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Result;
use Zend\Stdlib\Parameters;
use Zend\Db\Sql\Predicate\Like;

class UserController extends AbstractCrudController
{

    protected $filter;

    /**
     * @return \IdmUser\Domain\Service\UserService
     */
    public function getService()
    {
    	return $this->getServiceLocator()->get('IdmUser\Domain\Service\UserService');
    }

    /**
     * @return \IdmUser\Domain\Service\IdentityService
     */
    public function getIdentityService()
    {
    	return $this->getServiceLocator()->get('IdmUser\Domain\Service\IdentityService');
    }

    /**
     * @return \IdmUser\Presentation\Form\UserForm
     */
    public function getForm()
    {
        $form = $this->getServiceLocator()->get('FormElementManager')->get('IdmUser\Presentation\Form\UserForm');
        if ($this->params('action') == 'edit') {
            $filter = $this->getService()->getInputFilter();
        	$form->remove('password')->remove('passconf');
        	$filter->remove('password')->remove('passconf');
        }
        return $form;
    }

    /**
     * @return ViewModel
     */
    public function getQuery()
    {
        $qb = $this->getService()->createQueryBuilder();
    	$qb->select('u, partial r.{id,name}, partial g.{id,name}, partial p.{id,first_name,last_name}')
    	   ->leftJoin('u.roles', 'r')
    	   ->leftJoin('u.group', 'g')
    	   ->leftJoin('u.principal', 'p');

    	if ($this->filter['name'] && $this->filter['value']) {
    	    if ($this->filter['name'] == 'fullname') {
    	       $qb->where("CONVERT(CONCAT(CONCAT(u.first_name, ' '), u.last_name) USING utf8) LIKE :filter");
    	    } else {
    	       $qb->where("CONVERT(u.{$this->filter['name']} USING utf8) LIKE :filter");
    	    }
    	    $qb->setParameter('filter', "%{$this->filter['value']}%");
    	}
        if ($this->params('order')) {
            $dir = $this->params('dir', 'asc');
            if ($this->params('order') == 'fullname') {
                $qb->orderBy('u.first_name', $dir);
                $qb->addOrderBy('u.last_name', $dir);
            } else {
                $qb->orderBy($qb->getRootAlias().'.'.$this->params('order'), $dir);
            }
        }
        return $qb->getQuery();
    }

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $request = $this->getRequest();

        $this->filter = $this->getSession();
        if ($request->isPost()) {
            $this->filter = $request->getPost();
        }

        $view = parent::indexAction();

        $search = $this->getServiceLocator()->get('IdmUser\Presentation\Form\SearchUserForm');
        $search->setData($this->filter);
        $view->setVariable('form', $search);
        return $view;
    }

    /**
     * @return ViewModel
     */
    public function hierarchyAction()
    {
        $repsitory = $this->getServiceLocator()->get('IdmUser\Domain\Service\GroupService');
        $result = $repsitory->getRepository()->findBy(array('parent' => null));
        return new ViewModel(array(
            'result' => $result
        ));
    }

    /**
     * @return ViewModel
     */
    public function registerAction()
    {
        $service = $this->getService();
        $request = $this->getRequest();

        $user = $service->create();
        $form = $this->getServiceLocator()->get('IdmUser\Presentation\Form\RegisterForm');
        $form->bind($user);

        if ($request->isPost()) {
            $data = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($data);
            if ($form->isValid()) {
                $service->save($user);
                return $this->redirect()->toRoute('application/success');
            }
        }

        $this->layout('layout/blank');
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view->setTemplate('idm-application/form');
    }

    /**
     * @return ViewModel
     */
    public function loginAction()
    {
        if ($this->getIdentityService()->getIdentityUser()) {
            $this->redirect()->toUrl('/');
        }

        $form = $this->getServiceLocator()->get('IdmUser\Presentation\Form\LoginForm');
        $request = $this->getRequest();

        $msg = array();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $email = $form->get('email')->getValue();
                $password = $form->get('password')->getValue();
                $remember = $form->get('remember')->getValue();
                if ($this->getIdentityService()->login($email, $password, $remember)) {
    				$url = $request->getRequestUri();
    				if ($url = '/login') {
    				    $url = '/';
    				}
    				$this->redirect()->toUrl($url);
                } else {
                	$msg['error'] = 'Invalid username or password';
                }
            }
        }

        $this->layout('layout/blank');
        $view = new ViewModel(array(
			'form' => $form,
            'msg' => $msg
		));
        return $view;
    }

    /**
     * @return ViewModel
     */
    public function logoutAction()
    {
        $this->getIdentityService()->logout();
        $this->redirect()->toRoute('application/login');
    }

    /**
     * @return ViewModel
     */
    public function successAction()
    {
        $this->layout('layout/blank');
    }

    /**
     * @return ViewModel
     */
    public function deniedAction()
    {
        $this->layout('layout/blank');
        return new ViewModel();
    }

}