<?php
namespace IdmUser\Presentation\Controller;

use IdmCore\Mvc\Controller\AbstractController;
use Zend\View\Model\ViewModel;

class ReportController extends AbstractController
{

    /**
     * @return \IdmBackend\Service\UserService
     */
    public function getService()
    {
    	return $this->getServiceLocator()->get('IdmBackend\Service\RoleService');
    }

    public function addAction()
    {
    	return $this->notFoundAction();
    }

    public function editAction()
    {
    	return $this->notFoundAction();
    }

    public function removeAction()
    {
    	return $this->notFoundAction();
    }

    public function viewAction()
    {
    }

    public function csvAction()
    {
    }

    public function pdfAction()
    {
    }

}