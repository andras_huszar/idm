<?php
namespace IdmUser\Presentation\Controller;

use Zend\View\Model\ViewModel;
use IdmBackend\Presentation\Controller\AbstractCrudController;
use IdmUser\Presentation\Form\RoleForm;
use IdmUser\Presentation\Form\RoleRightForm;

class RoleController extends AbstractCrudController
{

    /**
     * @return \IdmBackend\Service\UserService
     */
    public function getService()
    {
    	return $this->getServiceLocator()->get('IdmUser\Domain\Service\RoleService');
    }

    /**
     * @see \IdmBackend\Presentation\Controller\AbstractCrudController::getForm()
     */
    public function getForm()
    {
        return new RoleForm($this->getEntityManager());
    }

    /**
     * @param \IdmUser\Domain\Entity\Role $role
     */
    public function getRightForm($role)
    {
    	$form = new RoleRightForm($this->getEntityManager());
    	$form->setHydrator($this->getService()->getHydrator())->bind($role);
    	return $form;
    }

    /**
     * @return Ambigous \Zend\Http\Response|\Zend\View\Model\ViewModel
     */
    public function rightsAction()
    {
        /* @var $rightService \IdmUser\Domain\Service\RightService */
        $rightService = $this->getServiceLocator()->get('IdmUser\Domain\Service\RightService');
        $rightService->syncToDB();

        $service = $this->getService();
        $request = $this->getRequest();

        $id = (int) $this->params('id');
        $entity = $service->findById($id);
        if (! $entity) {
            $this->notFoundAction();
        }
        $form = $this->getRightForm($entity);

        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $service->save($entity);
                return $this->redirectToIndex();
            }
        }
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view->setTemplate('idm-application/form');
    }

}