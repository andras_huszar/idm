<?php
namespace IdmUser\Presentation\Controller;

use IdmBackend\Presentation\Controller\AbstractCrudController;
use IdmUser\Presentation\Form\RightForm;

class RightController extends AbstractCrudController
{
    /**
     * @return \IdmUser\Domain\Service\RightService
     */
    public function getService()
    {
    	return $this->getServiceLocator()->get('IdmUser\Domain\Service\RightService');
    }

    public function getForm()
    {
    	return new RightForm($this->getEntityManager());
    }
}