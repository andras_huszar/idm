<?php
namespace IdmUser\Presentation\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use IdmUser\Presentation\Form\UserChangePassword;
use IdmUser\Presentation\Form\UserProfile;

class ProfileController extends AbstractActionController
{
    /**
     * @return \IdmUser\Domain\Service\UserIdentityService
     */
    public function getService()
    {
    	return $this->getServiceLocator()->get('IdmUser\Domain\Service\UserService');
    }

    /**
     * @return \IdmUser\Domain\Service\IdentityService
     */
    public function getIdentityService()
    {
    	return $this->getServiceLocator()->get('IdmUser\Domain\Service\IdentityService');
    }

    /**
     * @return ViewModel
     */
    public function getUser()
    {
        // Got id, but user not
        if ($id = $this->params('id') && !($user = $this->getService()->getPictureByPath($id))) {
            $this->notFoundAction();
        } elseif (!$id) {
            $user = $this->getIdentityService()->getIdentityUser();
        }
        return $user;
    }

    /**
     * @return ViewModel
     */
    public function viewAction()
    {
	    $user = $this->getUser();
	    return new ViewModel(array(
	    	'user' => $user
	    ));
    }

    /**
     * @return ViewModel
     */
    public function editAction()
    {
	    $user = $this->getUser();
	    $form = $this->getServiceLocator()->get('IdmUser\Presentation\Form\UserProfile');
	    $form->bind($user);

	    $request = $this->getRequest();
	    if ($request->isPost()) {
	        $form->setData($request->getPost());
	        if ($form->isValid()) {
	            $this->getService()->save($user);
	            $this->redirect('application/profile', array('action' => 'view'));
	        }
	    }

        $view = new ViewModel(array(
	    	'user' => $user,
	    	'form' => $form
	    ));
        return $view->setTemplate('idm-user/profile/view');
    }

    /**
     * @return ViewModel
     */
    public function changePasswordAction()
    {
        $service = $this->getService();
        $request = $this->getRequest();

        $user = $service->getIdentityUser();
        $form = $this->getServiceLocator()->get('IdmUser\Presentation\Form\ChangePasswordForm');

        $msg = array();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $form->setData($data);
            $oldPassword = $form->get('old_password')->getValue();
            $newPassword = $form->get('password')->getValue();

            // Check old password is correct
            if ($form->isValid() && $this->getService()->verifyPassword($user, $oldPassword)) {
                $service->changePassword($user, $newPassword);
                $msg['success'] = 'Password successful changed';
            } else {
                $msg['error'] = 'Invalid password';
            }
        }

        $view = new ViewModel(array(
            'form' => $form,
            'msg' => $msg,
        ));
        return $view->setTemplate('idm-application/form');
    }
}