<?php
namespace IdmUser\Presentation\Controller;

use IdmBackend\Presentation\Controller\AbstractCrudController;
use Zend\View\Model\ViewModel;
use IdmUser\Presentation\Form\GroupForm;

class GroupController extends AbstractCrudController
{
    /**
     * @return \IdmUser\Domain\Service\GroupService
     */
    public function getService()
    {
    	return $this->getServiceLocator()->get('IdmUser\Domain\Service\GroupService');
    }

    /**
     * @see \IdmBackend\Presentation\Controller\AbstractCrudController::getForm()
     */
    public function getForm()
    {
    	return $this->getServiceLocator()->get('IdmUser\Presentation\Form\GroupForm');
    }

    /**
     * @see \IdmBackend\Presentation\Controller\AbstractCrudController::getQuery()
     */
    public function getQuery()
    {
        $qb = $this->getService()->createQueryBuilder();
        $qb->select('g, partial p.{id,name}, partial l.{id,first_name,last_name}')
            ->leftJoin('g.parent', 'p')
            ->leftJoin('g.leader', 'l')
            ->orderBy('p.id', 'asc');
		$order = $this->params('order');
        if ($order) {
            $qb->orderBy($order, $this->params('dir', 'asc'));
        }
        return $qb->getQuery();
    }

    /**
     * @return \Zend\View\Model\ViewModel
     */
    public function treeAction()
    {
        $result = $this->getService()->getRepository()->findBy(array('parent' => null));
        return new ViewModel(array(
            'result' => $result
        ));
    }
}