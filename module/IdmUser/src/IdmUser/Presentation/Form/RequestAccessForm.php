<?php
namespace IdmUser\Presentation\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;

class RequestAccessForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('user\presentation');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'name',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'name-field',
                'id' => 'name_field',
                'placeholder' => 'New user\'s fullname',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'Zend\Form\Element\Email',
            'attributes' => array(
                'placeholder' => 'New user\'s email',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
        ));
    }
}