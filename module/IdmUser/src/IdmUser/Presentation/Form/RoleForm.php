<?php
namespace IdmUser\Presentation\Form;

use Zend\Form\Form;
use Doctrine\ORM\EntityManager;

class RoleForm extends Form
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    /**
     *
     * @var UserFilter
     */
    protected $inputFilter;

    /**
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        parent::__construct('role-form');

        $this->em = $em;

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'name',
            'options' => array(
                'label' => 'Name'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'name'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Submit'
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-primary'
            )
        ));
    }
}