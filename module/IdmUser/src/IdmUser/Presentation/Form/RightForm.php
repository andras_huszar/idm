<?php
namespace IdmUser\Presentation\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Doctrine\ORM\EntityManager;
use Zend\Validator\Digits;
use Zend\Validator\StringLength;
use DoctrineModule\Validator\UniqueObject;
use IdmCore\ServiceManager\EntityManagerAwareInterface;

class RightForm extends Form implements EntityManagerAwareInterface
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    /**
     *
     * @var UserFilter
     */
    protected $inputFilter;

    /**
     */
    public function __construct()
    {
        parent::__construct('right-form');

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'name',
            'options' => array(
                'label' => 'Name'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'name'
            )
        ));
        $this->add(array(
            'name' => 'resource',
            'options' => array(
                'label' => 'Resource'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'resource'
            )
        ));
        $this->add(array(
            'name' => 'privilege',
            'options' => array(
                'label' => 'Privilege'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'privilege'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Submit'
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-primary'
            )
        ));
    }

    /**
     * @see \IdmCore\ServiceManager\EntityManagerAwareInterface::getEntityManager()
     */
    public function getEntityManager()
    {
    	return $this->em;
    }

    /**
     * @see \IdmCore\ServiceManager\EntityManagerAwareInterface::setEntityManager()
     */
    public function setEntityManager(EntityManager $em)
    {
    	$this->em = $em;
    	return $this;
    }

    /**
     * @todo create new inputfilter
     */
    public function getInputFilter()
    {
        if ($this->inputFilter === null) {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'id',
                'required' => false,
                'validators' => array(
                    new Digits(),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'name',
                'required' => true,
                'validators' => array(
                    new StringLength(array('max' => 64)),
                    new UniqueObject(array(
                        'object_manager' => $this->getEntityManager(),
                        'object_repository' => $this->getEntityManager()->getRepository('IdmUser\Domain\Entity\Role'),
                        'fields' => 'name'
                    )),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'resource',
                'required' => true,
                'validators' => array(
                    new StringLength(array('max' => 64)),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'privilege',
                'required' => true,
                'validators' => array(
                    new StringLength(array('max' => 64)),
                ),
            ));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}