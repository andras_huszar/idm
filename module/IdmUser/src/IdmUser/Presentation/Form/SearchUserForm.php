<?php
namespace IdmUser\Presentation\Form;

use Zend\Form\Form;

class SearchUserForm extends Form
{

    public function __construct()
    {
        parent::__construct('user_search');

        $this->add(array(
            'name' => 'name',
            'type' => 'select',
            'options' => array(
                'value_options' => array(
                    '' => 'Filter by',
                    'email' => 'Email',
                    'fullname' => 'Name',
                    'city' => 'City',
                )
            )
        ));
        $this->add(array(
            'name' => 'value',
            'attributes' => array(
                'type' => 'text',
                'placeholder' => 'Keywords'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Search'
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-primary'
            )
        ));
    }

}