<?php
namespace IdmUser\Presentation\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;
use Zend\Validator\Identical;

class ChangePasswordForm extends Form
{

    /**
     *
     * @var UserFilter
     */
    protected $inputFilter;

    public function __construct()
    {
        parent::__construct('user-change-password');

        $this->add(array(
            'name' => 'old_password',
            'options' => array(
                'label' => 'Old password'
            ),
            'attributes' => array(
                'type' => 'password',
                'class' => 'password'
            )
        ));

        $this->add(array(
            'name' => 'password',
            'options' => array(
                'label' => 'Password'
            ),
            'attributes' => array(
                'type' => 'password',
                'class' => 'password'
            )
        ));
        $this->add(array(
            'name' => 'passconf',
            'options' => array(
                'label' => 'Password confirm'
            ),
            'attributes' => array(
                'type' => 'password',
                'class' => 'passconf'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Change password'
            ),
            'attributes' => array(
                'type' => 'submit'
            )
        ));
    }

    public function getInputFilter()
    {
        if ($this->inputFilter === null) {
            $inputFilter = parent::getInputFilter();

            $inputFilter->add(array(
                'name' => 'old_password',
                'required' => true,
                'validators' => array(
    		        new StringLength(array('min' => 6, 'max' => 24)),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'password',
                'required' => true,
                'validators' => array(
    		        new StringLength(array('min' => 6, 'max' => 24)),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'passconf',
                'required' => false,
                'validators' => array(
    		        new StringLength(array('min' => 6, 'max' => 24)),
    		        $identical = new Identical(array('token' => 'password')),
                ),
            ));
        	$identical->setMessage('Passwords must match', Identical::NOT_SAME);

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}