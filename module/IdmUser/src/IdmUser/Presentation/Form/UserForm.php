<?php
namespace IdmUser\Presentation\Form;

use Zend\Form\Form;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Form\Element\ObjectMultiCheckbox;
use IdmCore\ServiceManager\EntityManagerAwareInterface;

class UserForm extends Form implements EntityManagerAwareInterface
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    /**
     */
    public function init()
    {
        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'email',
            'options' => array(
                'label' => 'Email'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'email'
            )
        ));
        $this->add(array(
            'name' => 'password',
            'options' => array(
                'label' => 'Password'
            ),
            'attributes' => array(
                'type' => 'password',
                'class' => 'password'
            )
        ));
        $this->add(array(
            'name' => 'passconf',
            'options' => array(
                'label' => 'Password confirm'
            ),
            'attributes' => array(
                'type' => 'password',
                'class' => 'passconf'
            )
        ));
        $this->add(array(
            'name' => 'first_name',
            'options' => array(
                'label' => 'First name'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'first_name'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'options' => array(
                'label' => 'Last name'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'last_name'
            )
        ));
        $this->add(array(
            'name' => 'city',
            'options' => array(
                'label' => 'City'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'address'
            )
        ));
        $this->add(array(
            'name' => 'address',
            'options' => array(
                'label' => 'Address'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'address'
            )
        ));
        $this->add(array(
            'name' => 'phone',
            'options' => array(
                'label' => 'Phone'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'phone'
            )
        ));
        $this->add(array(
            'name' => 'gender',
            'type' => 'radio',
            'options' => array(
                'label' => 'Gender',
                'value_options' => array(
                    'm' => 'Male',
                    'f' => 'Female'
                )
            ),
            'attributes' => array(
                'class' => 'gender'
            )
        ));
        $this->add(array(
            'name' => 'picture',
        	'type' => 'IdmCore\Form\Element\Picture',
            'options' => array(
        	    'label' => 'Picture'
            ),
            'attributes' => array(
                'class' => 'picture'
            ),
        ));
        $this->add(array(
            'name' => 'is_senior',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Senior'
            ),
            'attributes' => array(
                'class' => 'is_senior',
                'privilege' => 'edit.employment'
            )
        ));
        $this->add(array(
            'name' => 'is_lead',
            'type' => 'checkbox',
            'options' => array(
                'label' => 'Lead'
            ),
            'attributes' => array(
                'class' => 'is_lead',
                'privilege' => 'edit.employment'
            )
        ));
        $this->add(array(
            'name' => 'is_active',
        	'type' => 'radio',
            'options' => array(
                'label' => 'Status',
            	'value_options' => array(
            		1 => 'Active',
            		0 => 'Inactive'
            	)
            ),
            'attributes' => array(
                'type' => 'radio',
                'class' => 'is_active',
                'privilege' => 'edit.employment'
            )
        ));
        $this->add(array(
            'name' => 'admited_at',
            'type' => 'date-select',
            'allowEmpty' => true,
            'options' => array(
                'label' => 'Admited at',
                'create_empty_option' => true
            ),
            'attributes' => array(
                'class' => 'admited_at',
                'privilege' => 'edit.employment'
            )
        ));
        $this->add(array(
            'name' => 'fired_at',
            'type' => 'date-select',
            'allowEmpty' => true,
            'options' => array(
                'label' => 'Fired at',
                'create_empty_option' => true
            ),
            'attributes' => array(
                'class' => 'fired_at',
                'privilege' => 'edit.employment'
            )
        ));
        $this->add(array(
            'name' => 'title',
            'options' => array(
                'label' => 'Title'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'title'
            )
        ));
        $this->add(array(
            'name' => 'group',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'label' => 'Group',
                'object_manager' => $this->getEntityManager(),
                'target_class' => 'IdmUser\Domain\Entity\Group',
                'property' => 'name'
            ),
            'attributes' => array(
                'class' => 'group',
                'privilege' => 'admin_user'
            )
        ));
        $this->add(array(
            'name' => 'roles',
            'type' => 'DoctrineModule\Form\Element\ObjectMultiCheckbox',
            'options' => array(
                'label' => 'Roles',
                'object_manager' => $this->getEntityManager(),
                'target_class' => 'IdmUser\Domain\Entity\Role',
                'property' => 'name'
            ),
            'attributes' => array(
                'class' => 'role',
                'privilege' => 'admin_user'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Submit'
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-primary'
            )
        ));
    }

    /**
     * @see \IdmCore\ServiceManager\EntityManagerAwareInterface::getEntityManager()
     */
    public function getEntityManager()
    {
    	return $this->em;
    }

    /**
     * @see \IdmCore\ServiceManager\EntityManagerAwareInterface::setEntityManager()
     */
    public function setEntityManager(EntityManager $em)
    {
    	$this->em = $em;
    	return $this;
    }
}