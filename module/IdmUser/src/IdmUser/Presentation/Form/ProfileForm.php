<?php
namespace IdmUser\Presentation\Form;

use Zend\InputFilter\InputFilterInterface;
use Zend\Form\Fieldset;
use Zend\Form\Form;

class ProfileForm extends Form
{
    public function __construct()
    {
        parent::__construct();

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));

        $account = new Fieldset('account');
        $account->setLabel('Account Settings');
        $account->add(array(
            'name' => 'email',
            'options' => array(
                'label' => 'Email'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'email'
            )
        ));
        // New
        $account->add(array(
            'name' => 'username',
            'options' => array(
                'label' => 'Username'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'username'
            )
        ));
        $this->add($account);

        $profile = new Fieldset('profile');
        $profile->setLabel('Profile Setting');
        $profile->add(array(
            'name' => 'first_name',
            'options' => array(
                'label' => 'First name'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'first_name'
            )
        ));
        $profile->add(array(
            'name' => 'last_name',
            'options' => array(
                'label' => 'Last name'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'last_name'
            )
        ));
        $profile->add(array(
            'name' => 'gender',
            'type' => 'radio',
            'options' => array(
                'label' => 'Gender',
                'value_options' => array(
                    'm' => 'Male',
                    'f' => 'Female'
                )
            ),
            'attributes' => array(
                'class' => 'gender'
            )
        ));
        $profile->add(array(
            'name' => 'address',
            'options' => array(
                'label' => 'Address'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'address'
            )
        ));
        // New
        $profile->add(array(
            'name' => 'marital_status',
        	'type' => 'radio',
            'options' => array(
                'label' => 'Marital Status',
            	'value_options' => array(
            		1 => 'Single',
            		0 => 'Maried'
            	)
            ),
            'attributes' => array(
                'type' => 'radio',
                'class' => 'marital_status'
            )
        ));
        $profile->add(array(
            'name' => 'about',
            'options' => array(
                'label' => 'About'
            ),
            'attributes' => array(
                'type' => 'textarea',
                'class' => 'about'
            )
        ));
        $this->add($profile);


        $contact = new Fieldset('contact');
        $contact->setLabel('Contact Setting');
        $contact->add(array(
            'name' => 'phone',
            'options' => array(
                'label' => 'Phone'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'phone'
            )
        ));
        // New
        $contact->add(array(
            'name' => 'mobile_phone',
            'options' => array(
                'label' => 'Mobile Phone'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'mobile_phone'
            )
        ));
        // New
        $contact->add(array(
            'name' => 'private_phone',
            'options' => array(
                'label' => 'Private Phone'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'private_phone'
            )
        ));
        // New
        $contact->add(array(
            'name' => 'website',
            'options' => array(
                'label' => 'Website'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'website'
            )
        ));
        // New
        $contact->add(array(
            'name' => 'facebook',
            'options' => array(
                'label' => 'Facebook'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'facebook'
            )
        ));
        // New
        $contact->add(array(
            'name' => 'twitter',
            'options' => array(
                'label' => 'Twitter'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'twitter'
            )
        ));
        // New
        $contact->add(array(
            'name' => 'linkedin',
            'options' => array(
                'label' => 'Linkedin'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'linkedin'
            )
        ));
        $this->add($contact);


        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Submit'
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-primary'
            )
        ));
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {

    }

}