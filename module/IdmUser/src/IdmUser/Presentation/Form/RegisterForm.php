<?php
namespace IdmUser\Presentation\Form;

use IdmUser\Presentation\Form\UserForm;
use Zend\Captcha\Image as Captcha;

class RegisterForm extends UserForm
{
    protected $fieldset = array(
    	'id', 'first_name', 'last_name', 'picture', 'gender', 'phone', 'country', 'city', 'address', 'email', 'password', 'passconf', 'captcha'
    );

    public function __construct()
    {
        parent::__construct();
        $this->setName('profile-form');

        //pass captcha image options
        $captchaImage = new Captcha(array(
            'font' => './data/fonts/arial.ttf',
            'width' => 150,
            'height' => 50,
            'dotNoiseLevel' => 100,
            'lineNoiseLevel' => 10
        ));
        $captchaImage->setImgDir('./data/captcha');
        $captchaImage->setImgUrl('captcha');

        $this->add(array(
            'type' => 'Zend\Form\Element\Captcha',
            'name' => 'captcha',
            'options' => array(
                'label' => 'Please verify you are human',
                'captcha' => $captchaImage,
            ),
        ));

        $inputFilter = $this->getInputFilter();

        foreach ($this->getElements() as $element) {
            $name = $element->getName();
            if (!in_array($name, $this->fieldset)) {
                $this->remove($name);
                $this->getInputFilter()->remove($name);
            }
        }

        $this->getInputFilter()->get('first_name')->setRequired(true);
        $this->getInputFilter()->get('last_name')->setRequired(true);
        $this->getInputFilter()->get('phone')->setRequired(true);

        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Submit'
            ),
            'attributes' => array(
                'type' => 'submit'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'type' => 'button',
            'options' => array(
                'label' => 'Back'
            ),
            'attributes' => array(
                'type' => 'button',
                'onclick' => 'history.go(-1);return true;'
            )
        ));
        $this->add(array(
            'name' => 'clear',
            'type' => 'button',
            'options' => array(
                'label' => 'Clear'
            ),
            'attributes' => array(
                'type' => 'reset'
            )
        ));
    }

}