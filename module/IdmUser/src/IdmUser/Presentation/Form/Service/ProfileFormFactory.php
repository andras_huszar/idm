<?php
namespace IdmUser\Presentation\Form\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use IdmUser\Presentation\Form\UserProfile;

class ProfileFormFactory implements FactoryInterface
{

    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $service = $services->get('IdmUser\Domain\Service\UserService');
        $em = $services->get('Doctrine\ORM\EntityManager');

        $form = new UserProfile($em);
        $form->setInputFilter($service->getInputFilter());
        $form->setHydrator($service->getHydrator());

        return $form;
    }

}