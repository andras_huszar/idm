<?php
namespace IdmUser\Presentation\Form\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RegisterFormFactory implements FactoryInterface
{
    protected $configKey = 'my_service';

    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $config = $services->get('Config');
        $config = isset($config[$this->configKey]) ? $config[$this->configKey] : array();

        return new RegisterForm($config);
    }
}