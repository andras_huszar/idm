<?php
namespace IdmUser\Presentation\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;
use Zend\Validator\StringLength;
use Zend\Filter\Boolean;

class LoginForm extends Form
{
    protected $inputFilter;

    public function __construct()
    {
        parent::__construct('user-login');

        $this->add(array(
            'name' => 'email',
            'label' => 'Email',
            'attributes' => array(
                'type' => 'text',
                'placeholder' => 'Email address',
                'class' => 'email'
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'label' => 'Password',
            'attributes' => array(
                'type' => 'password',
                'placeholder' => 'Password',
                'class' => 'password'
            ),
        ));
        $this->add(array(
            'type' => 'checkbox',
            'name' => 'remember',
            'label' => 'Remember',
            'options' => array(
                'label' => 'Remember me'
            ),
            'attributes' => array(
                'type' => 'checkbox',
                'class' => 'remember'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Login'
            ),
            'attributes' => array(
                'type' => 'submit'
            )
        ));
    }

    public function getInputFilter()
    {
        if ($this->inputFilter === null) {
            $this->inputFilter = new InputFilter();
            $this->inputFilter->add(array(
                'name' => 'email',
                'required' => true,
                'validators' => array(
                    new EmailAddress(array('useMxCheck' => false)),
                    new StringLength(array('max' => 128)),
                ),
            ));
            $this->inputFilter->add(array(
                'name' => 'password',
                'required' => true,
                'validators' => array(
                    new StringLength(array('min' => 6, 'max' => 64)),
                ),
            ));
            $this->inputFilter->add(array(
                'name' => 'remember',
                'required' => false,
                'filters' => array(
            	    new Boolean()
                )
            ));
        }
        return $this->inputFilter;
    }
}