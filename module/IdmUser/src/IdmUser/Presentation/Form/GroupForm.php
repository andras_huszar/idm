<?php
namespace IdmUser\Presentation\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Doctrine\ORM\EntityManager;
use Zend\Validator\Digits;
use Zend\Validator\StringLength;
use DoctrineModule\Validator\UniqueObject;
use IdmCore\ServiceManager\EntityManagerAwareInterface;

class GroupForm extends Form implements EntityManagerAwareInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     */
    public function __construct()
    {
        parent::__construct('group-form');

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'name',
            'options' => array(
                'label' => 'Name'
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'name'
            )
        ));
        $this->add(array(
            'name' => 'parent',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'label' => 'Parent',
                'object_manager' => $this->getEntityManager(),
                'target_class' => 'IdmUser\Domain\Entity\Group',
                'property' => 'name'
            ),
            'attributes' => array(
                'class' => 'parent'
            )
        ));
        $this->add(array(
            'name' => 'leader',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'label' => 'Leader',
                'object_manager' => $this->getEntityManager(),
                'target_class' => 'IdmUser\Domain\Entity\User',
                'property' => 'email',
                'find_method' => array(
                    'name' => 'getGroupLeaderOptions'
                )
            ),
            'attributes' => array(
                'class' => 'parent'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'options' => array(
                'label' => 'Submit'
            ),
            'attributes' => array(
                'type' => 'submit',
                'class' => 'btn btn-primary'
            )
        ));
    }

    /**
     * @see \IdmCore\ServiceManager\EntityManagerAwareInterface::getEntityManager()
     */
    public function getEntityManager()
    {
    	return $this->em;
    }

    /**
     * @see \IdmCore\ServiceManager\EntityManagerAwareInterface::setEntityManager()
     */
    public function setEntityManager(EntityManager $em)
    {
    	$this->em = $em;
    	return $this;
    }
}