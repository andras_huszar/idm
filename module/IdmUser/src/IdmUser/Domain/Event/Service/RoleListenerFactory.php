<?php
namespace IdmUser\Domain\Event\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use IdmUser\Event\RoleListener;

class RoleListenerFactory implements FactoryInterface
{

    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $service = $services->get('IdmUser\Domain\Service\RoleService');
        return new RoleListener($service);
    }

}