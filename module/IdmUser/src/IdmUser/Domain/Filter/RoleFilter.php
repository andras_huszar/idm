<?php
namespace IdmUser\Domain\Filter;

use Zend\InputFilter\InputFilter;
use Doctrine\ORM\EntityManager;
use Zend\Validator\Digits;
use Zend\Validator\StringLength;
use DoctrineModule\Validator\UniqueObject;

class RoleFilter extends InputFilter
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;

        $this->add(array(
            'name' => 'id',
            'required' => false,
            'validators' => array(
                new Digits(),
            ),
        ));
        $this->add(array(
            'name' => 'name',
            'required' => false,
            'validators' => array(
                new StringLength(array('max' => 64)),
                new UniqueObject(array(
                    'object_manager' => $this->em,
                    'object_repository' => $this->em->getRepository('IdmUser\Domain\Entity\Role'),
                    'fields' => 'name'
                )),
            ),
        ));
    }
}