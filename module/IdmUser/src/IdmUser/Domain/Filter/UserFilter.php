<?php
namespace IdmUser\Domain\Filter;

use Zend\InputFilter\InputFilter;
use Doctrine\ORM\EntityManager;
use Zend\Validator\Digits;
use Zend\Validator\EmailAddress;
use Zend\Validator\StringLength;
use Zend\Validator\Identical;
use Zend\Validator\File\MimeType;
use Zend\Validator\File\Size;
use Zend\Validator\File\ImageSize;
use Zend\Validator\Date;
use Zend\Validator\Callback;
use Zend\Filter\File\RenameUpload;
use Zend\Filter\Boolean;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use DoctrineModule\Validator\UniqueObject;
use IdmCore\Form\StaticHelper;

class UserFilter extends InputFilter
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;

        $this->add(array(
            'name' => 'id',
            'required' => false,
            'validators' => array(
                new Digits(),
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                new StringTrim(),
                new StripTags()
            ),
            'validators' => array(
                new EmailAddress(array('useMxCheck' => false)),
                new StringLength(array('max' => 64)),
                new UniqueObject(array(
                    'object_manager' => $this->em,
                    'object_repository' => $this->em->getRepository('IdmUser\Domain\Entity\User'),
                    'fields' => 'email'
                )),
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'required' => true,
            'validators' => array(
                new StringLength(array('min' => 6, 'max' => 24)),
            ),
        ));
        $this->add(array(
            'name' => 'passconf',
            'required' => false,
            'validators' => array(
                new StringLength(array('min' => 6, 'max' => 24)),
                $identical = new Identical(array('token' => 'password')),
            ),
        ));
        $identical->setMessage('Passwords must match', Identical::NOT_SAME);

        $this->add(array(
            'name' => 'first_name',
            'required' => false,
            'filters' => array(
                new StringTrim(),
                new StripTags()
            ),
            'validators' => array(
                new StringLength(array('max' => 64)),
            ),
        ));
        $this->add(array(
            'name' => 'last_name',
            'required' => false,
            'filters' => array(
                new StringTrim(),
                new StripTags()
            ),
            'validators' => array(
                new StringLength(array('max' => 64)),
            ),
        ));
        $this->add(array(
            'name' => 'gender',
            'filters' => array(
                new StringTrim(),
                new StripTags()
            ),
            'required' => false,
        ));
        $this->add(array(
            'name' => 'city',
            'required' => false,
            'filters' => array(
                new StringTrim(),
                new StripTags()
            ),
            'validators' => array(
                new StringLength(array('max' => 64)),
            ),
        ));
        $this->add(array(
            'name' => 'address',
            'required' => false,
            'filters' => array(
                new StringTrim(),
                new StripTags()
            ),
            'validators' => array(
                new StringLength(array('max' => 128)),
            ),
        ));
        $this->add(array(
            'name' => 'phone',
            'required' => false,
            'filters' => array(
                new StringTrim(),
                new StripTags()
            ),
            'validators' => array(
                new StringLength(array('max' => 16)),
            )
        ));
        $this->add(array(
            'name' => 'picture',
            'filters' => array(
                new StringTrim()
            ),
            'required' => false,
        ));
        $this->add(array(
            'name' => 'is_active',
            'required' => false,
            'filters' => array(
                new Boolean()
            )
        ));
        $this->add(array(
            'name' => 'is_senior',
            'required' => false,
            'filters' => array(
                new Boolean()
            )
        ));
        $this->add(array(
            'name' => 'is_lead',
            'required' => false,
            'filters' => array(
                new Boolean()
            )
        ));
        $this->add(array(
            'name' => 'title',
            'required' => false,
            'filters' => array(
                new StringTrim(),
                new StripTags()
            ),
            'validators' => array(
                new StringLength(array('max' => 128)),
            ),
        ));
        $this->add(array(
            'name' => 'group',
            'required' => false
        ));
        $this->add(array(
            'name' => 'roles',
            'required' => false
        ));
        $admitedAt = StaticHelper::dateSelect('admited_at');
        $this->add($admitedAt);
        $firedAt = StaticHelper::dateSelect('fired_at');
        $firedAt['validators'][] = new Callback(array(
            'message' => array(
                Callback::INVALID_VALUE => 'Invalid period is given.',
            ),
            'callback' => function($value, $context) {
                if ($value) {
                    if (!array_filter($context['admited_at'])) {
                        return false;
                    }
                    return strtotime($value) > strtotime(implode('-', $context['admited_at']));
                }
                return true;
            }
        ));
        $this->add($firedAt);
    }
}