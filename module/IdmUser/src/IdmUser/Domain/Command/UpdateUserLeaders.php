<?php
namespace IdmUser\Domain\Command;

use Doctrine\DBAL\Connection;

class UpdateUserLeaders
{

    protected $em;

    public function __construct($em)
    {
    	$this->em = $em;
    }

    public function execute()
    {
        foreach ($this->em->getRepository('IdmUser\Domain\Entity\Group')->findAll() as $group) {
            $leader = $group->getLeader();
            foreach ($group->getMembers() as $member) {
                $member->setGroupLeader($leader);
                $this->em->persist($member);
            }
            $prtGroup = $group->getParent();
            if ($prtGroup) {
                $leader->setGroupLeader($prtGroup->getLeader());
            } else {
                $leader->setGroupLeader(null);
            }
            $this->em->persist($leader);
        }
        $this->em->flush();
    }

}