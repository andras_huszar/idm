<?php
namespace IdmUser\Domain\Command;

use Doctrine\DBAL\Connection;

class SyncRightsFromArray
{

    protected $conn;

    public function __construct(Connection $conn)
    {
    	$this->conn = $conn;
    }

    public function execute($rights)
    {
        $items = array();
        foreach ($rights as $right) {
            if (isset($right['controller']) && isset($right['action'])) {
                $items[] = "('{$right['label']}', '{$right['controller']}', '{$right['action']}')";
            }
        }
        $query = "INSERT IGNORE INTO `rights` (name, resource, privilege) VALUES " . implode(', ', $items);
        return $this->conn->executeUpdate($query);
    }

}