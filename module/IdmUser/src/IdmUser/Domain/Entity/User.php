<?php

namespace IdmUser\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user", indexes={@ORM\Index(name="email_index", columns={"email"})})
 * @ORM\Entity(repositoryClass="IdmUser\Infrastructure\Repository\UserRepository")
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=64)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=64, nullable=true)
     */
    private $first_name;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=64, nullable=true)
     */
    private $last_name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=1, nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=64, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=128, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=16, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $is_active;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_senior", type="boolean")
     */
    private $is_senior;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_lead", type="boolean")
     */
    private $is_lead;

    /**
     * @var guid
     *
     * @ORM\Column(name="uuid", type="guid")
     */
    private $uuid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="admited_at", type="date", nullable=true)
     */
    private $admited_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fired_at", type="date", nullable=true)
     */
    private $fired_at;

    /**
     * @var integer
     *
     * @ORM\Column(name="vacation_days", type="integer")
     */
    private $vacation_days;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="IdmReporting\Domain\Entity\Report", mappedBy="requester")
     */
    private $actions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="IdmUser\Domain\Entity\Group", mappedBy="leader")
     */
    private $managed_group;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="IdmUser\Domain\Entity\User", mappedBy="principal")
     */
    private $subordinates;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="IdmHoliday\Domain\Entity\Holiday", mappedBy="requester")
     */
    private $holidays;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="IdmFile\Domain\Entity\File", mappedBy="uploader")
     */
    private $uploads;

    /**
     * @var \IdmUser\Domain\Entity\Group
     *
     * @ORM\ManyToOne(targetEntity="IdmUser\Domain\Entity\Group", inversedBy="members")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     * })
     */
    private $group;

    /**
     * @var \IdmUser\Domain\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="IdmUser\Domain\Entity\User", inversedBy="subordinates")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="principal_id", referencedColumnName="id")
     * })
     */
    private $principal;

    /**
     * @var \IdmHoliday\Domain\Entity\Holiday
     *
     * @ORM\ManyToOne(targetEntity="IdmHoliday\Domain\Entity\Holiday", inversedBy="possible_replacements")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="possible_replacements_id", referencedColumnName="id")
     * })
     */
    private $possible_replacements;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="IdmUser\Domain\Entity\Role", inversedBy="users")
     * @ORM\JoinTable(name="user__role",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $roles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->managed_group = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subordinates = new \Doctrine\Common\Collections\ArrayCollection();
        $this->holidays = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uploads = new \Doctrine\Common\Collections\ArrayCollection();
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return User
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set picture
     *
     * @param string $picture
     * @return User
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return string 
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set is_senior
     *
     * @param boolean $isSenior
     * @return User
     */
    public function setIsSenior($isSenior)
    {
        $this->is_senior = $isSenior;

        return $this;
    }

    /**
     * Get is_senior
     *
     * @return boolean 
     */
    public function getIsSenior()
    {
        return $this->is_senior;
    }

    /**
     * Set is_lead
     *
     * @param boolean $isLead
     * @return User
     */
    public function setIsLead($isLead)
    {
        $this->is_lead = $isLead;

        return $this;
    }

    /**
     * Get is_lead
     *
     * @return boolean 
     */
    public function getIsLead()
    {
        return $this->is_lead;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     * @return User
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set admited_at
     *
     * @param \DateTime $admitedAt
     * @return User
     */
    public function setAdmitedAt($admitedAt)
    {
        $this->admited_at = $admitedAt;

        return $this;
    }

    /**
     * Get admited_at
     *
     * @return \DateTime 
     */
    public function getAdmitedAt()
    {
        return $this->admited_at;
    }

    /**
     * Set fired_at
     *
     * @param \DateTime $firedAt
     * @return User
     */
    public function setFiredAt($firedAt)
    {
        $this->fired_at = $firedAt;

        return $this;
    }

    /**
     * Get fired_at
     *
     * @return \DateTime 
     */
    public function getFiredAt()
    {
        return $this->fired_at;
    }

    /**
     * Set vacation_days
     *
     * @param integer $vacationDays
     * @return User
     */
    public function setVacationDays($vacationDays)
    {
        $this->vacation_days = $vacationDays;

        return $this;
    }

    /**
     * Get vacation_days
     *
     * @return integer 
     */
    public function getVacationDays()
    {
        return $this->vacation_days;
    }

    /**
     * Add actions
     *
     * @param \IdmReporting\Domain\Entity\Report $actions
     * @return User
     */
    public function addAction(\IdmReporting\Domain\Entity\Report $actions)
    {
        $this->actions[] = $actions;

        return $this;
    }

    /**
     * Remove actions
     *
     * @param \IdmReporting\Domain\Entity\Report $actions
     */
    public function removeAction(\IdmReporting\Domain\Entity\Report $actions)
    {
        $this->actions->removeElement($actions);
    }

    /**
     * Get actions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * Add managed_group
     *
     * @param \IdmUser\Domain\Entity\Group $managedGroup
     * @return User
     */
    public function addManagedGroup(\IdmUser\Domain\Entity\Group $managedGroup)
    {
        $this->managed_group[] = $managedGroup;

        return $this;
    }

    /**
     * Remove managed_group
     *
     * @param \IdmUser\Domain\Entity\Group $managedGroup
     */
    public function removeManagedGroup(\IdmUser\Domain\Entity\Group $managedGroup)
    {
        $this->managed_group->removeElement($managedGroup);
    }

    /**
     * Get managed_group
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getManagedGroup()
    {
        return $this->managed_group;
    }

    /**
     * Add subordinates
     *
     * @param \IdmUser\Domain\Entity\User $subordinates
     * @return User
     */
    public function addSubordinate(\IdmUser\Domain\Entity\User $subordinates)
    {
        $this->subordinates[] = $subordinates;

        return $this;
    }

    /**
     * Remove subordinates
     *
     * @param \IdmUser\Domain\Entity\User $subordinates
     */
    public function removeSubordinate(\IdmUser\Domain\Entity\User $subordinates)
    {
        $this->subordinates->removeElement($subordinates);
    }

    /**
     * Get subordinates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubordinates()
    {
        return $this->subordinates;
    }

    /**
     * Add holidays
     *
     * @param \IdmHoliday\Domain\Entity\Holiday $holidays
     * @return User
     */
    public function addHoliday(\IdmHoliday\Domain\Entity\Holiday $holidays)
    {
        $this->holidays[] = $holidays;

        return $this;
    }

    /**
     * Remove holidays
     *
     * @param \IdmHoliday\Domain\Entity\Holiday $holidays
     */
    public function removeHoliday(\IdmHoliday\Domain\Entity\Holiday $holidays)
    {
        $this->holidays->removeElement($holidays);
    }

    /**
     * Get holidays
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHolidays()
    {
        return $this->holidays;
    }

    /**
     * Add uploads
     *
     * @param \IdmFile\Domain\Entity\File $uploads
     * @return User
     */
    public function addUpload(\IdmFile\Domain\Entity\File $uploads)
    {
        $this->uploads[] = $uploads;

        return $this;
    }

    /**
     * Remove uploads
     *
     * @param \IdmFile\Domain\Entity\File $uploads
     */
    public function removeUpload(\IdmFile\Domain\Entity\File $uploads)
    {
        $this->uploads->removeElement($uploads);
    }

    /**
     * Get uploads
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUploads()
    {
        return $this->uploads;
    }

    /**
     * Set group
     *
     * @param \IdmUser\Domain\Entity\Group $group
     * @return User
     */
    public function setGroup(\IdmUser\Domain\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \IdmUser\Domain\Entity\Group 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set principal
     *
     * @param \IdmUser\Domain\Entity\User $principal
     * @return User
     */
    public function setPrincipal(\IdmUser\Domain\Entity\User $principal = null)
    {
        $this->principal = $principal;

        return $this;
    }

    /**
     * Get principal
     *
     * @return \IdmUser\Domain\Entity\User 
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Set possible_replacements
     *
     * @param \IdmHoliday\Domain\Entity\Holiday $possibleReplacements
     * @return User
     */
    public function setPossibleReplacements(\IdmHoliday\Domain\Entity\Holiday $possibleReplacements = null)
    {
        $this->possible_replacements = $possibleReplacements;

        return $this;
    }

    /**
     * Get possible_replacements
     *
     * @return \IdmHoliday\Domain\Entity\Holiday 
     */
    public function getPossibleReplacements()
    {
        return $this->possible_replacements;
    }

    /**
     * Add roles
     *
     * @param \IdmUser\Domain\Entity\Role $roles
     * @return User
     */
    public function addRole(\IdmUser\Domain\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \IdmUser\Domain\Entity\Role $roles
     */
    public function removeRole(\IdmUser\Domain\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles;
    }
}
