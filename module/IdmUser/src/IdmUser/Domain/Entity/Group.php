<?php

namespace IdmUser\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Group
 *
 * @ORM\Table(name="groups")
 * @ORM\Entity
 */
class Group
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="IdmUser\Domain\Entity\Group", mappedBy="parent")
     */
    private $children;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="IdmUser\Domain\Entity\User", mappedBy="group")
     */
    private $members;

    /**
     * @var \IdmUser\Domain\Entity\Group
     *
     * @ORM\ManyToOne(targetEntity="IdmUser\Domain\Entity\Group", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var \IdmUser\Domain\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="IdmUser\Domain\Entity\User", inversedBy="managed_group")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="leader_id", referencedColumnName="id")
     * })
     */
    private $leader;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->members = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add children
     *
     * @param \IdmUser\Domain\Entity\Group $children
     * @return Group
     */
    public function addChild(\IdmUser\Domain\Entity\Group $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \IdmUser\Domain\Entity\Group $children
     */
    public function removeChild(\IdmUser\Domain\Entity\Group $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add members
     *
     * @param \IdmUser\Domain\Entity\User $members
     * @return Group
     */
    public function addMember(\IdmUser\Domain\Entity\User $members)
    {
        $this->members[] = $members;

        return $this;
    }

    /**
     * Remove members
     *
     * @param \IdmUser\Domain\Entity\User $members
     */
    public function removeMember(\IdmUser\Domain\Entity\User $members)
    {
        $this->members->removeElement($members);
    }

    /**
     * Get members
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Set parent
     *
     * @param \IdmUser\Domain\Entity\Group $parent
     * @return Group
     */
    public function setParent(\IdmUser\Domain\Entity\Group $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \IdmUser\Domain\Entity\Group 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set leader
     *
     * @param \IdmUser\Domain\Entity\User $leader
     * @return Group
     */
    public function setLeader(\IdmUser\Domain\Entity\User $leader = null)
    {
        $this->leader = $leader;

        return $this;
    }

    /**
     * Get leader
     *
     * @return \IdmUser\Domain\Entity\User 
     */
    public function getLeader()
    {
        return $this->leader;
    }
}
