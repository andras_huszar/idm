<?php
namespace IdmUser\Domain\Service;

use IdmBackend\Domain\AbstractEntityService;
use IdmUser\Domain\Entity\User;
use Zend\Crypt\Password\Bcrypt;
use Doctrine\ORM\Event\LifecycleEventArgs;

class UserService extends AbstractEntityService
{

    /**
     * @var string
     */
    protected $entityClass = 'IdmUser\Domain\Entity\User';

    /**
     * @var string
     */
    protected $inputFilterClass = 'IdmUser\Domain\Filter\UserFilter';

    /**
     * @var array
     */
    protected $config;

    /**
     * @var \Zend\Crypt\Password\PasswordInterface
     */
    protected $passwordAdapter;

    /**
     * @return array
     */
    public function getConfig()
    {
        if ($this->config === null) {
            $config = $this->getServiceLocator()->get('Config');
            $this->config = isset($config['application']) ? $config['application'] : array();
        }
        return $this->config;
    }

    /**
     *
     * @return \Zend\Crypt\Password\PasswordInterface
     */
    public function getPasswordAdapter()
    {
        if ($this->passwordAdapter === null) {
            $this->passwordAdapter = new Bcrypt();
            $this->passwordAdapter->setBackwardCompatibility(true);
        }
        return $this->passwordAdapter;
    }

    /**
     *
     * @param \Zend\Crypt\Password\PasswordInterface $adapter
     */
    public function setPasswordAdapter($adapter)
    {
        $this->passwordAdapter = $adapter;
    }

    /**
     * Find one user by email
     *
     * @param string $email
     * @return object
     */
    public function findByEmail($email)
    {
        $entity = $this->getRepository()->findOneByEmail($email);
        return $entity;
    }

    /**
     * @return array
     */
    public function findBoss()
    {
        $grpRepo = $this->getEntityManager()->getRepository('IdmUser\Entity\Group');
        $group = $grpRepo->findOneBy(array('parent' => null));
        return $group->getLeader();
    }

    /**
     *
     * @param \IdmUser\Entity\User $user
     * @return \IdmUser\Form\UserForm
     */
    public function getForm($user = null)
    {
        $form = parent::getForm($user);

        // remove password field if not a new user
        if ($this->getEntityManager()->contains($user)) {
            $form->remove('password')->remove('passconf');
            $filter = $form->getInputFilter();
            $filter->remove('password')->remove('passconf');
        }
        return $form;
    }

    /**
     */
    protected function updateGroupLeader(User $user)
    {
        $group = $user->getGroup();
        if ($group) {
            $principal = $group->getLeader();
            if (!$principal) {
                $principal = $this->getPrincipal($user);
            }
            $user->setPrincipal($principal);
        }
    }

    /**
     */
    public function getPrincipal(User $user)
    {
        $group = $user->getGroup();
        $principal = $this->getServiceLocator()->get('IdmUser\Domain\Service\GroupService')
                                               ->findPrincipal($group);
        return $principal;
    }

    /**
     * Pre insert callback
     * -hashing password for new user
     * -set create date
     */
    public function prePersist(User $user, LifecycleEventArgs $eventArgs)
    {
        $password = $this->hashPassword($user->getPassword());
        $user->setPassword($password)
             ->setCreatedAt(new \DateTime('Now'));

        if ($user->getIsActive() !== true) {
            $user->setIsLead(false)->setIsSenior(false);
        } elseif (!$user->getAdmitedAt()) {
            $user->setAdmitedAt(new \DateTime('Now'));
        }
    }

    /**
     * Pre update callback
     * -set admit date and unset fire date if actived
     * -set fire date if inactived
     * -set update date
     */
    public function preUpdate(User $user, LifecycleEventArgs $eventArgs)
    {
        $user->setUpdatedAt(new \DateTime('Now'));

        if ($eventArgs->hasChangedField('is_active')) {
            if ($user->getIsActive() === true) {
                $user->setAdmitedAt(new \DateTime('Now'))->setFiredAt(null);
            } else {
                $user->setFiredAt(new \DateTime('Now'));
            }
        }

        if ($eventArgs->hasChangedField('group')) {
            $this->updateGroupLeader($user);
        }
    }

    /**
     * Hash password
     *
     * @param string $password
     */
    protected function hashPassword($password)
    {
        return $this->getPasswordAdapter()->create($password);
    }

    /**
     * Verify user password
     *
     * @param User $user
     * @param string $password
     */
    public function verifyPassword(User $user, $password)
    {
        return $this->getPasswordAdapter()->verify($password, $user->getPassword());
    }

    /**
     * @param User $user
     * @param string $password
     */
    public function changePassword(User $user, $password)
    {
        $user->setPassword($this->getPasswordAdapter()->create($password));
        $this->save($user);
    }

    /**
     * @param User $user
     * @return string
     */
    public static function fullName($user)
    {
        if ($user instanceof User) {
    	    $data = array($user->getFirstName(), $user->getLastName());
        } else {
            $data = array($user['first_name'], $user['last_name']);
        }
        list($firstName, $lastName) = $data;
        return sprintf('%s %s', $firstName, $lastName);
    }

}