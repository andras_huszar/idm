<?php
namespace IdmUser\Domain\Service;

use Zend\Authentication\Result;
use IdmUser\Domain\Service\UserService;
use IdmUser\Domain\Entity\User;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Service\EventManagerFactory;
use Zend\EventManager\EventManager;

class IdentityService
{
	protected $events;
	
	protected $userService;
	
	protected $authService;
	
	protected $identityUser;

	/**
	 * @param UserService $userService
	 */
	public function __construct(UserService $userService, AuthenticationService $authService)
	{
		$this->userService = $userService;
		$this->authService = $authService;
	}

    /**
     * @return \Zend\Authentication\AuthenticationService
     */
    public function getAuthenticationService()
    {
        return $this->authService;
    }

    /**
     * @return \Zend\Authentication\AuthenticationService
     */
    public function getUserService()
    {
        return $this->userService;
    }
    
    /**
     * @return \Zend\EventManager\EventManagerInterface
     */
    public function getEventManager()
    {
        if ($this->events === null) {
            $this->events = new EventManager();
        }
        return $this->events;
    }

	/**
	 * Authenticate user
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 */
	public function login($email, $password, $remember = false)
	{
		$this->getEventManager()->trigger('login.pre', $this, array(
		    'email' => $email,
		    'password' => $password
		));

		$authService = $this->getAuthenticationService();
		$adapter = $authService->getAdapter();
		$adapter->setIdentity($email);
		$adapter->setCredential($password);

	    if ($remember === true) {
	        $authService->getStorage()->setRememberMe(true);
	    }

		$result = $authService->authenticate();

		$this->getEventManager()->trigger('login.post', $this, array(
		    'result' => $result,
		    'user' => $this->getIdentityUser()
		));
		return $result->getCode() === Result::SUCCESS;
	}

	/**
	 * Clear user authentication
	 * @return User
	 */
	public function logout()
	{
	    $user = $this->getIdentityUser();
		$this->getEventManager()->trigger('logout.pre', $this, array(
		    'user' => $user
		));

// 		$this->getAuthenticationService()->getStorage()->forgetMe();
	    $this->getAuthenticationService()->clearIdentity();

		$this->getEventManager()->trigger('logout.post', $this, array(
		    'user' => $user
		));
	}

	/**
	 * Get current user
	 * @return User
	 */
	public function hasIdentity()
	{
	    $id = (int) $this->getAuthenticationService()->hasIdentity();
	}
	
	/**
	 * @return User
	 */
	public function createGuest()
	{
		$user = new User();
		$user->setFirstName('Guest')->setLastName('User');
		return $user;
	}

	/**
	 * Get current user
	 * @return User
	 */
	public function getIdentityUser()
	{
		if ($this->identityUser === null) {
			$id = (int) $this->getAuthenticationService()->getIdentity();
		    $this->identityUser = $this->userService->findById($id);
		    if (!$this->identityUser) {
		    	$this->identityUser = $this->createGuest();
		    }
		}
	    return $this->identityUser;
	}

}