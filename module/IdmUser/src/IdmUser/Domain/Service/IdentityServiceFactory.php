<?php
namespace IdmUser\Domain\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use IdmUser\Domain\Service\IdentityService;

class IdentityServiceFactory implements FactoryInterface
{
    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $services)
    {
    	$userService = $services->get('IdmUser\Domain\Service\UserService');
    	$authService = $services->get('IdmCore\AuthenticationService');
        return new IdentityService($userService, $authService);
    }
}