<?php
namespace IdmUser\Domain\Service;

use IdmBackend\Domain\AbstractEntityService;
use IdmUser\Domain\Entity\Group;
use Doctrine\ORM\Event\LifecycleEventArgs;

class GroupService extends AbstractEntityService
{

    /**
     * @var string
     */
    protected $entityClass = 'IdmUser\Domain\Entity\Group';

    /**
     * @var string
     */
    protected $inputFilterClass = 'IdmUser\Domain\Filter\GroupFilter';

    public function prePersist(Group $group, LifecycleEventArgs $eventArgs)
    {
        $this->handleGroupLeaderChange($group);
    }

    public function preUpdate(Group $group, LifecycleEventArgs $eventArgs)
    {
        if ($eventArgs->hasChangedField('leader')) {
            $this->handleGroupLeaderChange($group);
        }
    }

    protected function handleGroupLeaderChange(Group $group)
    {
        //echo "Saving group {$group->getName()}<br>";
        $leader = $group->getLeader();
        $principal = $this->findPrincipal($group);

        if ($leader) {
            if ($leader != $principal) {
                $leader->setPrincipal($principal);
                $principal->addSubordinate($leader);
                $this->updateChangeSets($leader);
                $this->updateChangeSets($principal);
            } else {
                $leader->setPrincipal(null);
            }
        } else {
            $leader = $principal;
        }

        //echo "------------------<br>";
        $this->getEntityManager()->persist($principal);
        $this->getEntityManager()->persist($leader);

        foreach ($group->getMembers() as $member) {
            if ($member != $leader) {
                $member->setPrincipal($leader);
                $this->getEntityManager()->persist($member);
            }
        }
        foreach ($group->getChildren() as $childGrp) {
            $this->handleGroupLeaderChange($childGrp);
        }
    }

    public function findPrincipal(Group $group)
    {
        //echo "Find principal for {$group->getName()}<br>";
        $parentGrp = $group->getParent();
        if ($parentGrp) {
            //echo "Parent group {$parentGrp->getName()}<br>";
            $parentGrpLeader = $parentGrp->getLeader();
            if ($parentGrpLeader && $parentGrpLeader != $group->getLeader()) {
                //echo "Principal found {$parentGrpLeader->getEmail()}<br>";
                return $parentGrpLeader;
            } else {
                //echo "No leader find next<br>";
                return $this->findPrincipal($parentGrp);
            }
        } else {
            //echo "No parent group<br>";
            return null;
        }
    }

}