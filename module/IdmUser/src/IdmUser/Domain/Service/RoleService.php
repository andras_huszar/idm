<?php
namespace IdmUser\Domain\Service;

use IdmBackend\Domain\AbstractEntityService;
use IdmUser\Form\RoleRightForm;

class RoleService extends AbstractEntityService
{
    /**
     * @var string
     */
    protected $entityClass = 'IdmUser\Domain\Entity\Role';

    /**
     * @var string
     */
    protected $inputFilterClass = 'IdmUser\Domain\Filter\RoleFilter';
}