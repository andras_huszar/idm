
<<<<<<< Updated upstream
=======
0.0.9
	Routes reconfigured
		Now we uses default route for simple requests
			in ZF1 style: *:controller/:action/:params*
	Navigation configured
	Authentication updated
		*AuthenticationAdapter* implemented
		Check has identity on dispatch
		Forward to login if has not
	Acl updated
		*Zend\Acl* extended
		Check access for rescoures on dispatch
			controller => resource
			action => privilege
		Still come from configuration

0.0.9
	*Backend\UserService*

0.0.8
	Backend updates
		**AbstractEntityService** updated
			Magic form creating from entity name
			Method **delete** renamed to **remove**
			Alias param added to **createQuery** method
			**createPaginator** updated
			**Logger** added for logging backend level stuff
		**DefaultHydrator** updated
			Generate camelcase getter-setter method names from fields
			Date strategy added, need test and fix

0.0.7
	Bootstrap frontend added
	Twb bootstrap module added

0.0.6
	Separeted config files created
	Navigation config and factories added

0.0.5
	Schema updated
		User fields added
		Group renamed to Role
	Base service, filter and form for User

0.0.4
	Entity service layer and hydrator implementation for doctrine

0.0.3
	Application routes configure
	Restricted routes handling added

0.0.2
	Doctrine build scripts added
	Doctrine schema manager script added
	IdmBackend module created with doctrine yaml metadata config

* 0.0.9
	* Acl
		* Acl config added
		* 

* 0.0.8
	* Authentication

* 0.0.7
	* Navigation
		* Default navigation configured

* 0.0.6
	* Role crud
		* `RoleController` admin pages
		* `RoleService` and `RoleForm`

* 0.0.5
	* User crud
		* `UserController` admin pages
		* `UserService` class implemented
		* `UserForm` created with input filter

* 0.0.4
	* Backend model created
		* Doctirne yaml config
		* Entities generated

>>>>>>> Stashed changes
* 0.0.3
	* Application config
		* Config splited to multiple files
		* Default route created
	* Abstract crud controller created

* 0.0.2
	* Backend module created
		* Base entity service
		* Default hydrator with strategy for associations
		* Defatul naming strategy
		* Namespace driver configured

* 0.0.1
	* Application configured
		* Environments
		* Default directory sctructure created
		* Modules env configs
	* Required modules
		* Doctrine
		* GkSmarty
		* TwbBundle
	* Dev modules
		* ZFTool
		* ZendDeveloperTools
		* EdpSuperluminal
		* PgiTool
		* Yaml
		* SqlFormatter
	* Smarty templates added to application
	* Builder added

* 0.0.0
	* Zend Skeleton