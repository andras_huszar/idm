php -f ./bin/doctrine.php orm:convert-mapping annotation module/IdmUser/src --force --filter=IdmUser\Domain\Entity
php -f ./bin/doctrine.php orm:convert-mapping annotation module/IdmFile/src --force --filter=IdmFile\Domain\Entity
php -f ./bin/doctrine.php orm:convert-mapping annotation module/IdmReporting/src --force --filter=IdmReporting\Domain\Entity
php -f ./bin/doctrine.php orm:convert-mapping annotation module/IdmHoliday/src --force --filter=IdmHoliday\Domain\Entity
php -f ./bin/doctrine.php orm:generate-entities module/IdmUser/src --update-entities --filter=IdmUser\Domain\Entity
php -f ./bin/doctrine.php orm:generate-entities module/IdmFile/src --update-entities --filter=IdmFile\Domain\Entity
php -f ./bin/doctrine.php orm:generate-entities module/IdmReporting/src --update-entities --filter=IdmReporting\Domain\Entity
php -f ./bin/doctrine.php orm:generate-entities module/IdmHoliday/src --update-entities --filter=IdmHoliday\Domain\Entity
php -f ./bin/doctrine.php orm:validate-schema
php -f ./bin/doctrine.php orm:ensure-production-settings