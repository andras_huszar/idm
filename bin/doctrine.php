<?php

use Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand;
include __DIR__ . '/../bootstrap.php';

/* @var $em \Doctrine\ORM\EntityManager */
$em = $app->getServiceManager()->get('doctrine.entitymanager.orm_default');

//$config = $app->getServiceManager()->get('config');
//$config = isset($config['doctrine_entities']) ? $config['doctrine_entities'] : array();

$config = array(
);

// $driver = new \Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver($config);
$driver = new \Doctrine\ORM\Mapping\Driver\YamlDriver(__DIR__ . '/../module/IdmBackend/config/doctrine/', '.yml');
$driver->setGlobalBasename('schema');
$em->getConfiguration()->setMetadataDriverImpl($driver);

/* @var $cli \Symfony\Component\Console\Application */
$cli = $app->getServiceManager()->get('doctrine.cli');
$cli->run();