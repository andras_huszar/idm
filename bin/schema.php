<?php

include __DIR__ . '/../bootstrap.php';

/* @var $em \Doctrine\ORM\EntityManager */
$em = $app->getServiceManager()->get('doctrine.entitymanager.orm_default');

$dumpDir = __DIR__ . '/../data/migrations/';

// Get all configured entity
$factory = new Doctrine\ORM\Mapping\ClassMetadataFactory();
$factory->setEntityManager($em);
$metadatas = $factory->getAllMetadata();

$tool = new \Doctrine\ORM\Tools\SchemaTool($em);
switch ($_SERVER['argv'][1]) {
	case 'install':
	case 'create':
	    $tool->createSchema($metadatas);
	    break;
	case 'update':
	    $tool->updateSchema($metadatas);
	    break;
	case 'drop':
	    $tool->dropDatabase($metadatas);
	    break;
	case 'reset':
	    // @todo!
	    $tool->dropAndCreate($metadatas);
	    break;
	case 'install-sql':
	    $sql = '';
	    foreach ($tool->getCreateSchemaSql($metadatas) as $table) {
	        $sql .= SqlFormatter::format($table, false) . "\n\n";
	    }
	    file_put_contents($dumpDir . 'install_' . date('YmdHi') . '.sql', $sql);
	    break;
	case 'update-sql':
	    $sql = '';
	    foreach ($tool->getUpdateSchemaSql($metadatas) as $table) {
	        $sql .= SqlFormatter::format($table, false) . "\n\n";
	    }
	    file_put_contents($dumpDir . 'update_' . date('YmdHi') . '.sql', $sql);
	    break;
	default:
	    echo 'Invalid command';
}

